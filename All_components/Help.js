import React from 'react';
import { View, Text, globalStylesheet, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

const Help = ({navigation}) => {
  return (
    <SafeAreaView style={globalStyles.containerHelp}>
      <ScrollView contentContainerStyle={globalStyles.scrollContainerHelp}>
        <Text style={globalStyles.headerHelp}>Help</Text>
        <View style={globalStyles.sectionHelp}>
          <Text style={globalStyles.sectionTitleHelp}>How to Track</Text>
          <Text style={globalStyles.sectionTextHelp}>
            To track an activity, simply navigate to the tracking screen and select the activity you want to track.
            {"\n\n"}
            Enter any relevant details such as duration, distance, or notes, then press the "Track" button to save your entry.
          </Text>
        </View>
        <View style={globalStyles.sectionHelp}>
          <Text style={globalStyles.sectionTitleHelp}>Setting Reminders</Text>
          <Text style={globalStyles.sectionTextHelp}>
            You can set reminders for your activities by accessing the settings menu and selecting "Reminders."
            {"\n\n"}
            Choose the activity for which you want to set a reminder, specify the time and frequency, and save your settings.
          </Text>
        </View>
        <View style={globalStyles.sectionHelp}>
          <Text style={globalStyles.sectionTitleHelp}>Viewing Reports</Text>
          <Text style={globalStyles.sectionTextHelp}>
            To view detailed reports of your tracked activities, navigate to the reports section of the app.
            {"\n\n"}
            Here, you can filter your data by date range, activity type, or any other relevant criteria to generate custom reports.
          </Text>
        </View>
        <TouchableOpacity style={globalStyles.backButtonHelp}  onPress={() => navigation.navigate('Settings')}>
          <Text style={globalStyles.backButtonTextHelp}>Back</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

// const globalStyles = globalStylesheet.create({
//   containerHelp: {
//     flex: 1,
//     backgroundColor: '#FFFFFF',
//   },
//   scrollContainerHelp: {
//     flexGrow: 1,
//     marginHorizontal: wp('5%'),
//   },
//   headerHelp: {
//     fontSize: wp('6%'),
//     fontWeight: 'bold',
//     textAlign: 'center',
//     marginTop: hp('4%'),
//     marginBottom: hp('4%'),
//   },
//   sectionHelp: {
//     marginBottom: hp('4%'),
//   },
//   sectionTitleHelp: {
//     fontSize: wp('5%'),
//     fontWeight: 'bold',
//     marginBottom: hp('2%'),
//   },
//   sectionTextHelp: {
//     fontSize: wp('3%'),
//     lineHeight: hp('3%'),
//   },
//   backButtonHelp: {
//     backgroundColor: '#7F3DFF',
//     borderRadius: wp('2%'),
//     paddingVertical: hp('2%'),
//     alignItems: 'center',
//     marginBottom: hp('3%'),
//   },
//   backButtonTextHelp: {
//     color: '#FFFFFF',
//     fontSize: wp('4%'),
//   },
// });

export default Help;