import { SafeAreaView, globalStylesheet, Text, View, Image, StatusBar, TouchableOpacity,Modal,TouchableWithoutFeedback } from 'react-native'
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

const DetailTransactionIncome = ({ navigation, route, }) => {
    const { userData } = route.params || {};
    // use state for modal
    const [showModal, setShowModal] = useState(false);
    // Alert useState
    const [showAlert, setShowAlert] = useState(false);
     // toggle for modal
  const toggleModal = () => {
      setShowModal(!showModal);
    };
  // handle Alert
  const handleYesButton = () => {
      toggleModal(); // Close the modal
      setShowAlert(true); // Show the custom alert
  };
  // Close the custom alert when press the outside of the screen
  const handleOverlayPress = () => {
      setShowAlert(false); 
  };
  const CustomAlert = () => (
      <TouchableWithoutFeedback onPress={handleOverlayPress}>
      <View style={globalStyles.DetailTransactioncustomAlertContainer}>
          <View style={globalStyles.DetailTransactioncustomAlert}>
              <Icon name="check-circle" style={globalStyles.DetailTransactionalertIcon} />
              <Text style={globalStyles.alertText}>Transaction has been successfully removed</Text>
          </View>
      </View>
      </TouchableWithoutFeedback>
  );
  return (
    <SafeAreaView style={globalStyles.DetailTransactionIncomeSafe}>
      <StatusBar translucent backgroundColor="#00a86b" />
      <View style={globalStyles.DetailTransactionIncomeMain}>
        <View style={globalStyles.DetailTransactionIncomeTopBOTH}>
          <View style={globalStyles.DetailTransactionIncomeTopHead}>
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
            <Icon name='arrow-left' style={globalStyles.arrowleftDeatailIncome}></Icon>
          </TouchableOpacity>
            <Text style={globalStyles.DetailTransactionIncomeText}>Detail Transaction</Text>
            <TouchableOpacity onPress={toggleModal}>
              <Icon name='trash' style={globalStyles.trashDeatailIncome}></Icon>
            </TouchableOpacity>
          </View>
          <View style={globalStyles.DetailTransactionIncomeSub2}>
            <Text style={globalStyles.DetailTransactionIncomeSub2Rupess}>$5000</Text>
            <Text style={globalStyles.DetailTransactionIncomeSub2Shoping}>Salary for July</Text>
            <View style={globalStyles.DetailTransactionIncomeSu2DayDate}>
              <Text style={globalStyles.DetailTransactionIncomeSub2Day}>Saturday 4 June 2021</Text>
              <Text style={globalStyles.DetailTransactionIncomeSubDate}>16.20</Text>
            </View>
          </View>
        </View>

        <View style={globalStyles.DetailTransactionIncomeSub3}>
          <View style={globalStyles.DetailTransactionIncomeSub3Sub}>
            <Text style={globalStyles.DetailTransactionIncomeSub3Text1}>Type</Text>
            <Text style={globalStyles.DetailTransactionIncomeSub3Text2}>Income</Text>
          </View>
          <View style={globalStyles.DetailTransactionIncomeSub3Sub}>
            <Text style={globalStyles.DetailTransactionIncomeSub3Text1}>Category</Text>
            <Text style={globalStyles.DetailTransactionIncomeSub3Text2}>Salary</Text>
          </View>
          <View style={globalStyles.DetailTransactionIncomeSub3Sub}>
            <Text style={globalStyles.DetailTransactionIncomeSub3Text1}>Wallet</Text>
            <Text style={globalStyles.DetailTransactionIncomeSub3Text2}>Chase</Text>
          </View>
        </View>

        <View style={globalStyles.DetailTransactionIncomeSub4}>
          <Text style={globalStyles.DetailTransactionIncomeSub4Des}>Description</Text>
          <Text style={globalStyles.DetailTransactionIncomeSub4Con}>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</Text>
          <Text style={globalStyles.DetailTransactionIncomeSub4AttachHead}>Attachment</Text>
          <Image
            source={{ uri: 'https://s3-alpha-sig.figma.com/img/49cc/86e6/849cbd762a23c32962df5f832e7fdf9e?Expires=1716163200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=oanIqv2mujPJGvJT9frRJmX4kSv67HlnW7BP3KVL26yYlO3f6OdEPaCWQ504KEEnVkjxw~Hrdo2yIk2yv2xa965t2tQjM2FKPaRdfvytAFHhxWcs7A4ooi8Qt-tIK8~0F9oPFCZ~enJBtxAJs1pWKvoOHkqhY1Gzf11zIF7QF1KDjKSNgn2DGrSHyXdu2Q3JJIOqgr0M~mIP6FGaaUMkDDsYvZxs~0HD5lhkmW4-Yv2U6MIAqIkvrywLUCODJ3iLWLkV0mCTPMD5-FZTwySRwkjiRmC-YeSPySuXadb-HVta2oeVgNh5lmN7gSfrZDRWnZgqnSsSodW9XS~K73V6GQ__' }}
            style={globalStyles.DetailTransactionIncomeSub4Image}
          />
        </View>
      </View>

      {/* Bottom Button */}
      <TouchableOpacity style={globalStyles.DetailTransactionIncomeSub5} >
        <Text style={globalStyles.DetailTransactionIncomeSub5Text}>Edit</Text>
      </TouchableOpacity>
      {/* Modal */}
      <Modal visible={showModal} animationType="slide" transparent>
        <View style={globalStyles.DetailTransactionmodalContainer}>
          <View style={globalStyles.DetailTransactionmodalContent}>
            <View style={globalStyles.DetailTransactionmodalDivider}></View>
            <Text style={globalStyles.DetailTransactionmodalTitle}>Remove this transaction?</Text>
            <Text style={globalStyles.DetailTransactionmodalText}>Are you sure you want to remove this transaction?</Text>
            <View style={globalStyles.DetailTransactionmodalButtonContainer}>
              <TouchableOpacity style={globalStyles.DetailTransactionmodalButtonNo} onPress={toggleModal}>
                <Text style={globalStyles.DetailTransactionmodalButtonTextNo}>No</Text>
              </TouchableOpacity>
              <TouchableOpacity style={globalStyles.DetailTransactionmodalButtonYes} onPress={handleYesButton}>
                <Text style={globalStyles.DetailTransactionmodalButtonTextYes} >Yes</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* Custom Alert */}
      {showAlert && <CustomAlert />}
    </SafeAreaView>
  )
}

export default DetailTransactionIncome

// const globalStyles = globalStylesheet.create({
//     DetailTransactionIncomeSafe: {
//         flexGrow:1
//       },
//       DetailTransactionIncomeMain: {
//         display:'flex',
//         justifyContent:'center',
//         marginTop: StatusBar.currentHeight || 0,
//       },
//       DetailTransactionIncomeTopBOTH: {
//         backgroundColor: '#00a86b',
//         paddingHorizontal: wp('5%'),
//         paddingVertical: hp('5%'),
//         borderBottomLeftRadius: hp('3%'),
//         borderBottomRightRadius: hp('3%'),
//         position: 'relative',
//       },
//       DetailTransactionIncomeTopHead: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//       },
//       arrowleftDeatailIncome: {
//         fontSize: hp('2.5%'),
//         color: '#fff',
//       },
//       DetailTransactionIncomeText: {
//         fontSize: hp('2.5%'),
//         color: '#fff',
//       },
//       trashDeatailIncome: {
//         fontSize: hp('2.5%'),
//         color: '#fff',
//       },
//       DetailTransactionIncomeSub2: {
//         justifyContent: 'center',
//         alignItems: 'center',
//         paddingTop: hp('4%'),
//         paddingBottom: hp('4%'),
//         gap: hp('1%'),
//       },
//       DetailTransactionIncomeSu2DayDate: {
//         flexDirection: 'row',
//         gap: hp('1%'),
//       },
//       DetailTransactionIncomeSub2Rupess: {
//         fontSize: hp('5%'),
//         fontWeight: '700',
//         color: '#fff',
//       },
//       DetailTransactionIncomeSub2Shoping: {
//         color: '#fff',
//         fontSize: hp('2%'),
//       },
//       DetailTransactionIncomeSub2Day: {
//         color: '#fff',
//         fontSize: hp('2%'),
//       },
//       DetailTransactionIncomeSubDate: {
//         color: '#fff',
//         fontSize: hp('2%'),
//       },
//       DetailTransactionIncomeSub3: {
//         flexDirection: 'row',
//         borderWidth: 1,
//         marginHorizontal: wp('5%'),
//         justifyContent: 'space-around',
//         bottom: hp('4%'),
//         backgroundColor: '#fff',
//         paddingVertical: hp('2a%'),
//         borderRadius: hp('1%'),
//         borderColor: '#f1f1fa',
//       },
//       DetailTransactionIncomeSub3Sub: {
//         alignItems: 'center',
//         gap: hp('1%'),
//       },
//       DetailTransactionIncomeSub3Text1: {
//         color: '#91919f',
//         fontSize: hp('1.7%'),
//         fontWeight: '500',
//       },
//       DetailTransactionIncomeSub3Text2: {
//         fontSize: hp('2%'),
//         fontWeight: '500',
//       },
//       DetailTransactionIncomeSub4: {
//         borderTopWidth: hp('0.2%'),
//         borderStyle: 'dashed',
//         marginHorizontal: wp('5%'),
//         bottom: hp('2%'),
//         paddingVertical: hp('2%'),
//         gap: hp('1%'),
//       },
//       DetailTransactionIncomeSub4Des: {
//         fontSize: hp('2%'),
//         color: '#91919f',
//       },
//       DetailTransactionIncomeSub4Con: {
//         fontSize: hp('1.8%'),
//       },
//       DetailTransactionIncomeSub4AttachHead: {
//         fontSize: hp('2%'),
//         color: '#91919f',
//       },
//       DetailTransactionIncomeSub4Image: {
//         width: '100%',
//         height: hp('20%'),
//         borderRadius: hp('1%'),
//       },
//       DetailTransactionIncomeSub5: {
//         position: 'absolute',
//         bottom: hp('2%'),
//         left: wp('5%'),
//         right: wp('5%'),
//         backgroundColor: '#7f3dff',
//         alignItems: 'center',
//         paddingVertical: hp('2%'),
//         borderRadius: hp('1%'),
//       },
//       DetailTransactionIncomeSub5Text: {
//         fontSize: hp('2%'),
//         fontWeight: '600',
//         color: '#fff',
//       },
//        //   for modal
//        DetailTransactionmodalContainer: {
//     flex: 1,
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//     backgroundColor: 'rgba(0, 0, 0, 0.5)',
//   },
//   DetailTransactionmodalContent: {
//     backgroundColor: '#ffffff',
//     width: '100%',
//     paddingVertical: hp('2%'),
//     paddingHorizontal: wp('5%'),
//     borderTopLeftRadius: hp('2%'),
//     borderTopRightRadius: hp('2%'),
//     gap:15,
//   },
//   DetailTransactionmodalDivider: {
//     width: '10%',
//     height: 5,
//     backgroundColor: '#d3bdff',
//     alignSelf: 'center',
//     marginBottom: hp('1%'),
//   },
//   DetailTransactionmodalTitle: {
//     fontSize: hp('2.2%'),
//     fontWeight: '600',
//     textAlign: 'center',
//     marginBottom: hp('1%'),
//   },
//   DetailTransactionmodalText: {
//     fontSize: hp('2%'),
//     textAlign: 'center',
//     marginBottom: hp('2%'),
//     color:'#91919f'
//   },
//   DetailTransactionmodalButtonContainer: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   DetailTransactionmodalButtonNo: {
//     flex: 1,
//     paddingVertical: hp('2%'),
//     alignItems: 'center',
//     backgroundColor: '#eee5ff',
//     borderRadius: hp('2%'),
//     marginHorizontal: wp('1%'),
//   },
//   DetailTransactionmodalButtonYes: {
//     flex: 1,
//     paddingVertical: hp('2%'),
//     alignItems: 'center',
//     backgroundColor: '#7f3dff',
//     borderRadius: hp('2%'),
//     marginHorizontal: wp('1%'),
//   },
//   DetailTransactionmodalButtonTextNo: {
//     fontSize: hp('2%'),
//     fontWeight: '600',
//     color: '#7f3dff',
//   },
//   DetailTransactionmodalButtonTextYes: {
//     fontSize: hp('2%'),
//     fontWeight: '600',
//     color: '#ffffff',
//   },
// //   Alert
// DetailTransactionoverlay: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: 'rgba(0, 0, 0, 0.5)', 
// },
// DetailTransactioncustomAlertContainer: {
//     position: 'absolute',
//     top: 0,
//     bottom: 0,
//     left: 0,
//     right: 0,
//     backgroundColor: 'rgba(0, 0, 0, 0.5)', 
//     justifyContent: 'center',
//     alignItems: 'center',
// },
// DetailTransactioncustomAlert: {
//     backgroundColor: '#fff',
//     padding: wp('5%'), 
//     borderRadius: hp('2%'), 
//     alignItems: 'center',
// },
// DetailTransactionalertIcon: {
//     fontSize: hp('7%'), 
//     color: '#5233ff',
//     marginBottom: hp('1%'), 
// },
// DetailTransactionalertText: {
//     fontSize: hp('2%'), 
//     textAlign: 'center',
// },
// })