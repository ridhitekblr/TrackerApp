import { StyleSheet, Platform,StatusBar } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AddAttachmentTransfer from './AddAttachmentTransfer';
// import { Dimensions, PixelRatio } from 'react-native';

// const { width } = Dimensions.get('window');

// Define a function to calculate responsive font size
// const getResponsiveFontSize = (fontSize) => {
  // const baseWidth = 375; // Base width for scaling
  // const scale = width / baseWidth; // Calculate scaling factor based on device width
  // const newSize = fontSize * scale; // Calculate responsive font size
  // return Math.round(PixelRatio.roundToNearestPixel(newSize));
// };

// Adjust font size based on platform (if needed)
// const isIOS = Platform.OS === 'ios';
// const platformFontSize = isIOS ? 0 : -2; // Adjust as per requirement
export const globalStyles = StyleSheet.create({
    container: {
        flex: 1,
        paddingRight: 10,
        paddingLeft: 10,
        backgroundColor: '#FFFFFF',
      },
      darkContainer: {
        backgroundColor: '#000000',
      },
      item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        fontSize: 18,
        height: 60,
      },
      itemText: {
        fontSize: 18,
        color: 'black', // Default text color
      },
      
      darkText: {
        color: 'white', // Text color in dark mode
      },
      itemWithIcon: {
        display: 'flex',
        flexDirection: 'row',
      },
      itemTextright: {
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 12,
        color: 'grey',
        paddingTop: 4,
        opacity: 0.7,
      },
    //   Container Home CSS
     containerHomeSetting:{
        flex: 1, alignItems: 'center', justifyContent: 'center' },
    //   Detail normal and dark mode
    darkitemText:{
        color:'#ffffff'
    },
    // Language normal and dark css
    containerScroll: {
       paddingRight: 10, 
        paddingLeft: 10,
      },
    //   Notification dark and normal css
    containerNotification: {
        flex: 1,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop:20
    
      },
      itemno: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        fontSize: 18,
        height: 60,
        marginBottom:20
      },
      smallText: {
        fontSize: 14,
        color: '#888',
     },
      toggleIcon: {
        fontSize: 36, 
      },

      // Profile css ----------------------------start
      containerProfile: {
        flex: 1,
        padding: wp(6.25),
        position: 'relative',
        backgroundColor: '#F6F6F6'
      },
      profileContainerpr: {
        marginBottom: hp(2.5),
      },
      profileInfopr: {
        flexDirection: 'row',
        alignItems: 'center',
      },
      profileImage: {
        width: wp(22.5),
        height: wp(22.5),
        borderRadius: wp(11.25),
        marginRight: wp(2.5),
        borderColor: '#AD00FF',
        borderWidth: wp(0.75)
      },
      userInfopr: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
      },
      usernamepr: {
        fontSize: wp(6.5),
        fontWeight: 'bold',
        marginRight: wp(2.5),
      },
      pencil: {
        position: 'absolute',
        right: 0,
      },
      DownICON4: {
        backgroundColor: '#EEE5FF',
        padding: wp(2),
        borderRadius: wp(2.5),
        marginRight: wp(3.25),
      },
      DownICON4L: {
        backgroundColor: '#FFE2E4',
        padding: wp(2),
        borderRadius: wp(2.5),
        marginRight: wp(3.25),
      },
      menuprofile4: {
        fontSize: wp(4.5)
      },
      menuContainerpr: {
        paddingLeft: wp(3.75),
        paddingTop: hp(2.5),
        paddingBottom: hp(2.5),
        borderRadius: wp(5),
        paddingRight: wp(3.75),
        backgroundColor: '#fff'
      },
      menuItempr: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: hp(1.875),
        borderBottomColor: 'grey',
      },

      // dark mode for profile
      usernameprofile:{
        color:'grey'
      },
      usernameprDark:{
        color:'#FFFFFF'
      },
      menuContainerprDark:{
        backgroundColor:'grey'
      },
      // modal
      modalViewDark:{
      backgroundColor:'grey'
      },
      messageTextDark:{
        color:'#FFFFFF'
      },

      // <------------------------SignupCarousel  Start---------------------------------------------------->
      Onbordingcontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
      },
      Onbordingsiglogbtn: {
        width: '100%', // Ensure the parent container takes up 100% width
        justifyContent: 'center',
        alignItems: 'center',
      },
      Onbordingsignin: {
        width: '90%', 
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#7F3DFF',
        paddingVertical: 20,
        borderRadius: 20,
        marginBottom: 10,
      },
      Onbordingsignintxt: {
        color: '#FFFFFF',
        fontWeight: '600',
        fontSize:17,
      },
      Onbordinglogin: {
        width: '90%', 
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        borderRadius: 20,
        marginBottom: 10,
        backgroundColor: '#EEE5FF',
        marginBottom: 20
      },
      Onbordinglogintxt: {
        color: '#7F3DFF',
        fontWeight: '600',
        fontSize:17,
      },
      onboardingdot: {
        height: 10,
        borderRadius: 5,
        backgroundColor: '#7F3DFF',
        marginHorizontal: 8,
      },
      Onbordingcontainer1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
      },
      Onbordingimage: {
        flex: 0.4,
        justifyContent: 'center',
        alignItems: 'center',
      },
      OnbordingtextContainer: {
        flex: 0.3,
        justifyContent: 'center',
        alignItems: 'center',
        gap: 5
      },
      Onbordingtitle: {
        width: 277,
        fontWeight: '700',
        fontSize: 32,
        color: '#212325',
        textAlign: 'center',
        lineHeight: 39,
      },
      Onbordingdescription: {
        fontWeight: '500',
        color: '#91919F',
        textAlign: 'center',
        paddingHorizontal: 32,
        fontSize: 16,
        lineHeight: 19.36,
        width: 320
      },
    // <------------------------SignupCarousel End---------------------------------------------------->
    
     // <------------------------SetupAcount Start---------------------------------------------------->
    //  containerSetupAccount: {
    //   flex: 1,
    //   backgroundColor: 'white',
    //   padding: 20,
    //   justifyContent: 'center',
    //   alignItems: 'flex-start',
    // },
    // scrollContainerSetupAccount: {
    //   flexGrow: 1,
    // },
    // headingContainerSetupAccount: {
    //   marginBottom: 35,
    // },
    // headingSetupAccount: {
    //   fontSize: 40,
    //   // fontWeight: 'bold',
    //   textAlign: 'left',
    // },
    // contentContainerSetupAccount: {
    //   marginBottom: 40,
    // },
    // contentSetupAccount: {
    //   fontSize: 16,
    //   textAlign: 'left',
    // },
    // buttonSetupAccount: {
    //   position: 'absolute',
    //   bottom: 20,
    //   left: 20,
    //   right: 20,
    //   backgroundColor: '#7f3dff',
    //   paddingVertical: 15,
    //   alignItems: 'center',
    //   borderRadius: 8,
    // },
    // buttonTextSetupAccount: {
    //   fontSize: 18,
    //   color: 'white',
    //   fontWeight:'600'
    // },
    containerSetupAccount: {
      flex: 1,
      backgroundColor: 'white',
      padding: wp(5), // 5% of the screen width
      justifyContent: 'center',
      alignItems: 'flex-start',
    },
    scrollContainerSetupAccount: {
      flexGrow: 1,
    },
    headingContainerSetupAccount: {
      marginBottom: hp(1), // 4% of the screen height
    },
    headingSetupAccount: {
      fontSize: hp(4.5), // 4.5% of the screen height
      // fontWeight: 'bold',
      textAlign: 'left',
    },
    contentContainerSetupAccount: {
      marginBottom: hp(5), // 5% of the screen height
    },
    contentSetupAccount: {
      fontSize: hp(2), // 2% of the screen height
      textAlign: 'left',
    },
    buttonSetupAccount: {
      position: 'absolute',
      bottom: hp(2), // 2% of the screen height
      left: wp(5), // 5% of the screen width
      right: wp(5), // 5% of the screen width
      backgroundColor: '#7f3dff',
      paddingVertical: hp(2), // 1.5% of the screen height
      alignItems: 'center',
      borderRadius: 8,
    },
    buttonTextSetupAccount: {
      fontSize: hp(2), // 2% of the screen height
      color: 'white',
      fontWeight: '600',
    },
  
      


     // <------------------------SetupAcount End---------------------------------------------------->

      // <------------------------NewAccount Strat---------------------------------------------------->
      NewAccountcontainer: {
        flex: 1,
        backgroundColor: '#7F3DFF',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
      },
      NewAccountBalancePaisa: {
        position: 'absolute',
        bottom: hp('40%'), // Use heightPercentageToDP for height responsiveness
        zIndex: 1,
        paddingHorizontal: wp('5%') // Use widthPercentageToDP for width responsiveness
      },
      NewAccountBalance: {
        fontSize: wp('5%'), // Use widthPercentageToDP for font size responsiveness
        color: '#FFFFFF'
      },
      NewAccountpaisa: {
        color: 'white',
        fontWeight: '600',
        fontSize: wp('10%') // Use widthPercentageToDP for font size responsiveness
      },
      NewAccountbox: {
        backgroundColor: '#FFFFFF',
        borderTopLeftRadius: wp('5%'), // Use widthPercentageToDP for border radius responsiveness
        borderTopRightRadius: wp('5%'), // Use widthPercentageToDP for border radius responsiveness
        borderWidth: 1,
        borderColor: 'gray',
        position: 'absolute',
        width: '100%',
        bottom: 0
      },
      NewAccountInput: {
        paddingVertical: hp('1.5%'), // Use heightPercentageToDP for height responsiveness
        paddingHorizontal: wp('5%'), // Use widthPercentageToDP for width responsiveness
        marginHorizontal: wp('5%'), // Use widthPercentageToDP for margin responsiveness
        marginVertical: hp('2%'), // Use heightPercentageToDP for margin responsiveness
        color: 'black',
        borderWidth: 1,
        borderRadius: wp('2%'), // Use widthPercentageToDP for border radius responsiveness
        marginBottom: hp('2%'), // Use heightPercentageToDP for margin responsiveness
        borderColor: 'lightgrey'
      },
      NewAccounttypeDropdown: {
        paddingVertical: hp('2%'), // Use heightPercentageToDP for height responsiveness
        paddingHorizontal: wp('5%'), // Use widthPercentageToDP for width responsiveness
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        color: 'black',
        borderWidth: 1,
        borderColor: 'lightgrey',
        borderRadius: wp('2%'), // Use widthPercentageToDP for border radius responsiveness
        margin: wp('5%'), // Use widthPercentageToDP for margin responsiveness
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      },
      NewAccounttypeText: {
        color: 'black'
      },
      NewAccountdropdownItem: {
        paddingVertical: hp('2%'), // Use heightPercentageToDP for height responsiveness
        paddingHorizontal: wp('10%') // Use widthPercentageToDP for width responsiveness
      },
      NewAccountcontinueButton: {
        backgroundColor: '#7F3DFF',
        borderRadius: wp('2%'), // Use widthPercentageToDP for border radius responsiveness
        paddingVertical: hp('2%'), // Use heightPercentageToDP for height responsiveness
        marginHorizontal: wp('5%'), // Use widthPercentageToDP for margin responsiveness
        alignItems: 'center',
        marginBottom: hp('3%') // Use heightPercentageToDP for margin responsiveness
      },
      NewAccountcontinueButtonText: {
        color: 'white',
        fontSize: wp('5%') // Use widthPercentageToDP for font size responsiveness
      },
       // <------------------------NewAccount End---------------------------------------------------->


        // <------------------------Wallet Start---------------------------------------------------->
        Walletcontainer: {
          flex: 1,
          backgroundColor: '#7F3DFF',
          justifyContent:'left',
          alignItems:'flex-start'
        },
        WalletBalancepaisa:{
        position:'absolute',
        bottom:290,
        zIndex:1,
        paddingHorizontal:20
        },
       
        
        WalletBalance: {
          fontSize:20,
          color:'#FFFFFF'
        },
        Walletpaisa: {
          color: 'white',
          fontWeight: '600',
          fontSize: 40,
        
        },
        Walletbox: {
          backgroundColor: '#FFFFFF',
          borderTopLeftRadius: 22,
          borderTopRightRadius: 22,
          borderWidth: 1,
          borderColor: 'gray',
          width:'100%',
          position:'absolute',
          bottom:0,
        
        
        },
        WalletName: {
       
          paddingVertical: 16,
          paddingHorizontal: 16,
          marginHorizontal:20,
          marginVertical:35,
          color: 'black',
          borderWidth: 1, 
          borderRadius: 8,
          marginBottom: 10,
          borderColor:'lightgrey',
        },
        Walletcaret:{
        fontSize:20,
        
        },
        WallettypeDropdown: {
          paddingVertical: 18,
          paddingHorizontal: 16,
         
          borderWidth: 1, 
          borderColor:'lightgrey', 
          borderRadius: 8,
          margin:20,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        },
        WallettypeText: {
          // color: 'black',
        },
        Walletdropdown: {
          maxHeight: 170,
          borderWidth: 1,
          borderColor:'lightgrey',
          borderRadius: 8,
          margin:20,
          position:'relative',
          paddingVertical:20,
        },
        WalletdropdownItem: {
          flexDirection: 'row',
          alignItems: 'center',
          textAlign:'center',
          backgroundColor:'#FFFFFF',
          padding:10,
          borderRadius:20,
          marginVertical:5,
          zIndex:-1,
          width:"20%",
          height:45,
         //shadow properties
         shadowColor:'#000',
         shadowOffset:{
          width:0,
          height:2
         },
         shadowOpacity:0.5,
         shadowRadius: 3.84,
         elevation: 5,
        },
        WalletbankImage: {
          maxWidth:30,
          paddingLeft:15,
          paddingRight:15,
          height: 25,
          margin: 8,
          flexDirection:'row',
          borderRadius:15,

          

        },
        WalletrowContainer: {
          flexDirection: 'row',
          gap:20,
          paddingBottom:40,
          flexWrap:'wrap',
          alignItems: 'center',
          justifyContent: 'center',
        },
        WalletcontinueButton: {
          backgroundColor: '#7F3DFF',
          borderRadius: 8,
          paddingVertical: 15,
          marginHorizontal: 16,
          alignItems: 'center',
       
          marginBottom: 20,
        },
        WalletcontinueButtonText: {
          color: '#FFFFFF',
          fontSize: 18,
        },
         // <------------------------Wallet End---------------------------------------------------->
        
        // <-------------------------Setok Start-------------------------------------------------->
         Setcontainer: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor:'#FFFFFF'
        },
        setimage: {
          width: 128,
          height: 128,
        },
        set1: {
          fontSize: 24,
          fontWeight: '500',
          marginTop: 16,
        },
        gotoLogin:{
         backgroundColor:'#00a86b',
         marginVertical:20,
         paddingVertical:10,
         paddingHorizontal:15,
         borderRadius:10
        },
        gotoLoginText:{
         color:'#fff'
         },

        // Budget Empty Start
        // <-------------------------Setok End-------------------------------------------------->

       // <-------------------------BudgetEmpty Start-------------------------------------------------->
       Emptycontainer: {
        flex: 1,
        backgroundColor: '#7F3DFF',
        justifyContent: 'center',
    },
    Emptyheader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        top: 30,
        padding: 20,
        textAlign: 'center',
        alignItems: 'center',
    },
    Emptymonth: {
        fontSize: 23,
        color: '#ffffff',
    },
    Emptyicons: {
        fontSize: 22,
        color: '#ffffff',
    },
    Emptybody: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        top: 70,
        flex: 1,
        padding: 20,
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
    },
    Emptybody1: {
        alignItems: 'center',
        textAlign: 'center'
    },
    Emptylet: {
        fontSize: 18,
        color: '#91919f',
    },
    Emptybutton: {
        backgroundColor: '#7f3dff',
        width: '100%',
        position: 'absolute',
        bottom: 120,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        borderRadius: 20,
     },
    EmptybuttonText: {
        textAlign: 'center',
        fontSize: 17,
        color: '#FFFFFF',
        fontWeight: '600',
    },
       // <-------------------------BudgetEmpty End-------------------------------------------------->


       // <------------------------- Create_budget Start-------------------------------------------------->
       Create_budgetcontainer: {
        flex: 1,
        backgroundColor: '#7F3DFF',
    },
    Create_budgetheader: {
        flexDirection: 'row',
        marginTop: hp('5%'),
        paddingHorizontal: wp('5%'),
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    Create_budgetleft: {
        fontSize: wp('4%'),
        color: '#fff',
        textAlign: 'center'
    },
    Create_budgetheadertext: {
        color: '#fff',
        paddingHorizontal: wp('5%'),
        fontSize: wp('4%'),
    },
    Create_budgetspend: {
        marginTop: hp('10%'),
        opacity: 0.6,
        fontSize: wp('4%'),
        color: '#fcfcfc'
    },
    Create_budgetamount: {
        paddingHorizontal: wp('5%'),
        paddingVertical: hp('5%')
    },
    Create_budgetamount1: {
        fontSize: wp('10%'),
        color: '#fff',
    },
    Create_budgetbottom: {
        backgroundColor: '#ffffff',
        flex: 1,
        padding: wp('5%'),
        borderTopLeftRadius: wp('5%'),
        borderTopRightRadius: wp('5%'),
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },
    Create_budgetdropdown: {
        paddingVertical: hp('2%'),
        paddingHorizontal: wp('5%'),
        borderRadius: wp('2%'),
    },
    Create_budgettoogle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: wp('2%'),
        marginBottom: hp('5%'),
    },  
    Create_budgetalert: {
        fontSize: wp('4%'),
        color: '#000',
    },
    Create_budgetalert1: {
        fontSize: wp('3%'),
        color: '#808080',
    },
    Create_budgetswitch: {
        marginHorizontal: wp('1%'),
    },
    Create_budgetbutton: {
        backgroundColor: '#7f3dff',
        width: '100%',
        paddingVertical: hp('3%'),
        borderRadius: wp('5%'),
        marginBottom: hp('2%'),
    },
    Create_budgetbuttonText: {
        textAlign: 'center',
        fontSize: wp('4%'),
        color: '#fff',
        fontWeight: '600'
    },
    Create_budgetplaceholderStyle: {
      fontSize: 1,
      color: '#808080',
      marginLeft:wp('-2.5%')
  },  
  Create_budgetselectedTextStyle: {
      fontSize: 16,
      marginLeft:wp('-2.5%')

  },
  Create_budgetinputSearchStyle: {
      // height: 40,
      // fontSize: 16,
  },
       // <------------------------- Create_budget End-------------------------------------------------->

        // <------------------------- BudgerAfter Start-------------------------------------------------->
        Budgetcontainer: {
          flex: 1,
          backgroundColor: '#7F3DFF',
          justifyContent: 'center',
      },
      Budgetheader: {
          flexDirection: 'row',
          justifyContent: 'space-between',
          top: 50,
          padding: 20,
          textAlign: 'center',
          alignItems: 'center',
      },
      Budgetmonth: {
          fontSize: 23,
          color: '#ffffff',
      },
      Budgeticons: {
          fontSize: 22,
          color: '#ffffff',
      },
      Budgetbody: {
          backgroundColor: '#fcfcfc',
          top: 70,
          flex: 1,
          borderTopLeftRadius: 24,
          borderTopRightRadius: 24,
          padding: 10,
      },
      Budgetcard1: {
          backgroundColor: '#FFF',
          height: 200,
          padding: 16,
          borderRadius: 16,
      },
      Budgetbody1: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
      },
      Budgetshopping: {
          backgroundColor: '#f5f5f5',
          padding: 5,
          borderRadius: 50,
          fontSize: 16,
          width: 110,
          height: 35,
          textAlign: 'center'
      },
      Budgetdot: {
          fontSize: 15,
          color: '#ffa500',
      },
      Budgetbody11: {
          // paddingLeft: 12,
          // position: 'absolute',
          paddingVertical: 15,
          // padding: 15
          
      },
      BudgetRemaining: {
          fontSize: 22,
          fontWeight: 'bold'
      },
      Budgetslider: {
          width: 350,
          top: 5,
      },
      Budgetamount: {
          top: 0,
          fontSize: 16,
          color: '#808080'
      },
      Budgetlimit: {
          fontSize: 16,
          color: '#ff0000',
          paddingVertical: 8
      },
  
      Budgetcard2: {
          backgroundColor: '#FFF',
          padding: 16,
          gap: 8,
          borderRadius: 16,
          marginVertical: 10,
      },
      Budgetbody2: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
      },
      Budgettransport: {
          backgroundColor: '#f5f5f5',
          padding: 5,
          borderRadius: 50,
          fontSize: 16,
          width: 150,
          height: 35,
          textAlign: 'center'
      },
      Budgetdot1: {
          fontSize: 15,
          color: '#0077ff',
      },
      Budgetline1: {
          width: 346,
          height: 12,
          backgroundColor: '#0000FF',
          borderRadius: 30,
          top: 5,
      },
      Budgetbutton: {
          backgroundColor: '#7f3dff',
          width: '100%',
          bottom: 90,
          position: 'absolute',
          borderRadius: 20,
          padding: 20,
          paddingVertical: 20,
          marginHorizontal: 10
      },
      BudgetbuttonText: {
          textAlign: 'center',
          fontSize: 16,
          color: '#fff',
          fontWeight:'600',
      },
       // <-------------------------BudgerAfter End-------------------------------------------------->

        // <-------------------------Detail_budget Start-------------------------------------------------->
        Detail_budgetcontainer: {
          flex: 1,
          padding: 20,
          backgroundColor:'#ffffff',
      },
      Detail_budgetheader: {
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 50,
          textAlign: 'center',
          alignItems: 'center',
         
      },
      Detail_budgetheadertext: {
          color: '#000',
          fontSize: 20,
      },
      Detail_budgeticons: {
          fontSize: 20,
          color: '#000',
      },
      Detail_budgetshopkart: {
          alignItems: 'center',
          justifyContent:'center',
          width:'100%',
          
      },
      Detail_budgetshopping: {
          backgroundColor: '#fcfcfc',
          borderRadius: 50,
          fontSize: 18,
          textAlign: 'center',
          paddingVertical: 15,
          paddingHorizontal: 25,
          borderColor:'lightgrey',
          borderWidth:1,
      },
      Detail_budgetbag: {
          fontSize:20,
          color: '#fcac12',
        
      },
      Detail_budgetRemainamount: {
          alignItems: 'center',
          marginVertical: 50,
      },
      Detail_budgetRemaining1: {
          fontSize: 28,
          fontWeight: 'bold',
          textAlign: 'center',
          alignItems: 'center'
      },
      Detail_budgetamnt: {
          fontSize: 80,
          fontWeight: 'bold',
          marginHorizontal: 50
      },
      Detail_budgetslider: {
           bottom: 20,
          justifyContent: 'center',
          marginHorizontal: 10
      },
      Detail_budgetamount: {
          marginHorizontal:0,
          marginVertical:10
          
      },
      limitTextofdetail:{
       fontSize:18,
       color:"#ffffff",
       marginLeft:10
      },
      Detail_budgetlimit: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        paddingVertical:10,
        paddingHorizontal:60,
        borderRadius: 50,
      },
      Detail_budgetbutton: {
          backgroundColor: '#7f3dff',
          width: '100%',
          position: 'absolute',
          borderRadius: 20,
          paddingVertical: 20,
          marginHorizontal: 20,
          bottom: 20,
      },
      Detail_budgetbuttonText: {
          textAlign: 'center',
          alignItems: 'center',
          fontSize: 16,
          color: '#fff',
          fontWeight:'600'
      },
       // <-------------------------Detail_budget  End-------------------------------------------------->

        // <------------------------- Edit_Budjet Start-------------------------------------------------->
        Edit_Budgetcontainer: {
          flex: 1,
          backgroundColor: '#7F3DFF',
      },
      Edit_Budgetheader: {
          flexDirection: 'row',
          marginVertical: 44,
          padding: 16,
          gap: 76,
      },
      Edit_Budgetleft: {       
          fontSize: 20,
          color: '#fff',
          alignItems: 'center',
          textAlign: 'center'
      },
      Edit_Budgetheadertext: {
          color: '#fff',
          paddingHorizontal: 40,
          fontSize: 18,
      },
      Edit_Budgetamount:{ 
          paddingHorizontal: 26,
          paddingVertical: 50  
      },
      Edit_Budgetspend: {
          top: 100,
          marginVertical: 100,
          opacity: 0.6,
          fontSize: 18,
          color: '#fcfcfc'
      },
      Edit_Budgetamount1: {
          fontSize: 64,
          color: '#fff',
          
      },
      Edit_Budgetamount2: {
          fontSize: 64,
          color: '#fff',
          width: 200,
          height: 77,
      },
      Edit_Budgetbottom: {
          backgroundColor: '#fff',
          flex: 1,
          padding: 20,
          borderTopLeftRadius: 32,
          borderTopRightRadius: 32,
          gap: 10,
      },
      Edit_Budgetdropdown: {
          paddingVertical: 15,
          gap: 16,
          borderColor: '#808080',
          borderWidth: 0.5,
          padding: 10,
          borderRadius: 10,
      },
      Edit_Budgeticon: {
          marginHorizontal: 8,
      },
      Edit_BudgetplaceholderStyle: {
          fontSize: 19,
          color: '#808080'
      },
      Edit_BudgetselectedTextStyle: {
          fontSize: 16,
      },
      Edit_BudgeticonStyle: {
          width: 35,
          height: 35,
      },
      Edit_BudgetinputSearchStyle: {
          height: 40,
          fontSize: 16,
      },
      Edit_Budgettoogle: {
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
          letterSpacing: 1
      },
      Edit_Budgetalert: {
          fontSize: 18,
          color: '#000',
      },
      Edit_Budgetalert1: {
          fontSize: 15,
          color: '#808080',
      },
      Edit_Budgetswitch: {
          marginHorizontal: 5,
          marginVertical: 5,
      },
      Edit_Budgetslider: {
          paddingVertical: 75,
          position: 'absolute',
      },
      Edit_Budgetbutton: {
          backgroundColor: '#7f3dff',
          width: '100%',
          paddingVertical: 20,
          marginHorizontal: 20,
          position: 'absolute',
          borderRadius: 20,
          bottom: 20,
      },
      Edit_BudgetbuttonText: {
          textAlign: 'center',
          fontSize: 16,
          color: '#fff',
          fontWeight:'600'
      },
       // <------------------------- Edit_Budget End-------------------------------------------------->

        // <-------------------------Remove_budget Start-------------------------------------------------->
        Remove_budgetcontainer: {
          flex: 1,
          padding: 20
      },
      Remove_budgetheader: {
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 50,
          textAlign: 'center',
          alignItems: 'center',
      },
      Remove_budgetheadertext: {
          color: '#000',
          fontSize: 20,
      },
      Remove_budgeticons: {
          fontSize: 20,
          color: '#000',
      },
      Remove_budgetshopkart: {
          alignItems: 'center'
      },
      Remove_budgetshopping: {
          backgroundColor: '#d3d3d3',
          borderRadius: 50,
          fontSize: 22,
          textAlign: 'center',
          paddingVertical: 15,
          paddingHorizontal: 15
      },
      Remove_budgetbag: {
          fontSize: 22,
          color: '#ffa500',
          fontWeight: 'bold',
      },
      Remove_budgetRemainamount: {
          alignItems: 'center',
          marginVertical: 50,
      },
      Remove_budgetRemaining1: {
          fontSize: 28,
          fontWeight: 'bold',
          textAlign: 'center',
          alignItems: 'center'
      },
      Remove_budgetamnt: {
          fontSize: 80,
          fontWeight: 'bold',
          marginHorizontal: 50
      },
      Remove_budgetslider: {
          width: 346,
          bottom: 20,
          marginHorizontal: 10
      },
      Remove_budgetamount: {
          marginHorizontal: 20,
      },
      Remove_budgetlimit: {
          color: '#fff',
          marginVertical: 10,
          width: 290,
          fontSize: 20,
          textAlign: 'center',
          alignItems:'center',
          padding: 10,
          borderRadius: 50,
      },
      Remove_budgetbutton: {
          backgroundColor: '#7f3dff',
          width: '100%',
          position: 'absolute',
          borderRadius: 20,
          paddingVertical: 20,
          marginHorizontal: 20,
          bottom: 20,
      },
      Remove_budgetbuttonText: {
          textAlign: 'center',
          fontSize: 16,
          color: '#fff',
          fontWeight:'600'
      },
       // <-------------------------Remove_budget End-------------------------------------------------->
     
       containertabhome: {
        flex: 1,
        backgroundColor:'#ffffff'
      },
      ThreeSectionsJoin:{
        backgroundColor:'#fdf7eb',
        borderBottomRightRadius: wp(10),
        borderBottomLeftRadius: wp(10),
      },
      topSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wp(5),
        paddingTop: hp(3),
      },
      profileImage: {
        width: wp(12),
        height: wp(12),
        borderRadius: wp(6),
        borderWidth: wp(0.5),
        borderColor:'#ad00ff'
      },
      textDrop: {
        fontSize: wp(5),
        marginRight: wp(7.5),
        backgroundColor: '#fdf7ea',
        borderWidth: wp(0.25),
        paddingVertical: hp(1.25),
        paddingHorizontal: wp(5),
        borderColor:'#f1f1fa',
        borderRadius: wp(5),
      },
      userBell:{
        fontSize: wp(6.25),
        color:'#7f3dff'
      },
      secondSection: {
        paddingHorizontal: wp(5),
        paddingTop: hp(1.25),
        alignItems:'center',
        gap: hp(0.875),
      },
      incomeexpenses:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        gap: wp(2.5),
        width: '100%',
        paddingHorizontal: wp(7.5),
        paddingVertical: hp(2.5),
      },
      incomeIcon:{
        fontSize: wp(10),
        color:'#00a86b',
        backgroundColor:'#ffffff',
        borderRadius: wp(5),
        padding: wp(1.25),
      },
      expenseIcon:{
        fontSize: wp(10),
        color:'#fd3c4a',
        backgroundColor:'#ffffff',
        borderRadius: wp(5),
        padding: wp(1.25),
      },
      incomeCard:{
        width: '50%',
        backgroundColor:'#00a86b',
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        gap: wp(2.5),
        borderRadius: wp(7.5),
      },
      expensesCard:{
        width: '50%',
        backgroundColor:'#fd3c4a',
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        gap: wp(2.5),
        padding: wp(3.25),
        borderRadius: wp(7.5),
      },
      incomeCardIncome:{
        fontSize: wp(3.75),
        fontWeight:'500',
        color:'#ffffff'
      },
      incomeCardMoney:{
        fontSize: wp(6.25),
        fontWeight:'500',
        color:'#ffffff'
      },
      expensesCardExpense:{
        fontSize: wp(3.75),
        fontWeight:'500',
        color:'#ffffff'
      },
      expensesCardMoney:{
        fontSize: wp(6.25),
        fontWeight:'500',
        color:'#ffffff'
      },
      balanceText: {
        fontSize: wp(4.5),
        color:'#91919f',
        fontWeight:'500'
      },
      balanceAmount: {
        fontSize: wp(10),
        fontWeight:'700'
      },
      thirdSection: {
        paddingHorizontal: wp(0),
        paddingTop: hp(2.5),
      },
      spendFrequencyText: {
        fontSize: wp(5),
        fontWeight:'500',
        paddingHorizontal: wp(5),
      },
      fourthSection: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: hp(2.5),
      },
      periodButton: {
        fontSize: wp(4),
        paddingHorizontal: wp(3.75),
        paddingVertical: hp(0.625),
        borderRadius: wp(5),
      },
      RecentTransaction:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal: wp(7.5),
        paddingVertical: hp(2.5),
      },
      RecentTransactionText:{
        fontSize: wp(5),
        fontWeight:'500',
      },
      RecentTransactionSeeAll:{
        fontSize: wp(4),
        backgroundColor: '#eee5ff',
        paddingHorizontal: wp(3.75),
        paddingVertical: hp(0.625),
        borderRadius: wp(5),
        color:'#7f3dff'
      },
      listTransaction:{
        backgroundColor:'#fcfcfa',
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal: wp(5),
        paddingVertical: hp(1.825),
        borderRadius: wp(7.5),
      },
      iconwithlabdes:{
        flexDirection:'row',
        gap: wp(2.5),
      },
      TransactionIcon:{
        color:'#00a86b',
        backgroundColor:'#ffffff',
        borderRadius: wp(5),
        padding: wp(1.25),
        borderColor:'#f1f1fa',
        borderWidth: wp(0.25),
      },
      shopping:{
        fontSize: wp(8),
        color:'#fcac12',
      },
      note:{
        fontSize: wp(8),
        color:'#7f3dff',
      },
      food:{
        fontSize: wp(8),
        color:'#fd3c4a',
      },
      TransactionLabDes:{
        gap: hp(1),
      },
      TransactionLable:{
        fontSize: wp(4),
        color:'#000000'
      },
      TransactionDescription:{
        color:'#91919f',
        fontSize: wp(3),
      },
      TransactionMoneyTime:{
        gap: hp(1),
      },
      TransactionMoney:{
        fontSize: wp(4),
        color:'#fd3c4a'
      },
      TransactionTime:{
        color:'#91919f',
        fontSize: wp(3),
      },
    
    
      
        // <------------------------- AppTABHome Start-------------------------------------------------->

       // <------------------------- AppTABHome  End-------------------------------------------------->
       // <------------------------- Expense Start-------------------------------------------------->
       
       // <------------------------- Expense End-------------------------------------------------->
       // <------------------------- Start-------------------------------------------------->
       // <------------------------- End-------------------------------------------------->
       // <------------------------- Start-------------------------------------------------->
       // <------------------------- End-------------------------------------------------->
       // <------------------------- Start-------------------------------------------------->
       // <------------------------- End-------------------------------------------------->

   
   
  //  -----------------------------------------------------------addattachmentexpense ------------------------------------------------------------
  DetailTransactionExpenseTopHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor:'#d73541',
    padding:20
  },
  arrowleftDeatailExpense: {
    fontSize: hp('2.5%'),
    color: '#fff',
  },
  DetailTransactionExpenseText: {
    fontSize: hp('2.5%'),
    color: '#fff',
  },
  trashDeatailExpense: {
    fontSize: hp('2.5%'),
    color: '#fff',
  },
  Walletcontainermain:{
    flex: 1,
    backgroundColor: '#d73541',
  },
  Walletcontainer: {
    position:'absolute',
    bottom:0,
    width:"100%"
  },
  WalletBalancepaisa: {
    padding: wp('5%')
  },
  WalletBalance: {
    fontSize: wp('5%'),
    color: '#FFFFFF'
  },
  Walletpaisa: {
    color: 'white',
    fontWeight: '600',
    fontSize: wp('10%'),
  },
  Walletbox: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: wp('5%'),
    borderTopRightRadius: wp('5%'),
    borderWidth: 1,
    borderColor: 'gray',
    width: '100%',
  },
  NewAccountInput: {
    paddingVertical: hp('2%'), 
    paddingHorizontal: wp('5%'), 
    marginHorizontal: wp('5%'),
    color: 'black',
    borderWidth: 1,
    borderRadius: wp('2%'), 
    borderColor: 'lightgrey'
  },
  WallettypeDropdown: {
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    borderWidth: 1,
    borderColor: 'lightgrey',
    borderRadius: wp('2%'),
    margin: wp('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  WallettypeDropdown1: {
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    borderWidth: 1,
    borderColor: 'lightgrey',
    borderRadius: wp('2%'),
    marginHorizontal: wp('5%'),
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  Walletdropdown1: {
    marginHorizontal: wp('10%'),
    marginBottom: hp('1%')
  },
  dotcategory: {
    width: 10,
    height: 10,
    borderRadius:20,
    marginRight:10,
    backgroundColor: 'green',
  },
  selectedCategoryContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'lightgrey',
    borderWidth: 1,
    borderRadius: wp('50%'),
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('.5%'),
  },
  Walletdropdown2: {
    marginHorizontal: wp('10%'),
  },
  WalletdropdownItem: {
    //
  },
  WalletrowContainer: {
    flexDirection: 'column',
    gap: hp('2%'),
    paddingBottom: 0,
    flexWrap: 'wrap',
    alignItems: 'left',
    justifyContent: 'center',
  },
  WalletcontinueButton: {
    backgroundColor: '#7F3DFF',
    borderRadius: wp('2%'),
    paddingVertical: hp('2%'),
    marginHorizontal: wp('5%'),
    alignItems: 'center',
    marginBottom: hp('2%'),
  },
  WalletcontinueButtonText: {
    color: '#FFFFFF',
    fontSize: wp('4%'),
  },
  box: {
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    margin: wp('5%'),
    paddingVertical: hp('1.7%'),
    alignItems: 'center',
    borderRadius: wp('2%'),
    borderColor: 'gray',
    borderWidth: 1,
    borderStyle: 'dashed'
  },
  attachmentButton: {
    flexDirection: 'row',
    gap: wp('2%'),
    textAlign: 'center',
  },
  addAttachmentText: {
    color: 'grey'
  },
  smallText: {
    fontSize: wp('3%'),
    color: '#888',
  },
  itemText: {
    fontSize: wp('4%'),
    color: 'black',
  },
  itemno: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: wp('10%'),
    marginBottom: hp('2%')
  },
  toggleIcon: {
    fontSize: wp('8%'),
  },
  // modal
  attachmentButton: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp('2%'),
  },
  addAttachmentText: {
    marginLeft: wp('2%'),
    color: 'grey',
    fontSize: wp('4%'),
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#fff',
    borderTopLeftRadius: wp('5%'),
    borderTopRightRadius: wp('5%'),
    paddingHorizontal: wp('5%'),
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between'
  },
  iconContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor:'#eee5ff',
    width: wp('25%'),
    height: hp('12%'),
    justifyContent:'center',
    marginBottom: hp('6%'),
    marginTop: hp('8%'),
    borderRadius: wp('5%'),
  },
  iconText: {
    marginTop: hp('1%'),
    color:'#7f3dff',
    fontSize: wp('4%'),
    fontWeight:'600'
  },

  // image Attach
  imageAttach:{
    flex: 1, 
    alignItems:'flex-start',
    justifyContent: 'center',
    marginHorizontal:30,
    marginVertical:10,
    borderRadius:10

  },
  AttachImage:{
    width:130,
    height:130,
    borderRadius:20,
    position:"relative",
    zIndex:-1

  },

  maincloseicon:{
    position:"absolute",
    marginLeft:110,
    top:-5,
    backgroundColor:"rgba(173,173,173,0.6)",
    width:"7%",
    padding:5, 
    textAlign:"center",
    borderRadius:40
    },

  // after next modal 
  
    //  next modal  inside 
    row: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      gap: wp('2%'),
    },
    dropdown: {
      width: (wp('100%') - wp('20%')) / 3,
      paddingVertical: hp('1%'),
    },
    dropdown2: {
      width: (wp('100%') - wp('20%')) / 2,
      paddingVertical: hp('1%'),
    },
    BarNextAfter: {
      height: hp('0.5%'),
      width: wp('10%'),
      backgroundColor: '#d3bdff',
      marginVertical: hp('1%'),
      alignSelf: 'center',
      borderRadius: wp('2%'),
    },
    Frequency: {
      paddingVertical: hp('2.2%'),
      backgroundColor: '#fff',
      width: '100%',
      flexDirection: 'row', 
      justifyContent: 'space-between', 
      alignItems: 'center', 
      paddingHorizontal: wp('5%'),
      marginVertical: hp('2%'),
      borderWidth:1,
      borderColor:'#91919f',
      borderRadius: wp('2%'),
    },
    FrequencyEndafter:{
      backgroundColor: '#fff',
      width: '100%',
      flexDirection: 'row', 
      justifyContent: 'space-between', 
      alignItems: 'center', 
      paddingHorizontal: wp('9.5%'),
      marginVertical: hp('2%'),
    },
    freendselect:{
      gap:5
    },
    freendselectText1:{
     color:'#000',
     fontSize:17,
     fontWeight:'400'
    },
    freendselectText2:{
      color:'#91919f',
      fontWeight:'400'
     },
     freendselectEdit:{
      backgroundColor:'#eee5ff',
      paddingVertical:8,
      paddingHorizontal:18,
      borderRadius:25
     },
     freendselectTextEdit:{
      color:'#7f3dff',
      fontSize:12,
      fontWeight:'500'
     },
    Datepicker:{
      width: wp('40%'),
      flexDirection:"row",
      borderWidth:1,
      borderColor:'#91919f',
      alignItems:'center',
      justifyContent:'space-between',
      paddingVertical: hp('1.5%'),
      marginBottom: hp('1.5%'),
      borderRadius: wp('2%'),
      paddingHorizontal: wp('3%'),
    },
    DatepickerDisplay:{
      width: wp('50%'),
      borderWidth:1,
      borderColor:'#91919f',
      alignItems:'center',
      paddingVertical: hp('1.5%'),
      marginBottom: hp('1.5%'),
      borderRadius: wp('2%'),
    },
    DatepickerDisplayText:{
      color:'#7f3dff'
    },
    nextbuttonAfter:{
      backgroundColor:'#7f3dff',
      paddingVertical: hp('2%'),
      marginVertical: hp('1.5%'),
      borderRadius: wp('2%'),
      alignItems:'center',
    },
    nextbuttonAfterText:{
      fontSize: hp('2.2%'),
      fontWeight:'500',
      color:'#fff'
    },
    //   Alert
overlay: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
},
customAlertContainer: {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
  justifyContent: 'center',
  alignItems: 'center',
},
customAlert: {
  backgroundColor: '#fff',
  padding: wp('5%'), 
  borderRadius: hp('2%'), 
  alignItems: 'center',
},
alertIcon: {
  fontSize: hp('7%'), 
  color: '#5233ff',
  marginBottom: hp('1%'), 
},
alertText: {
  fontSize: hp('2%'), 
  textAlign: 'center',
},
alertTextOK:{
  backgroundColor: '#5233ff',
    color: '#fff',
    paddingHorizontal: wp('4%'), 
    paddingVertical: hp('1.5%'), 
    marginTop: hp('1%'), 
    borderRadius: wp('2.5%'),
    fontWeight:'600',
},
// ------------------------------------------addattachmentinc0me------------------------------------------------------
DetailTransactionExpenseTopHead: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  backgroundColor:'#00a86b',
  padding:20
},
arrowleftDeatailExpense: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionExpenseText: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
trashDeatailExpense: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
Walletcontainermain:{
  flex: 1,
  backgroundColor: '#00a86b',
},
Walletcontainer: {
  position:'absolute',
  bottom:0,
  width:"100%"
},
WalletBalancepaisa: {
  padding: wp('5%')
},
WalletBalance: {
  fontSize: wp('5%'),
  color: '#FFFFFF'
},
Walletpaisa: {
  color: 'white',
  fontWeight: '600',
  fontSize: wp('10%'),
},
Walletbox: {
  backgroundColor: '#FFFFFF',
  borderTopLeftRadius: wp('5%'),
  borderTopRightRadius: wp('5%'),
  borderWidth: 1,
  borderColor: 'gray',
  width: '100%',
},
NewAccountInput: {
  paddingVertical: hp('2%'), 
  paddingHorizontal: wp('5%'), 
  marginHorizontal: wp('5%'),
  color: 'black',
  borderWidth: 1,
  borderRadius: wp('2%'), 
  borderColor: 'lightgrey'
},
WallettypeDropdown: {
  paddingVertical: hp('2%'),
  paddingHorizontal: wp('5%'),
  borderWidth: 1,
  borderColor: 'lightgrey',
  borderRadius: wp('2%'),
  margin: wp('5%'),
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
},
WallettypeDropdown1: {
  paddingVertical: hp('2%'),
  paddingHorizontal: wp('5%'),
  borderWidth: 1,
  borderColor: 'lightgrey',
  borderRadius: wp('2%'),
  marginHorizontal: wp('5%'),
  marginTop: hp('2%'),
  marginBottom: hp('1%'),
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
},
Walletdropdown1: {
  marginHorizontal: wp('10%'),
  marginBottom: hp('1%')
},
dotcategory: {
  width: 10,
  height: 10,
  borderRadius:20,
  marginRight:10,
  backgroundColor: 'green',
},
selectedCategoryContainer:{
  flexDirection: 'row',
  alignItems: 'center',
  borderColor: 'lightgrey',
  borderWidth: 1,
  borderRadius: wp('50%'),
  paddingHorizontal: wp('5%'),
  paddingVertical: hp('.5%'),
},
Walletdropdown2: {
  marginHorizontal: wp('10%'),
},
WalletdropdownItem: {
  //
},
WalletrowContainer: {
  flexDirection: 'column',
  gap: hp('2%'),
  paddingBottom: 0,
  flexWrap: 'wrap',
  alignItems: 'left',
  justifyContent: 'center',
},
WalletcontinueButton: {
  backgroundColor: '#7F3DFF',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  marginHorizontal: wp('5%'),
  alignItems: 'center',
  marginBottom: hp('2%'),
},
WalletcontinueButtonText: {
  color: '#FFFFFF',
  fontSize: wp('4%'),
},
box: {
  justifyContent: 'center',
  backgroundColor: '#ffffff',
  margin: wp('5%'),
  paddingVertical: hp('1.7%'),
  alignItems: 'center',
  borderRadius: wp('2%'),
  borderColor: 'gray',
  borderWidth: 1,
  borderStyle: 'dashed'
},
attachmentButton: {
  flexDirection: 'row',
  gap: wp('2%'),
  textAlign: 'center',
},
addAttachmentText: {
  color: 'grey'
},
smallText: {
  fontSize: wp('3%'),
  color: '#888',
},
itemText: {
  fontSize: wp('4%'),
  color: 'black',
},
itemno: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginHorizontal: wp('10%'),
  marginBottom: hp('2%')
},
toggleIcon: {
  fontSize: wp('8%'),
},
// modal
attachmentButton: {
  flexDirection: 'row',
  alignItems: 'center',
  paddingHorizontal: wp('2%'),
},
addAttachmentText: {
  marginLeft: wp('2%'),
  color: 'grey',
  fontSize: wp('4%'),
},
modalContainer: {
  flex: 1,
  justifyContent: 'flex-end',
  backgroundColor: 'rgba(0, 0, 0, 0.5)',
},
modalContent: {
  backgroundColor: '#fff',
  borderTopLeftRadius: wp('5%'),
  borderTopRightRadius: wp('5%'),
  paddingHorizontal: wp('5%'),
  display:'flex',
  flexDirection:'row',
  justifyContent:'space-between'
},
iconContainer: {
  flexDirection: 'column',
  alignItems: 'center',
  backgroundColor:'#eee5ff',
  width: wp('25%'),
  height: hp('12%'),
  justifyContent:'center',
  marginBottom: hp('6%'),
  marginTop: hp('8%'),
  borderRadius: wp('5%'),
},
iconText: {
  marginTop: hp('1%'),
  color:'#7f3dff',
  fontSize: wp('4%'),
  fontWeight:'600'
},

// image Attach
imageAttach:{
  flex: 1, 
  alignItems:'flex-start',
  justifyContent: 'center',
  marginHorizontal:30,
  marginVertical:10,
  borderRadius:10

},
AttachImage:{
  width:130,
  height:130,
  borderRadius:20,
  position:"relative",
  zIndex:-1

},

maincloseicon:{
  position:"absolute",
  marginLeft:110,
  top:-5,
  backgroundColor:"rgba(173,173,173,0.6)",
  width:"7%",
  padding:5, 
  textAlign:"center",
  borderRadius:40
  },

// after next modal 

  //  next modal  inside 
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: wp('2%'),
  },
  dropdown: {
    width: (wp('100%') - wp('20%')) / 3,
    paddingVertical: hp('1%'),
  },
  dropdown2: {
    width: (wp('100%') - wp('20%')) / 2,
    paddingVertical: hp('1%'),
  },
  BarNextAfter: {
    height: hp('0.5%'),
    width: wp('10%'),
    backgroundColor: '#d3bdff',
    marginVertical: hp('1%'),
    alignSelf: 'center',
    borderRadius: wp('2%'),
  },
  Frequency: {
    paddingVertical: hp('2.2%'),
    backgroundColor: '#fff',
    width: '100%',
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    paddingHorizontal: wp('5%'),
    marginVertical: hp('2%'),
    borderWidth:1,
    borderColor:'#91919f',
    borderRadius: wp('2%'),
  },
  FrequencyEndafter:{
    backgroundColor: '#fff',
    width: '100%',
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    paddingHorizontal: wp('9.5%'),
    marginVertical: hp('2%'),
  },
  freendselect:{
    gap:5
  },
  freendselectText1:{
   color:'#000',
   fontSize:17,
   fontWeight:'400'
  },
  freendselectText2:{
    color:'#91919f',
    fontWeight:'400'
   },
   freendselectEdit:{
    backgroundColor:'#eee5ff',
    paddingVertical:8,
    paddingHorizontal:18,
    borderRadius:25
   },
   freendselectTextEdit:{
    color:'#7f3dff',
    fontSize:12,
    fontWeight:'500'
   },
  Datepicker:{
    width: wp('40%'),
    flexDirection:"row",
    borderWidth:1,
    borderColor:'#91919f',
    alignItems:'center',
    justifyContent:'space-between',
    paddingVertical: hp('1.5%'),
    marginBottom: hp('1.5%'),
    borderRadius: wp('2%'),
    paddingHorizontal: wp('3%'),
  },
  DatepickerDisplay:{
    width: wp('50%'),
    borderWidth:1,
    borderColor:'#91919f',
    alignItems:'center',
    paddingVertical: hp('1.5%'),
    marginBottom: hp('1.5%'),
    borderRadius: wp('2%'),
  },
  DatepickerDisplayText:{
    color:'#7f3dff'
  },
  nextbuttonAfter:{
    backgroundColor:'#7f3dff',
    paddingVertical: hp('2%'),
    marginVertical: hp('1.5%'),
    borderRadius: wp('2%'),
    alignItems:'center',
  },
  nextbuttonAfterText:{
    fontSize: hp('2.2%'),
    fontWeight:'500',
    color:'#fff'
  },
  //   Alert
overlay: {
flex: 1,
justifyContent: 'center',
alignItems: 'center',
backgroundColor: 'rgba(0, 0, 0, 0.5)', 
},
customAlertContainer: {
position: 'absolute',
top: 0,
bottom: 0,
left: 0,
right: 0,
backgroundColor: 'rgba(0, 0, 0, 0.5)', 
justifyContent: 'center',
alignItems: 'center',
},
customAlert: {
backgroundColor: '#fff',
padding: wp('5%'), 
borderRadius: hp('2%'), 
alignItems: 'center',
},
alertIcon: {
fontSize: hp('7%'), 
color: '#5233ff',
marginBottom: hp('1%'), 
},
alertText: {
fontSize: hp('2%'), 
textAlign: 'center',
},
alertTextOK:{
backgroundColor: '#5233ff',
  color: '#fff',
  paddingHorizontal: wp('4%'), 
  paddingVertical: hp('1.5%'), 
  marginTop: hp('1%'), 
  borderRadius: wp('2.5%'),
  fontWeight:'600',
},
// ---------------------------------------------------AddAttachmentTransfer---------------------------------------------------------------
// DetailTransactionExpenseTopHead: {
//   flexDirection: 'row',
//   justifyContent: 'space-between',
//   backgroundColor:'#0077ff',
//   padding:20
// },
// arrowleftDeatailExpense: {
//   fontSize: hp('2.5%'),
//   color: '#fff',
// },
// DetailTransactionExpenseText: {
//   fontSize: hp('2.5%'),
//   color: '#fff',
// },
// trashDeatailExpense: {
//   fontSize: hp('2.5%'),
//   color: '#fff',
// },
// Walletcontainermain:{
//   flex: 1,
//   backgroundColor: '#0077ff',
// },
// Walletcontainer: {
//   position:'absolute',
//   bottom:0,
//   width:"100%"
// },
// WalletBalancepaisa: {
//   padding: wp('5%')
// },
// WalletBalance: {
//   fontSize: wp('5%'),
//   color: '#FFFFFF'
// },
// Walletpaisa: {
//   color: 'white',
//   fontWeight: '600',
//   fontSize: wp('10%'),
// },
// Walletbox: {
//   backgroundColor: '#FFFFFF',
//   borderTopLeftRadius: wp('5%'),
//   borderTopRightRadius: wp('5%'),
//   borderWidth: 1,
//   borderColor: 'gray',
//   width: '100%',
// },
// inputfromto:{
//   flexDirection:'row',
//   alignItems:'center',
//   marginHorizontal: wp('5%'),
//   position:'relative',
//   marginTop:hp('2%')
// },
// inputfrom: {
//   flex: 1,
//   paddingVertical: hp('1.5%'), 
//   paddingHorizontal: wp('5%'), 
//   marginRight:wp('1%'),
//   marginBottom:hp('2%'),
//   color: 'black',
//   borderWidth: 1,
//   borderRadius: wp('2%'), 
//   borderColor: 'lightgrey',
//   zIndex:-1
// },
// inputto:{
//   flex: 1,
//   paddingVertical: hp('1.5%'), 
//   paddingHorizontal: wp('5%'), 
//   marginLeft:wp('1%'),
//   marginBottom:hp('2%'),
//   color: 'black',
//   borderWidth: 1,
//   borderRadius: wp('2%'), 
//   borderColor: 'lightgrey',
//   zIndex:-1
//   },
// iconContainerexchange: {
//   justifyContent: 'center',
//   alignItems: 'center',
//   height:35,
//   width:35,
//   borderColor: 'lightgray',
//   borderWidth:1,
//   backgroundColor: '#fff',
//   borderRadius:wp('50%'),
//   position:'absolute',
//   right:wp('40%'),
//   top:hp('1%'),

//   },
// NewAccountInput: {
//   paddingVertical: hp('2%'), 
//   paddingHorizontal: wp('5%'), 
//   marginHorizontal: wp('5%'),
//   color: 'black',
//   borderWidth: 1,
//   borderRadius: wp('2%'), 
//   borderColor: 'lightgrey'
// },
// WallettypeDropdown: {
//   paddingVertical: hp('2%'),
//   paddingHorizontal: wp('5%'),
//   borderWidth: 1,
//   borderColor: 'lightgrey',
//   borderRadius: wp('2%'),
//   margin: wp('5%'),
//   flexDirection: 'row',
//   justifyContent: 'space-between',
//   alignItems: 'center',
// },
// WallettypeDropdown1: {
//   paddingVertical: hp('2%'),
//   paddingHorizontal: wp('5%'),
//   borderWidth: 1,
//   borderColor: 'lightgrey',
//   borderRadius: wp('2%'),
//   marginHorizontal: wp('5%'),
//   marginTop: hp('2%'),
//   marginBottom: hp('1%'),
//   flexDirection: 'row',
//   justifyContent: 'space-between',
//   alignItems: 'center',
// },
// Walletdropdown1: {
//   marginHorizontal: wp('10%'),
//   marginBottom: hp('1%')
// },
// dotcategory: {
//   width: 10,
//   height: 10,
//   borderRadius:20,
//   marginRight:10,
//   backgroundColor: 'green',
// },
// selectedCategoryContainer:{
//   flexDirection: 'row',
//   alignItems: 'center',
//   borderColor: 'lightgrey',
//   borderWidth: 1,
//   borderRadius: wp('50%'),
//   paddingHorizontal: wp('5%'),
//   paddingVertical: hp('.5%'),
// },
// Walletdropdown2: {
//   marginHorizontal: wp('10%'),
// },
// WalletdropdownItem: {
//   //
// },
// WalletrowContainer: {
//   flexDirection: 'column',
//   gap: hp('2%'),
//   paddingBottom: 0,
//   flexWrap: 'wrap',
//   alignItems: 'left',
//   justifyContent: 'center',
// },
// WalletcontinueButton: {
//   backgroundColor: '#7F3DFF',
//   borderRadius: wp('2%'),
//   paddingVertical: hp('2%'),
//   marginHorizontal: wp('5%'),
//   alignItems: 'center',
//   marginBottom: hp('2%'),
// },
// WalletcontinueButtonText: {
//   color: '#FFFFFF',
//   fontSize: wp('4%'),
// },
// box: {
//   justifyContent: 'center',
//   backgroundColor: '#ffffff',
//   margin: wp('5%'),
//   paddingVertical: hp('1.7%'),
//   alignItems: 'center',
//   borderRadius: wp('2%'),
//   borderColor: 'gray',
//   borderWidth: 1,
//   borderStyle: 'dashed'
// },
// attachmentButton: {
//   flexDirection: 'row',
//   gap: wp('2%'),
//   textAlign: 'center',
// },
// addAttachmentText: {
//   color: 'grey'
// },
// smallText: {
//   fontSize: wp('3%'),
//   color: '#888',
// },
// itemText: {
//   fontSize: wp('4%'),
//   color: 'black',
// },
// itemno: {
//   flexDirection: 'row',
//   justifyContent: 'space-between',
//   alignItems: 'center',
//   marginHorizontal: wp('10%'),
//   marginBottom: hp('2%')
// },
// toggleIcon: {
//   fontSize: wp('8%'),
// },
// // modal
// attachmentButton: {
//   flexDirection: 'row',
//   alignItems: 'center',
//   paddingHorizontal: wp('2%'),
// },
// addAttachmentText: {
//   marginLeft: wp('2%'),
//   color: 'grey',
//   fontSize: wp('4%'),
// },
// modalContainer: {
//   flex: 1,
//   justifyContent: 'flex-end',
//   backgroundColor: 'rgba(0, 0, 0, 0.5)',
// },
// modalContent: {
//   backgroundColor: '#fff',
//   borderTopLeftRadius: wp('5%'),
//   borderTopRightRadius: wp('5%'),
//   paddingHorizontal: wp('5%'),
//   display:'flex',
//   flexDirection:'row',
//   justifyContent:'space-between'
// },
// iconContainer: {
//   flexDirection: 'column',
//   alignItems: 'center',
//   backgroundColor:'#eee5ff',
//   width: wp('25%'),
//   height: hp('12%'),
//   justifyContent:'center',
//   marginBottom: hp('6%'),
//   marginTop: hp('8%'),
//   borderRadius: wp('5%'),
// },
// iconText: {
//   marginTop: hp('1%'),
//   color:'#7f3dff',
//   fontSize: wp('4%'),
//   fontWeight:'600'
// },

// // image Attach
// imageAttach:{
//   flex: 1, 
//   alignItems:'flex-start',
//   justifyContent: 'center',
//   marginHorizontal:30,
//   marginVertical:10,
//   borderRadius:10

// },
// AttachImage:{
//   width:130,
//   height:130,
//   borderRadius:20,
//   position:"relative",
//   zIndex:-1

// },

// maincloseicon:{
//   position:"absolute",
//   marginLeft:110,
//   top:-5,
//   backgroundColor:"rgba(173,173,173,0.6)",
//   width:"7%",
//   padding:5, 
//   textAlign:"center",
//   borderRadius:40
//   },

// // after next modal 

//   //  next modal  inside 
//   row: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     gap: wp('2%'),
//   },
//   dropdown: {
//     width: (wp('100%') - wp('20%')) / 3,
//     paddingVertical: hp('1%'),
//   },
//   dropdown2: {
//     width: (wp('100%') - wp('20%')) / 2,
//     paddingVertical: hp('1%'),
//   },
//   BarNextAfter: {
//     height: hp('0.5%'),
//     width: wp('10%'),
//     backgroundColor: '#d3bdff',
//     marginVertical: hp('1%'),
//     alignSelf: 'center',
//     borderRadius: wp('2%'),
//   },
//   Frequency: {
//     paddingVertical: hp('2.2%'),
//     backgroundColor: '#fff',
//     width: '100%',
//     flexDirection: 'row', 
//     justifyContent: 'space-between', 
//     alignItems: 'center', 
//     paddingHorizontal: wp('5%'),
//     marginVertical: hp('2%'),
//     borderWidth:1,
//     borderColor:'#91919f',
//     borderRadius: wp('2%'),
//   },
//   FrequencyEndafter:{
//     backgroundColor: '#fff',
//     width: '100%',
//     flexDirection: 'row', 
//     justifyContent: 'space-between', 
//     alignItems: 'center', 
//     paddingHorizontal: wp('9.5%'),
//     marginVertical: hp('2%'),
//   },
//   freendselect:{
//     gap:5
//   },
//   freendselectText1:{
//    color:'#000',
//    fontSize:17,
//    fontWeight:'400'
//   },
//   freendselectText2:{
//     color:'#91919f',
//     fontWeight:'400'
//    },
//    freendselectEdit:{
//     backgroundColor:'#eee5ff',
//     paddingVertical:8,
//     paddingHorizontal:18,
//     borderRadius:25
//    },
//    freendselectTextEdit:{
//     color:'#7f3dff',
//     fontSize:12,
//     fontWeight:'500'
//    },
//   Datepicker:{
//     width: wp('40%'),
//     flexDirection:"row",
//     borderWidth:1,
//     borderColor:'#91919f',
//     alignItems:'center',
//     justifyContent:'space-between',
//     paddingVertical: hp('1.5%'),
//     marginBottom: hp('1.5%'),
//     borderRadius: wp('2%'),
//     paddingHorizontal: wp('3%'),
//   },
//   DatepickerDisplay:{
//     width: wp('50%'),
//     borderWidth:1,
//     borderColor:'#91919f',
//     alignItems:'center',
//     paddingVertical: hp('1.5%'),
//     marginBottom: hp('1.5%'),
//     borderRadius: wp('2%'),
//   },
//   DatepickerDisplayText:{
//     color:'#7f3dff'
//   },
//   nextbuttonAfter:{
//     backgroundColor:'#7f3dff',
//     paddingVertical: hp('2%'),
//     marginVertical: hp('1.5%'),
//     borderRadius: wp('2%'),
//     alignItems:'center',
//   },
//   nextbuttonAfterText:{
//     fontSize: hp('2.2%'),
//     fontWeight:'500',
//     color:'#fff'
//   },
//   //   Alert
// overlay: {
// flex: 1,
// justifyContent: 'center',
// alignItems: 'center',
// backgroundColor: 'rgba(0, 0, 0, 0.5)', 
// },
// customAlertContainer: {
// position: 'absolute',
// top: 0,
// bottom: 0,
// left: 0,
// right: 0,
// backgroundColor: 'rgba(0, 0, 0, 0.5)', 
// justifyContent: 'center',
// alignItems: 'center',
// },
// customAlert: {
// backgroundColor: '#fff',
// padding: wp('5%'), 
// borderRadius: hp('2%'), 
// alignItems: 'center',
// },
// alertIcon: {
// fontSize: hp('7%'), 
// color: '#5233ff',
// marginBottom: hp('1%'), 
// },
// alertText: {
// fontSize: hp('2%'), 
// textAlign: 'center',
// },
// alertTextOK:{
// backgroundColor: '#5233ff',
//   color: '#fff',
//   paddingHorizontal: wp('4%'), 
//   paddingVertical: hp('1.5%'), 
//   marginTop: hp('1%'), 
//   borderRadius: wp('2.5%'),
//   fontWeight:'600',
// },
// ----------------------apptabhome-----------------------------------------------------------
    
container: {
  flex: 1,
  backgroundColor:'#ffffff'
},
ThreeSectionsJoin:{
  backgroundColor:'#fdf7eb',
  borderBottomRightRadius: wp(10),
  borderBottomLeftRadius: wp(10),
},
topSection: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingHorizontal: wp(5),
  paddingTop: hp(3),
},
profileImage: {
  width: wp(12),
  height: wp(12),
  borderRadius: wp(6),
  borderWidth: wp(0.5),
  borderColor:'#ad00ff'
},
textDrop: {
  fontSize: wp(5),
  marginRight: wp(7.5),
  backgroundColor: '#fdf7ea',
  borderWidth: wp(0.25),
  paddingVertical: hp(1.25),
  paddingHorizontal: wp(5),
  borderColor:'#f1f1fa',
  borderRadius: wp(5),
},
userBell:{
  fontSize: wp(6.25),
  color:'#7f3dff'
},
secondSection: {
  paddingHorizontal: wp(5),
  paddingTop: hp(1.25),
  alignItems:'center',
  gap: hp(0.875),
},
incomeexpenses:{
  display:'flex',
  flexDirection:'row',
  justifyContent:'center',
  gap: wp(2.5),
  width: '100%',
  paddingHorizontal: wp(7.5),
  paddingVertical: hp(2.5),
},
incomeIcon:{
  fontSize: wp(10),
  color:'#00a86b',
  backgroundColor:'#ffffff',
  borderRadius: wp(5),
  padding: wp(1.25),
},
expenseIcon:{
  fontSize: wp(10),
  color:'#fd3c4a',
  backgroundColor:'#ffffff',
  borderRadius: wp(5),
  padding: wp(1.25),
},
incomeCard:{
  width: '50%',
  backgroundColor:'#00a86b',
  alignItems:'center',
  justifyContent:'center',
  flexDirection:'row',
  gap: wp(2.5),
  borderRadius: wp(7.5),
},
expensesCard:{
  width: '50%',
  backgroundColor:'#fd3c4a',
  alignItems:'center',
  justifyContent:'center',
  flexDirection:'row',
  gap: wp(2.5),
  padding: wp(3.25),
  borderRadius: wp(7.5),
},
incomeCardIncome:{
  fontSize: wp(3.75),
  fontWeight:'500',
  color:'#ffffff'
},
incomeCardMoney:{
  fontSize: wp(6.25),
  fontWeight:'500',
  color:'#ffffff'
},
expensesCardExpense:{
  fontSize: wp(3.75),
  fontWeight:'500',
  color:'#ffffff'
},
expensesCardMoney:{
  fontSize: wp(6.25),
  fontWeight:'500',
  color:'#ffffff'
},
balanceText: {
  fontSize: wp(4.5),
  color:'#91919f',
  fontWeight:'500'
},
balanceAmount: {
  fontSize: wp(10),
  fontWeight:'700'
},
thirdSection: {
  paddingHorizontal: wp(0),
  paddingTop: hp(2.5),
},
spendFrequencyText: {
  fontSize: wp(5),
  fontWeight:'500',
  paddingHorizontal: wp(5),
},
fourthSection: {
  flexDirection: 'row',
  justifyContent: 'space-around',
  marginTop: hp(2.5),
},
periodButton: {
  fontSize: wp(4),
  paddingHorizontal: wp(3.75),
  paddingVertical: hp(0.625),
  borderRadius: wp(5),
},
RecentTransaction:{
  flexDirection:'row',
  justifyContent:'space-between',
  paddingHorizontal: wp(7.5),
  paddingVertical: hp(2.5),
},
RecentTransactionText:{
  fontSize: wp(5),
  fontWeight:'500',
},
RecentTransactionSeeAll:{
  fontSize: wp(4),
  backgroundColor: '#eee5ff',
  paddingHorizontal: wp(3.75),
  paddingVertical: hp(0.625),
  borderRadius: wp(5),
  color:'#7f3dff'
},
listTransaction:{
  backgroundColor:'#fcfcfa',
  flexDirection:'row',
  justifyContent:'space-between',
  paddingHorizontal: wp(5),
  paddingVertical: hp(1.825),
  borderRadius: wp(7.5),
},
iconwithlabdes:{
  flexDirection:'row',
  gap: wp(2.5),
},
TransactionIcon:{
  color:'#00a86b',
  backgroundColor:'#ffffff',
  borderRadius: wp(5),
  padding: wp(1.25),
  borderColor:'#f1f1fa',
  borderWidth: wp(0.25),
},
shopping:{
  fontSize: wp(8),
  color:'#fcac12',
},
note:{
  fontSize: wp(8),
  color:'#7f3dff',
},
food:{
  fontSize: wp(8),
  color:'#fd3c4a',
},
TransactionLabDes:{
  gap: hp(1),
},
TransactionLable:{
  fontSize: wp(4),
  color:'#000000'
},
TransactionDescription:{
  color:'#91919f',
  fontSize: wp(3),
},
TransactionMoneyTime:{
  gap: hp(1),
},
TransactionMoney:{
  fontSize: wp(4),
  color:'#fd3c4a'
},
TransactionTime:{
  color:'#91919f',
  fontSize: wp(3),
},
// -----------------------------------------------------budgetafter------------------------------------------------------------------
Aftercontainer: {
  flex: 1,
  justifyContent: 'space-between',
  backgroundColor: '#7F3DFF',
  paddingTop: StatusBar.currentHeight,
},
Afterheader: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: hp('4%'),
  paddingHorizontal: wp('7%'),
  alignItems: 'baseline',
},
Aftermonth: {
  fontSize: hp('3%'),
  color: '#ffffff',
},
Aftericons: {
  fontSize: wp('6%'),
  color: '#ffffff',
},
Afterbody: {
  justifyContent: 'flex-end',
  backgroundColor: '#fff',
  paddingHorizontal: wp('5%'),
  marginTop: hp('6%'),
  borderTopLeftRadius: wp('10%'),
  borderTopRightRadius: wp('10%'),
},
Budgetbutton: {
  backgroundColor: '#7f3dff',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  alignItems: 'center',
  marginBottom: hp('2%'),
},
BudgetbuttonText: {
  fontWeight: 'bold',
  color: '#fff',
  fontSize: wp('5%'),
},
Aftercategorycont: {
  marginVertical: hp('4%'),
},
categoryBorder: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-evenly',
  borderWidth: 1,
  width: wp('35%'),
  borderColor: 'lightgray',
  textAlign: 'center',
  paddingHorizontal: wp('2%'),
  paddingVertical: hp('.5%'),
  borderRadius: 20,
  marginBottom: hp('1%'),
},
remainingamount: {
  fontSize: wp('7%'),
  fontWeight: 'bold',
},
remainingamountdoller: {
  fontSize: wp('4%'),
},
thumbBar: {
  height: 10,
  backgroundColor: 'lightgray',
  borderRadius: 5,
  marginVertical: hp('2%'),
},
thumbProgress: {
  height: '100%',
  borderRadius: 5,
},
// --------------------------------------------------camera-----------------------------------------------------------
camera: {
  flex: 1,
},
buttonContainer: {
  flex: 1,
  flexDirection: 'row',
  backgroundColor: 'transparent',
  margin: 20,
},
button: {
  flex: 1,
  alignSelf: 'flex-end',
  alignItems: 'center',
},
cameraround:{
  width: wp('18%'), 
  height: wp('18%'), 
  backgroundColor: '#fff',
  borderWidth: wp('1%'), 
  borderColor: 'rgba(255, 255, 255, 0.5)', 
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: wp('9%'),
},
FlipCameracon:{
 backgroundColor:'rgba(134,142,146,0.5)',
 paddingVertical:8,
 paddingHorizontal:15,
 marginVertical:10,
 borderRadius:10
 
},
Fliptext:{
  color:'#fff'
},
// --------------------------------------------create_budget-------------------------------------------------
notificationtrContainertitle: {
  flexDirection: 'row',
  paddingHorizontal: wp('4%'),
  alignItems: 'center',
  paddingTop: hp('3%'),
},
notificationtrTitle: {
  fontSize: wp('6'),
  fontWeight: 'bold',
  color: '#fff',
  marginLeft: wp('20%')
},
containercus: {
  alignItems: 'center',
  flex: 1,
  position: 'relative',
  paddingBottom: hp('6%'),
  paddingHorizontal: wp('5%')
},
sliderTrack: {
  backgroundColor: '#ddd',
  height: hp('1.5%'),
  borderRadius: 10,
  width: wp('90%'),
},
sliderBar: {
  height: '100%',
  backgroundColor: '#7f3dff',
  borderRadius: 10,
},
thumb: {
  position: 'absolute',
  justifyContent: 'center',
  alignItems: 'center',
  width: wp('12%'),
  height: hp('4%'),
  backgroundColor: '#7f3dff',
  borderRadius: 20,
  marginTop: hp('-1.5%'),
  borderColor: '#ffffff',
  borderWidth: 1,
},
percentageText: {
  fontSize: wp('3%'),
  color: '#fff',
  fontWeight: '700'
},
Create_budgetcontainer: {
  flex: 1,
  backgroundColor: '#7F3DFF',
  justifyContent: 'flex-end',
  paddingTop: StatusBar.currentHeight,
},
Create_budgetspend: {
  fontSize: wp('4%'),
  color: '#fcfcfc'
},
Create_budgetamount: {
  paddingHorizontal: wp('5%'),
  paddingBottom: hp('3%')
},
Create_budgetamount1: {
  fontSize: wp('12%'),
  color: '#fff',
},
Create_budgetbottom: {
  backgroundColor: '#ffffff',
  padding: wp('5%'),
  borderTopLeftRadius: wp('5%'),
  borderTopRightRadius: wp('5%'),
},
Create_budgetdropdown: {
  paddingVertical: hp('1%'),
  paddingHorizontal: wp('5%'),
  marginBottom: hp('2%'),
  borderRadius: wp('2%'),
  borderColor: 'lightgray',
  borderWidth: 1
},
Create_budgettoogle: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  padding: wp('2%'),
  marginBottom: hp('3%'),
},  
Create_budgetalert: {
  fontSize: wp('4%'),
  color: '#000',
},
Create_budgetalert1: {
  fontSize: wp('3%'),
  color: '#808080',
},
Create_budgetbutton: {
  backgroundColor: '#7f3dff',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  alignItems: 'center',
 
},
Create_budgetbuttonText: {
  fontWeight: 'bold',
  color: '#fff',
  fontSize: wp('5%')
},
Create_budgetplaceholderStyle: {
  fontSize: wp('4%'),
  color: '#808080',
},  
Create_budgetselectedTextStyle: {
  fontSize: wp('4%'),
},
// ---------------------------------------------customslider-------------------------------------------

    container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    position: 'relative',
    padding:50,
  },
  sliderTrack: {
    backgroundColor: '#ddd',
    height: 20,
    borderRadius: 10,
    width: '100%',
    position:'absolute'
  },
  sliderBar: {
    height: '100%',
    backgroundColor: '#007bff',
    borderRadius: 10,
  },
  thumb: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 30,
    backgroundColor: '#007bff',
    borderRadius: 20,
    zIndex: 1,
    marginLeft: 25, 
  },
  percentageText: {
    fontSize: 12,
    color: '#fff',
  },
  // -----------------------------------------aboupage0-------------------------------------------------------
  containerAbout: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: 'flex-start',
  },
  headerAbout: {
    fontSize: wp('6%'),
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: hp('4%'),
    marginBottom: hp('4%'),
  },
  sectionAbout: {
    marginBottom: hp('2.5%'),
    marginHorizontal: wp('5%'),
  },
  sectionTitleAbout: {
    fontSize: wp('5%'),
    fontWeight: 'bold',
    marginBottom: hp('2%'),
  },
  sectionTextAbout: {
    fontSize: wp('3%'),
    lineHeight: hp('3.5%'),
    marginBottom: hp('1%'),
  },
  aboutButton: {
    backgroundColor: '#7F3DFF',
    borderRadius: wp('2%'),
    paddingVertical: hp('2%'),
    marginHorizontal: wp('5%'),
    alignItems: 'center',
    marginBottom: hp('3%'),
  },
  aboutButtonText: {
    color: '#FFFFFF',
    fontSize: wp('4%'),
  },
  // ---------------------------------------------------------detailaccount------------------------------------------------------------
  detailaccountcontainer: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
},
detailaccountcontent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('2%'),
},
detailaccountheading: {
    fontSize: wp('6%'),
    fontWeight: 'bold',
},
detailaccountcont: {
    alignItems: 'center',
    paddingVertical: hp('3%')
},
detailaccountText: {
    fontSize: wp('4%'),
    color: 'gray',
    paddingBottom: hp('1%')
},
detailbalanceText: {
    fontSize: wp('12%'),
    fontWeight: 'bold',
},
detaildayscontainer: {
    paddingHorizontal: wp('5%'),
},
detaildayscontheading: {
    fontWeight: '700',
    fontSize: wp('5%')
},
detaildaysconttransfer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: hp('3%')
},
detaildaysconttransfersubcont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
},
detaildaysconttransfersub: {
    fontSize: wp('4%'),
    fontWeight: "500",
    color: 'gray',
},
detaildaysconttransferhead: {
    fontSize: wp('5%'),
    paddingBottom: hp('1%')
},
detaildaysconttransferamount: {
    fontWeight: '700',
    fontSize: wp('5%'),
    color: 'red',
    paddingBottom: hp('1%')
},
detaildaysconttransfersubmain: {
    marginLeft: wp('4%')
},
//  -----------------------------------detailsbudet---------------------------------------------------------------
containerdetailbudget: {
  flex: 1,
  backgroundColor: '#fff',
},
headerdetailbudget: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: wp('5%'),
  marginTop: hp('4%'),
  alignItems: 'center',
  paddingTop: StatusBar.currentHeight,
},
headerTextdetailbudget: {
  color: '#000',
  fontSize: wp('6%'),
  fontWeight: 'bold',
},
iconsdetailbudget: {
  fontSize: 35,
  color: '#000',
},
biniconsdetailbudget: {
  fontSize: 25,
  color: '#000',
},
bodydetailbudget: {
  flex: 1,
  paddingHorizontal: wp('5%'),
},
buttonContainerdetailsbudget: {
  flex: 1,
  justifyContent: 'flex-end',
},
buttondetailsbudget: {
  backgroundColor: '#7f3dff',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  alignItems: 'center',
  marginBottom: hp('2%'),
},
buttonTextdetailbudget: {
  fontWeight: 'bold',
  color: '#fff',
  fontSize: wp('5%'),
},
detailRemainderContainer: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
},
detailRemaining: {
  justifyContent: 'center',
  alignItems: 'center',
},
detailRemainingHeading: {
  fontSize: wp('7%'),
  fontWeight: 'bold',
},
detailRemainingBalance: {
  fontSize: wp('12%'),
  fontWeight: 'bold',
},
thumbBar: {
  marginTop: hp('3%'),
  height: 10,
  backgroundColor: 'lightgray',
  borderRadius: 5,
  marginVertical: hp('2%'),
  width: '90%',
},
thumbProgress: {
  height: '100%',
  borderRadius: 5,
},
categoryContainer: {
  flexDirection: 'row',
  alignItems: 'center',
  borderWidth: 1,
  borderColor: 'lightgray',
  paddingHorizontal: wp('2%'),
  paddingVertical: hp('1%'),
  borderRadius: 20,
  marginBottom: hp('3%'),
},
categoryIcon: {
  width: wp('5%'),
  height: wp('5%'),
  marginRight: wp('2%'),
},
categoryLabel: {
  fontSize: wp('4.5%'),
},
remainingAmount: {
  fontSize: wp('4.5%'),
  marginTop: hp('2%'),
},
// -----------------------------------------------------detailstraansactionepense----------------------------------------------------------
DetailTransactionExpenseSafe: {
  flexGrow:1
},
DetailTransactionExpenseMain: {
  display:'flex',
  justifyContent:'center',
  marginTop: StatusBar.currentHeight || 0,
},
DetailTransactionExpenseTopBOTH: {
  backgroundColor: '#fd3c4a',
  paddingHorizontal: wp('5%'),
  paddingVertical: hp('5%'),
  borderBottomLeftRadius: hp('3%'),
  borderBottomRightRadius: hp('3%'),
  position: 'relative',
},
DetailTransactionExpenseTopHead: {
  flexDirection: 'row',
  justifyContent: 'space-between',
},
arrowleftDeatailExpense: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionExpenseText: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
trashDeatailExpense: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionExpenseSub2: {
  justifyContent: 'center',
  alignItems: 'center',
  paddingTop: hp('4%'),
  paddingBottom: hp('4%'),
  gap: hp('1%'),
},
DetailTransactionExpenseSu2DayDate: {
  flexDirection: 'row',
  gap: hp('1%'),
},
DetailTransactionExpenseSub2Rupess: {
  fontSize: hp('5%'),
  fontWeight: '700',
  color: '#fff',
},
DetailTransactionExpenseSub2Shoping: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionExpenseSub2Day: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionExpenseSubDate: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionExpenseSub3: {
  flexDirection: 'row',
  borderWidth: 1,
  marginHorizontal: wp('5%'),
  justifyContent: 'space-around',
  bottom: hp('4%'),
  backgroundColor: '#fff',
  paddingVertical: hp('2a%'),
  borderRadius: hp('1%'),
  borderColor: '#f1f1fa',
},
DetailTransactionExpenseSub3Sub: {
  alignItems: 'center',
  gap: hp('1%'),
},
DetailTransactionExpenseSub3Text1: {
  color: '#91919f',
  fontSize: hp('1.7%'),
  fontWeight: '500',
},
DetailTransactionExpenseSub3Text2: {
  fontSize: hp('2%'),
  fontWeight: '500',
},
DetailTransactionExpenseSub4: {
  borderTopWidth: hp('0.2%'),
  borderStyle: 'dashed',
  marginHorizontal: wp('5%'),
  bottom: hp('2%'),
  paddingVertical: hp('2%'),
  gap: hp('1%'),
},
DetailTransactionExpenseSub4Des: {
  fontSize: hp('2%'),
  color: '#91919f',
},
DetailTransactionExpenseSub4Con: {
  fontSize: hp('1.8%'),
},
DetailTransactionExpenseSub4AttachHead: {
  fontSize: hp('2%'),
  color: '#91919f',
},
DetailTransactionExpenseSub4Image: {
  width: '100%',
  height: hp('20%'),
  borderRadius: hp('1%'),
},
DetailTransactionExpenseSub5: {
  position: 'absolute',
  bottom: hp('2%'),
  left: wp('5%'),
  right: wp('5%'),
  backgroundColor: '#7f3dff',
  alignItems: 'center',
  paddingVertical: hp('2%'),
  borderRadius: hp('1%'),
},
DetailTransactionExpenseSub5Text: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#fff',
},
//   for modal
modalContainer: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.5)',
},
modalContent: {
  backgroundColor: '#ffffff',
  width: '100%',
  paddingVertical: hp('2%'),
  paddingHorizontal: wp('5%'),
  borderTopLeftRadius: hp('2%'),
  borderTopRightRadius: hp('2%'),
  gap:15,
},
modalDivider: {
  width: '10%',
  height: 5,
  backgroundColor: '#d3bdff',
  alignSelf: 'center',
  marginBottom: hp('1%'),
},
modalTitle: {
  fontSize: hp('2.2%'),
  fontWeight: '600',
  textAlign: 'center',
  marginBottom: hp('1%'),
},
modalText: {
  fontSize: hp('2%'),
  textAlign: 'center',
  marginBottom: hp('2%'),
  color:'#91919f'
},
modalButtonContainer: {
  flexDirection: 'row',
  justifyContent: 'space-between',
},
modalButtonNo: {
  flex: 1,
  paddingVertical: hp('2%'),
  alignItems: 'center',
  backgroundColor: '#eee5ff',
  borderRadius: hp('2%'),
  marginHorizontal: wp('1%'),
},
modalButtonYes: {
  flex: 1,
  paddingVertical: hp('2%'),
  alignItems: 'center',
  backgroundColor: '#7f3dff',
  borderRadius: hp('2%'),
  marginHorizontal: wp('1%'),
},
modalButtonTextNo: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#7f3dff',
},
modalButtonTextYes: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#ffffff',
},
//   Alert
overlay: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
},
customAlertContainer: {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
  justifyContent: 'center',
  alignItems: 'center',
},
customAlert: {
  backgroundColor: '#fff',
  padding: wp('5%'), 
  borderRadius: hp('2%'), 
  alignItems: 'center',
},
alertIcon: {
  fontSize: hp('7%'), 
  color: '#5233ff',
  marginBottom: hp('1%'), 
},
alertText: {
  fontSize: hp('2%'), 
  textAlign: 'center',
},
// -------------------------------------------------detailtransactionincome---------------------------------------------------
DetailTransactionIncomeSafe: {
  flexGrow:1
},
DetailTransactionIncomeMain: {
  display:'flex',
  justifyContent:'center',
  marginTop: StatusBar.currentHeight || 0,
},
DetailTransactionIncomeTopBOTH: {
  backgroundColor: '#00a86b',
  paddingHorizontal: wp('5%'),
  paddingVertical: hp('5%'),
  borderBottomLeftRadius: hp('3%'),
  borderBottomRightRadius: hp('3%'),
  position: 'relative',
},
DetailTransactionIncomeTopHead: {
  flexDirection: 'row',
  justifyContent: 'space-between',
},
arrowleftDeatailIncome: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionIncomeText: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
trashDeatailIncome: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionIncomeSub2: {
  justifyContent: 'center',
  alignItems: 'center',
  paddingTop: hp('4%'),
  paddingBottom: hp('4%'),
  gap: hp('1%'),
},
DetailTransactionIncomeSu2DayDate: {
  flexDirection: 'row',
  gap: hp('1%'),
},
DetailTransactionIncomeSub2Rupess: {
  fontSize: hp('5%'),
  fontWeight: '700',
  color: '#fff',
},
DetailTransactionIncomeSub2Shoping: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionIncomeSub2Day: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionIncomeSubDate: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionIncomeSub3: {
  flexDirection: 'row',
  borderWidth: 1,
  marginHorizontal: wp('5%'),
  justifyContent: 'space-around',
  bottom: hp('4%'),
  backgroundColor: '#fff',
  paddingVertical: hp('2a%'),
  borderRadius: hp('1%'),
  borderColor: '#f1f1fa',
},
DetailTransactionIncomeSub3Sub: {
  alignItems: 'center',
  gap: hp('1%'),
},
DetailTransactionIncomeSub3Text1: {
  color: '#91919f',
  fontSize: hp('1.7%'),
  fontWeight: '500',
},
DetailTransactionIncomeSub3Text2: {
  fontSize: hp('2%'),
  fontWeight: '500',
},
DetailTransactionIncomeSub4: {
  borderTopWidth: hp('0.2%'),
  borderStyle: 'dashed',
  marginHorizontal: wp('5%'),
  bottom: hp('2%'),
  paddingVertical: hp('2%'),
  gap: hp('1%'),
},
DetailTransactionIncomeSub4Des: {
  fontSize: hp('2%'),
  color: '#91919f',
},
DetailTransactionIncomeSub4Con: {
  fontSize: hp('1.8%'),
},
DetailTransactionIncomeSub4AttachHead: {
  fontSize: hp('2%'),
  color: '#91919f',
},
DetailTransactionIncomeSub4Image: {
  width: '100%',
  height: hp('20%'),
  borderRadius: hp('1%'),
},
DetailTransactionIncomeSub5: {
  position: 'absolute',
  bottom: hp('2%'),
  left: wp('5%'),
  right: wp('5%'),
  backgroundColor: '#7f3dff',
  alignItems: 'center',
  paddingVertical: hp('2%'),
  borderRadius: hp('1%'),
},
DetailTransactionIncomeSub5Text: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#fff',
},
 //   for modal
 DetailTransactionmodalContainer: {
flex: 1,
justifyContent: 'flex-end',
alignItems: 'center',
backgroundColor: 'rgba(0, 0, 0, 0.5)',
},
DetailTransactionmodalContent: {
backgroundColor: '#ffffff',
width: '100%',
paddingVertical: hp('2%'),
paddingHorizontal: wp('5%'),
borderTopLeftRadius: hp('2%'),
borderTopRightRadius: hp('2%'),
gap:15,
},
DetailTransactionmodalDivider: {
width: '10%',
height: 5,
backgroundColor: '#d3bdff',
alignSelf: 'center',
marginBottom: hp('1%'),
},
DetailTransactionmodalTitle: {
fontSize: hp('2.2%'),
fontWeight: '600',
textAlign: 'center',
marginBottom: hp('1%'),
},
DetailTransactionmodalText: {
fontSize: hp('2%'),
textAlign: 'center',
marginBottom: hp('2%'),
color:'#91919f'
},
DetailTransactionmodalButtonContainer: {
flexDirection: 'row',
justifyContent: 'space-between',
},
DetailTransactionmodalButtonNo: {
flex: 1,
paddingVertical: hp('2%'),
alignItems: 'center',
backgroundColor: '#eee5ff',
borderRadius: hp('2%'),
marginHorizontal: wp('1%'),
},
DetailTransactionmodalButtonYes: {
flex: 1,
paddingVertical: hp('2%'),
alignItems: 'center',
backgroundColor: '#7f3dff',
borderRadius: hp('2%'),
marginHorizontal: wp('1%'),
},
DetailTransactionmodalButtonTextNo: {
fontSize: hp('2%'),
fontWeight: '600',
color: '#7f3dff',
},
DetailTransactionmodalButtonTextYes: {
fontSize: hp('2%'),
fontWeight: '600',
color: '#ffffff',
},
//   Alert
DetailTransactionoverlay: {
flex: 1,
justifyContent: 'center',
alignItems: 'center',
backgroundColor: 'rgba(0, 0, 0, 0.5)', 
},
DetailTransactioncustomAlertContainer: {
position: 'absolute',
top: 0,
bottom: 0,
left: 0,
right: 0,
backgroundColor: 'rgba(0, 0, 0, 0.5)', 
justifyContent: 'center',
alignItems: 'center',
},
DetailTransactioncustomAlert: {
backgroundColor: '#fff',
padding: wp('5%'), 
borderRadius: hp('2%'), 
alignItems: 'center',
},
DetailTransactionalertIcon: {
fontSize: hp('7%'), 
color: '#5233ff',
marginBottom: hp('1%'), 
},
DetailTransactionalertText: {
fontSize: hp('2%'), 
textAlign: 'center',
},

// ------------------------------------------------------detailtransactiontransfer------------------------------------------------
DetailTransactionTransferSafe: {
  flexGrow:1
},
DetailTransactionTransferMain: {
  display:'flex',
  justifyContent:'center',
  marginTop: StatusBar.currentHeight || 0,
},
DetailTransactionTransferTopBOTH: {
  backgroundColor: '#0077ff',
  paddingHorizontal: wp('5%'),
  paddingVertical: hp('5%'),
  borderBottomLeftRadius: hp('3%'),
  borderBottomRightRadius: hp('3%'),
  position: 'relative',
},
DetailTransactionTransferTopHead: {
  flexDirection: 'row',
  justifyContent: 'space-between',
},
arrowleftDeatailTransfer: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionTransferText: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
trashDeatailTransfer: {
  fontSize: hp('2.5%'),
  color: '#fff',
},
DetailTransactionTransferSub2: {
  justifyContent: 'center',
  alignItems: 'center',
  paddingTop: hp('4%'),
  paddingBottom: hp('4%'),
  gap: hp('1%'),
},
DetailTransactionTransferSu2DayDate: {
  flexDirection: 'row',
  gap: hp('1%'),
},
DetailTransactionTransferSub2Rupess: {
  fontSize: hp('5%'),
  fontWeight: '700',
  color: '#fff',
},
DetailTransactionTransferSub2Shoping: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionTransferSub2Day: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionTransferSubDate: {
  color: '#fff',
  fontSize: hp('2%'),
},
DetailTransactionTransferSub3: {
  flexDirection: 'row',
  borderWidth: 1,
  marginHorizontal: wp('5%'),
  justifyContent: 'space-around',
  bottom: hp('4%'),
  backgroundColor: '#fff',
  paddingVertical: hp('2a%'),
  borderRadius: hp('1%'),
  borderColor: '#f1f1fa',
},
DetailTransactionTransferSub3Sub: {
  alignItems: 'center',
  gap: hp('1%'),
},
DetailTransactionTransferSub3Text1: {
  color: '#91919f',
  fontSize: hp('1.7%'),
  fontWeight: '500',
},
DetailTransactionTransferSub3Text2: {
  fontSize: hp('2%'),
  fontWeight: '500',
},
DetailTransactionTransferSub4: {
  borderTopWidth: hp('0.2%'),
  borderStyle: 'dashed',
  marginHorizontal: wp('5%'),
  bottom: hp('2%'),
  paddingVertical: hp('2%'),
  gap: hp('1%'),
},
DetailTransactionTransferSub4Des: {
  fontSize: hp('2%'),
  color: '#91919f',
},
DetailTransactionTransferSub4Con: {
  fontSize: hp('1.8%'),
},
DetailTransactionTransferSub4AttachHead: {
  fontSize: hp('2%'),
  color: '#91919f',
},
DetailTransactionTransferSub4Image: {
  width: '100%',
  height: hp('20%'),
  borderRadius: hp('1%'),
},
DetailTransactionTransferSub5: {
  position: 'absolute',
  bottom: hp('2%'),
  left: wp('5%'),
  right: wp('5%'),
  backgroundColor: '#7f3dff',
  alignItems: 'center',
  paddingVertical: hp('2%'),
  borderRadius: hp('1%'),
},
DetailTransactionTransferSub5Text: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#fff',
},
//   for modal
DetailTransactionTransfermodalContainer: {
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.5)',
},
DetailTransactionTransfermodalContent: {
  backgroundColor: '#ffffff',
  width: '100%',
  paddingVertical: hp('2%'),
  paddingHorizontal: wp('5%'),
  borderTopLeftRadius: hp('2%'),
  borderTopRightRadius: hp('2%'),
  gap:15,
},
DetailTransactionTransfermodalDivider: {
  width: '10%',
  height: 5,
  backgroundColor: '#d3bdff',
  alignSelf: 'center',
  marginBottom: hp('1%'),
},
DetailTransactionTransfermodalTitle: {
  fontSize: hp('2.2%'),
  fontWeight: '600',
  textAlign: 'center',
  marginBottom: hp('1%'),
},
DetailTransactionTransfermodalText: {
  fontSize: hp('2%'),
  textAlign: 'center',
  marginBottom: hp('2%'),
  color:'#91919f'
},
DetailTransactionTransfermodalButtonContainer: {
  flexDirection: 'row',
  justifyContent: 'space-between',
},
DetailTransactionTransfermodalButtonNo: {
  flex: 1,
  paddingVertical: hp('2%'),
  alignItems: 'center',
  backgroundColor: '#eee5ff',
  borderRadius: hp('2%'),
  marginHorizontal: wp('1%'),
},
DetailTransactionTransfermodalButtonYes: {
  flex: 1,
  paddingVertical: hp('2%'),
  alignItems: 'center',
  backgroundColor: '#7f3dff',
  borderRadius: hp('2%'),
  marginHorizontal: wp('1%'),
},
DetailTransactionTransfermodalButtonTextNo: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#7f3dff',
},
DetailTransactionTransfermodalButtonTextYes: {
  fontSize: hp('2%'),
  fontWeight: '600',
  color: '#ffffff',
},
//   Alert
DetailTransactionTransferoverlay: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
},
DetailTransactionTransfercustomAlertContainer: {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
  justifyContent: 'center',
  alignItems: 'center',
},
DetailTransactionTransfercustomAlert: {
  backgroundColor: '#fff',
  padding: wp('5%'), 
  borderRadius: hp('2%'), 
  alignItems: 'center',
},
DetailTransactionTransferalertIcon: {
  fontSize: hp('7%'), 
  color: '#5233ff',
  marginBottom: hp('1%'), 
},
DetailTransactionTransferalertText: {
  fontSize: hp('2%'), 
  textAlign: 'center',
},
// -----------------------------------------------export---------------------------------------------------------

settingsexportContainer: {
  flex: 1,
  backgroundColor: '#FFFFFF',
  paddingTop: StatusBar.currentHeight || 0, 
},
settingsexportContent: {
  flex: 1,
  paddingHorizontal: wp('5%'),
  paddingTop: hp('2%'),
},
settingsexportTitle: {
  fontSize: wp('4.5%'),
  fontWeight: '600',
  marginBottom: hp('2%'),
},
settingsexportDropdown: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: hp('2%'),
  paddingHorizontal: wp('5%'),
  borderWidth: 1,
  borderColor: 'lightgrey',
  borderRadius: wp('2%'),
  marginBottom: hp('2%'),
},
settingsexportDropdownOptions: {
  paddingVertical: hp('1%'),
  paddingHorizontal: wp('3%'),
},
settingsexportDropdownItem: {
  paddingVertical: hp('1%'),
  paddingHorizontal: wp('3%'),
},
settingsexportButton: {
  backgroundColor: '#7F3DFF',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  alignItems: 'center',
  marginTop: 'auto', 
  marginBottom: hp('2%'),
  flexDirection: 'row',
  justifyContent: 'center',
},
settingsexportButtonText: {
  color: '#FFFFFF',
  fontSize: wp('5%'),
},
settingsexportIcon: {
  marginRight: wp('4%'),
},
// --------------------------------------------------exportemail-----------------------------------------------------
settingexport2container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
},
settingexport2cont: {
  flex: 1,
  justifyContent: 'center',
},
settingexport2image: {
  marginVertical:hp('3%'),
  marginLeft:'auto',
  marginRight:'auto',
 
},
settingexport2para: {
  fontSize: wp('4%'), 
  textAlign: 'center',
  marginHorizontal: wp('5%'),
},
settingexport2Button: {
  backgroundColor: '#7F3DFF',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  marginHorizontal: wp('5%'),
  alignItems: 'center',
  marginTop: 'auto',
  marginBottom: hp('3%'), 
},
settingexport2ButtonText: {
  color: '#FFFFFF',
  fontSize: wp('4%'), 
},
// ----------------------------------------------------financeaccount--------------------------------------------------
financeaccountcontainer: {
  flex: 1,
  paddingTop: StatusBar.currentHeight,
},
financeaccountcontent: {
  paddingHorizontal: wp('5%'),
  flexDirection: 'row',
  marginVertical: hp('1%')
},
Financeaccountheading: {
  fontSize: wp('6%'),
  fontWeight: 'bold',
  marginLeft: wp('25%')
},
accountDetails: {
  alignItems: 'center',
  paddingVertical: hp('8%')
},
accountText: {
  fontSize: wp('4%'),
  color: 'gray',
  paddingBottom: hp('1%')
},
balanceText: {
  fontSize: wp('12%'),
  fontWeight: 'bold',
},
sectiontext: {
  fontSize: wp('5%'),
  marginLeft: wp('2%')
},
sectionfinancecont: {
  flexDirection: 'row',
  alignItems: 'center'
},
sectionamounttext: {
  fontSize: wp('6%'),
  fontWeight: 'bold',
},
sectionfinancecontcontainer: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  paddingHorizontal: wp('5%'),
},
horizontalLine: {
  borderBottomWidth: 1,
  borderBottomColor: 'lightgray',
  marginVertical: hp('2%'),
},
addnewButton: {
  backgroundColor: '#7F3DFF',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  marginHorizontal: wp('5%'),
  alignItems: 'center',
  position: 'absolute',
  bottom: hp('2%'),
  width: '90%',
},
addnewButtonText: {
  color: '#FFFFFF',
  fontSize: wp('4%'),
},
// ---------------------------------------------------financialreport-------------------------------------------------------
financialreportmaincontainer:{
  flex:1,
  paddingTop: StatusBar.currentHeight,
},
paginationStyle: {
  bottom: hp('94%'),
  gap: 10
},
dotStyle: {
  width: wp(20),
  height: wp(1),
  borderRadius: 5,
  backgroundColor: 'rgba(255,255,255,.3)',
},
activeDotStyle: {
  width: wp(20),
  height: wp(1),
  borderRadius: 5,
  backgroundColor: '#fff',
},
slide1: {
  flex: 1,
  justifyContent: 'space-evenly',
  alignItems: 'center',
  backgroundColor: 'rgb(253,60,74)',
},
txt1: {
  color: 'white',
  fontSize: wp('5%'),
  alignItems: 'center',
  fontWeight: "600",
},
page1_txt2: {
  color: 'white',
  fontSize: wp('8%'),
  alignItems: 'center',
  fontWeight: "700"
},
page1_txt3: {
  color: 'white',
  fontSize: wp('10%'),
  fontWeight: "700",
  textAlign: 'center',
},
page1_footer: {
  width: wp('80%'),
  alignItems: 'center',
  backgroundColor: 'white',
  borderRadius: 30,
  padding: wp('5%')
},
page1_ftr: {
  gap: 20,
},
page1_footertxt1: {
  fontSize: wp('7%'),
  textAlign: 'center',
  fontWeight: "700",
},
page1_shopping: {
  width: wp('40%'),
  marginLeft: 'auto',
  marginRight: 'auto',
  paddingVertical: hp('2%'),
  borderWidth: 1,
  borderColor: 'lightgray',
  justifyContent: 'center',
  borderRadius: 20,
},
page1_shopping1: {
  flexDirection: 'row',
  gap: 15,
  alignItems: 'center',
  justifyContent: 'center',
},
page1_topicon: {
  borderRadius: 8,
  backgroundColor: '#FCEED4',
  color: 'gold',
  fontSize: wp(4),
  padding: 4
},
page1_footertxt2: {
  fontSize: wp('8%'),
  fontWeight: '700',
  textAlign: 'center',
},
slide2: {
  flex: 1,
  justifyContent: 'space-evenly',
  alignItems: 'center',
  backgroundColor: 'rgb(0,168,107)',
},
slide3: {
  flex: 1,
  justifyContent: 'space-evenly',
  alignItems: 'center',
  backgroundColor: '#rgb(127,61,255)',
},
page3_shopping: {
  width: wp('40%'),
  padding: hp('2%'),
  borderColor: 'gray',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 20,
  backgroundColor: '#FCFCFC',
  flexDirection: 'row'
},
page3_shopping2: {
  width: wp('40%'),
  padding: hp('2%'),
  borderWidth: 0.5,
  borderColor: 'gray',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 20,
  backgroundColor: '#FCFCFC',
  flexDirection: 'row'
},
page3_shopping1: {
  flexDirection: 'row',
  gap: 15,
},
page3_maintxt1: {
  marginTop: hp('-8%')
},
page3_container1txt: {
  textAlign: 'center',
  color: 'white',
  fontSize: wp('7%'),
  fontWeight: 'bold',
  letterSpacing: 0.6
},
page3_container1txt1: {
  textAlign: 'center',
  color: 'white',
  fontSize: wp('7%'),
  fontWeight: 'bold',
  letterSpacing: 1
},
page3buttons: {
  display: 'flex',
  flexDirection: 'row',
  marginTop: hp('3%'),
  gap: 10,
},
page3_topicon1: {
  borderRadius: 8,
  backgroundColor: '#FCEED4',
  color: 'gold',
  width: wp(6.5),
  height: hp(3),
  fontSize: wp(4),
  alignItems: 'center',
  justifyContent: 'center',
  padding: 3.5

},
page3_topicon2: {
  borderRadius: 8,
  backgroundColor: '#FDD5D7',
  color: 'gold',
  width: wp(6.5),
  height: hp(3),
  fontSize: wp(2),
  alignItems: 'center',
  justifyContent: 'center',
  padding: 3.5
},
slide4: {
  flex: 1,
  backgroundColor: '#rgb(127,61,255)',
},
page4_maintxt: {
  flex: 1,
  justifyContent: 'center',
  paddingHorizontal: wp('10%')
},
page4_txt1: {
  fontSize: wp('8%'),
  fontWeight: 'bold',
  color: 'white',
},
page4_txt2: {
  fontSize: wp('5%'),
  fontWeight: 'bold',
  color: 'white',
  paddingTop: hp('1%')
},
buttonmain: {
  flex: 1,
  justifyContent: 'flex-end',
},
page4_button: {
  backgroundColor: '#fff',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  marginHorizontal: wp('5%'),
  alignItems: 'center',
  marginBottom: hp('3%')

},
page4_buttontxt: {
  fontWeight: 'bold',
  color: '#rgb(127,61,255)',
  fontSize: wp('5%')
},
// ----------------------------------------------finacnciallinereport--------------------------------------------------------
FinancialReportLineSafe: {
  flexGrow: 1,
  backgroundColor: '#fff',
},
FinancialReportLineMain: {
  display: 'flex',
  justifyContent: 'center',
  marginTop: StatusBar.currentHeight || 0,
},
FinancialReportLineTopHead: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: 20,
  marginTop: 20,
},
arrowleftFinancialReport: {
  fontSize: hp('3.5%'),
  color: '#000',
},
FinancialReportText: {
  fontSize: hp('2.5%'),
  color: '#000',
},
SubCon2: {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: 40,
  marginBottom: 10,
  paddingHorizontal: 20,
},
lineAndChartIcon: {
  flexDirection: 'row',
  backgroundColor: '#fff',
  alignItems: 'center',
  borderRadius: 11,
  borderColor: '#f1f1fa',
  borderWidth: 1,
},
chartIcon1: {
  color: '#7f3dff',
  fontSize: 20,
  backgroundColor: 'white',
  paddingHorizontal: 10,
  paddingVertical: 13,
  borderTopLeftRadius:10,
  borderBottomLeftRadius:10,
},
chartIcon2: {
  color: '#7f3dff',
  fontSize: 20,
  backgroundColor: 'white',
  paddingHorizontal: 10,
  paddingVertical: 13,
  borderTopRightRadius:10,
  borderBottomRightRadius:10,
},
TotalReportAmountIE: {
  paddingHorizontal: 30,
  marginBottom: 10,
},
TotalReportAmountIETEXT: {
  fontSize: 40,
  fontWeight: '600',
},
ExpenseIncomeTABS: {
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#f1f1fa',
  padding: 5,
  marginHorizontal: 20,
  borderRadius: 50,
  marginTop: 10,
},
ExpenseIncomeTABSEx: {
  width: '50%',
  textAlign: 'center',
  paddingVertical: 15,
  borderRadius: 50,
},
ExpenseIncomeTABSIn: {
  width: '50%',
  textAlign: 'center',
  paddingVertical: 15,
  borderRadius: 50,
},
DropDownPickerLineChart: {},
DropDownpickmonths: {
  width: '30%',
},
dropdownStyle: {
  borderColor: '#f1f1fa',
  // borderRadius:50,
},
categoryDropdownWithIcon:{
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
   marginHorizontal: 20,
  marginVertical: 10,
},
categoryDropdownWithIconSep:{
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
  borderWidth:1,
  padding:10,
  borderColor:'#f1f1fa',
  borderRadius:10
},
categoryDropdown: {
 
  width:'35%',
 
},
listTransaction: {
  backgroundColor: '#fcfcfa',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: 20,
  paddingVertical: 15,
  borderRadius: 30,
},
iconwithlabdes: {
  flexDirection: 'row',
  gap: 10,
},
TransactionIcon: {
  color: '#00a86b',
  backgroundColor: '#ffffff',
  borderRadius: 20,
  padding: 5,
  borderColor: '#f1f1fa',
  borderWidth: 1,
},
shopping: {
  fontSize: 35,
  color: '#fcac12',
},
note: {
  fontSize: 35,
  color: '#7f3dff',
},
food: {
  fontSize: 35,
  color: '#fd3c4a',
},
salary: {
  fontSize: 35,
  color: '#00a86b',
},
freelancing: {
  fontSize: 35,
  color: '#ffa500',
},
TransactionLabDes: {
  gap: 5,
},
TransactionLable: {
  fontSize: 18,
  color: '#000000',
},
TransactionDescription: {
  color: '#91919f',
  fontSize: 14,
},
TransactionMoneyTime: {
  gap: 5,
},
TransactionMoney: {
  fontSize: 18,
  color: '#fd3c4a',
},
TransactionTime: {
  color: '#91919f',
  fontSize: 14,
},
// ---------------------------------------------------------financialreportpie-----------------------------------------------------------------
FinancialReportPieFinancialReportLineSafe: {
  flexGrow: 1,
  backgroundColor: '#fff',
},
FinancialReportPieFinancialReportLineMain: {
  display: 'flex',
  justifyContent: 'center',
  marginTop: StatusBar.currentHeight || 0,
},
FinancialReportPieFinancialReportLineTopHead: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: 20,
  marginTop: 20,
},
FinancialReportPiearrowleftFinancialReport: {
  fontSize: hp('3.5%'),
  color: '#000',
},
FinancialReportPieFinancialReportText: {
  fontSize: hp('2.5%'),
  color: '#000',
},
FinancialReportPieSubCon2: {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: 40,
  marginBottom: 10,
  paddingHorizontal: 20,
},
FinancialReportPielineAndChartIcon: {
  flexDirection: 'row',
  backgroundColor: '#fff',
  alignItems: 'center',
  borderRadius: 11,
  borderColor: '#f1f1fa',
  borderWidth: 1,
},
FinancialReportPiechartIcon1: {
  color: '#7f3dff',
  fontSize: 20,
  backgroundColor: 'white',
  paddingHorizontal: 10,
  paddingVertical: 13,
  borderTopLeftRadius:10,
  borderBottomLeftRadius:10,
},
FinancialReportPiechartIcon2: {
  color: '#7f3dff',
  fontSize: 20,
  backgroundColor: 'white',
  paddingHorizontal: 10,
  paddingVertical: 13,
  borderTopRightRadius:10,
  borderBottomRightRadius:10,
},
FinancialReportPieTotalReportAmountIE: {
  paddingHorizontal: 30,
  marginBottom: 10,
},
FinancialReportPieTotalReportAmountIETEXT: {
  fontSize: 40,
  fontWeight: '600',
},
FinancialReportPieExpenseIncomeTABS: {
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#f1f1fa',
  padding: 5,
  marginHorizontal: 20,
  borderRadius: 50,
  marginTop: 10,
},
FinancialReportPieExpenseIncomeTABSEx: {
  width: '50%',
  textAlign: 'center',
  paddingVertical: 15,
  borderRadius: 50,
},
FinancialReportPieExpenseIncomeTABSIn: {
  width: '50%',
  textAlign: 'center',
  paddingVertical: 15,
  borderRadius: 50,
},
FinancialReportPieDropDownPickerLineChart: {},
FinancialReportPieDropDownpickmonths: {
  width: '30%',
},
FinancialReportPiedropdownStyle: {
  borderColor: '#f1f1fa',
  // borderRadius:50,
},
FinancialReportPiecategoryDropdownWithIcon:{
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
   marginHorizontal: 20,
  marginVertical: 10,
},
FinancialReportPiecategoryDropdownWithIconSep:{
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
  borderWidth:1,
  padding:10,
  borderColor:'#f1f1fa',
  borderRadius:10
},
FinancialReportPiecategoryDropdown: {
 
  width:'35%',
 
},
FinancialReportPielistTransaction: {
  backgroundColor: '#fcfcfa',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: 20,
  paddingVertical: 15,
  borderRadius: 30,
},
FinancialReportPieiconwithlabdes: {
  flexDirection: 'row',
  gap: 10,
},
FinancialReportPieTransactionIcon: {
  color: '#00a86b',
  backgroundColor: '#ffffff',
  borderRadius: 20,
  padding: 5,
  borderColor: '#f1f1fa',
  borderWidth: 1,
},
FinancialReportPieshopping: {
  fontSize: 35,
  color: '#fcac12',
},
FinancialReportPienote: {
  fontSize: 35,
  color: '#7f3dff',
},
FinancialReportPiefood: {
  fontSize: 35,
  color: '#fd3c4a',
},
FinancialReportPiesalary: {
  fontSize: 35,
  color: '#00a86b',
},
FinancialReportPiefreelancing: {
  fontSize: 35,
  color: '#ffa500',
},
FinancialReportPieTransactionLabDes: {
  gap: 5,
},
FinancialReportPieTransactionLable: {
  fontSize: 18,
  color: '#000000',
},
FinancialReportPieTransactionDescription: {
  color: '#91919f',
  fontSize: 14,
},
FinancialReportPieTransactionMoneyTime: {
  gap: 5,
},
FinancialReportPieTransactionMoney: {
  fontSize: 18,
  color: '#fd3c4a',
},
FinancialReportPieTransactionTime: {
  color: '#91919f',
  fontSize: 14,
},
// ----------------------------------------------------------------forgot password----------------------------------------------------------------
forgotcontainer: {
  flex: 1,
  alignItems: 'left',
  justifyContent: 'center',
  paddingHorizontal: 20,
  backgroundColor: '#FFFFFF',
  position:'relative',
  width:'100%',
 
  
},
forgottextcon:{
marginBottom:40,
},
forgottext:{
  fontSize: 26,
  fontWeight: '500',
  // marginTop:20,
  lineHeight:32,
  
},
forgotinput: {
  width: '100%',
  height: 60,
  borderColor: 'lightgrey',
  borderWidth: 1,
  borderRadius: 20,
  paddingHorizontal: 10,
  marginBottom: 25,
},
forgotinputError: {
  borderColor: 'red',
},
forgotinputValid: {
  borderColor: 'green',
  borderWidth:2,
},
forgoterrorText: {
  color: 'red',
  fontSize: 12,
  marginBottom: 5,
  position: 'relative',
  textAlign: 'left',
  width: '100%',

},
forgotbutton: {
  width: '100%',
  backgroundColor: '#7f3dff',
  paddingVertical: 18,
  borderRadius: 20,
  alignItems: 'center',
  justifyContent: 'center',
  marginBottom: 10,
},
forgotbuttonText: {
  color: 'white',
  fontSize: 18,
  fontWeight: '500',
},

// ---------------------------------------------------------forgotpasswordemail------------------------------------------------------
forgotpasswordemailcontainer: {
  flex: 1,
  alignItems: 'center',
  // justifyContent: 'center',
  paddingHorizontal: 20,
  paddingVertical:80,
  backgroundColor: '#FFFFFF',
},
forgotpasswordemailEmailMessage:{
textAlign:'center',
fontSize:25,
fontWeight:"500",
marginBottom:15,
},
forgotpasswordemailEmailMessagedown:{
  textAlign:'center',
  fontSize:16,
  fontWeight:'500'
  // marginBottom:10,
},

forgotpasswordemailbutton: {
  width: '100%',
  backgroundColor: '#7f3dff',
  paddingVertical: 18,
  borderRadius: 20,
  alignItems: 'center',
  justifyContent: 'center',
  position: 'absolute', 
  bottom: 20,
 
},
forgotpasswordemailbuttonText: {
  color: 'white',
  fontSize: 18,
  fontWeight: '500',
},
// --------------------------------------------------------help-----------------------------------------------------------------
containerHelp: {
  flex: 1,
  backgroundColor: '#FFFFFF',
},
scrollContainerHelp: {
  flexGrow: 1,
  marginHorizontal: wp('5%'),
},
headerHelp: {
  fontSize: wp('6%'),
  fontWeight: 'bold',
  textAlign: 'center',
  marginTop: hp('4%'),
  marginBottom: hp('4%'),
},
sectionHelp: {
  marginBottom: hp('4%'),
},
sectionTitleHelp: {
  fontSize: wp('5%'),
  fontWeight: 'bold',
  marginBottom: hp('2%'),
},
sectionTextHelp: {
  fontSize: wp('3%'),
  lineHeight: hp('3%'),
},
backButtonHelp: {
  backgroundColor: '#7F3DFF',
  borderRadius: wp('2%'),
  paddingVertical: hp('2%'),
  alignItems: 'center',
  marginBottom: hp('3%'),
},
backButtonTextHelp: {
  color: '#FFFFFF',
  fontSize: wp('4%'),
},
// --------------------------------------------------------------------login-----------------------------------------------------------------
logincontainer: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  paddingHorizontal: 20,
  backgroundColor: '#FFFFFF',
},
logininput: {
  width: '100%',
  height: 60,
  borderColor: 'lightgrey',
  borderWidth: 1,
  borderRadius: 20,
  paddingHorizontal: 10,
  marginBottom: 25,
},
logininputError: {
  borderColor: 'red',
},
logininputValid: {
  borderColor: 'green',
  borderWidth:2,
},
loginerrorText: {
  color: 'red',
  fontSize: 12,
  marginBottom: 5,
  position: 'relative',
  textAlign: 'left',
  width: '100%',

},
loginpasswordInput: {
  flexDirection: 'row',
  alignItems: 'center',
},
loginpasswordToggle: {
  marginLeft: 10,
},
loginpasswordToggleIcon: {},
checkboxContainer: {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: 10,
},
logincheckbox: {
  width: 20,
  height: 20,
  borderWidth: 1,
  borderColor: '#7f3dff',
  marginRight: 10,
  justifyContent: 'center',
  alignItems: 'center',
},
logincheckboxText: {
  flex: 1,
  fontSize: 13,
  lineHeight: 18,
},
logincheckboxText1: {
  flex: 1,
  fontSize: 13,
  lineHeight: 18,
  color: '#7f3dff',
},
loginbutton: {
  width: '100%',
  backgroundColor: '#7f3dff',
  paddingVertical: 18,
  borderRadius: 20,
  alignItems: 'center',
  justifyContent: 'center',
  marginBottom: 10,
},
loginbuttonText: {
  color: 'white',
  fontSize: 18,
  fontWeight: '500',
},
loginsmallText: {
  marginBottom: 20,
  marginTop: 20,
  fontSize: 16,
  fontWeight: '600',
  color: '#7f3dff',
},
googleButton: {
  width: '100%',
  borderColor: 'lightgrey',
  borderWidth: 1,
  paddingVertical: 14,
  borderRadius: 20,
  alignItems: 'center',
  justifyContent: 'center',
  marginBottom: 10,
  flexDirection: 'row',
},
googleIcon: {
  width: 30,
  height: 30,
  marginRight: 10,
},
googleText: {
  fontSize: 16,
  fontWeight: '600',
  color: '#000',
},
loginAlreadyLog: {
  fontSize: 16,
  color: 'grey',
  paddingTop: 5,
},
loginAlreadyLog1: {
  fontSize: 16,
  color: '#7f3dff',
  textDecorationLine: 'underline',
},

// -------------------------------------------------------------------newaccount---------------------------------------------------------
 NewAccountcontainer: {
    flex: 1,
    backgroundColor: '#7F3DFF',
    justifyContent: 'flex-end'
  },
  NewAccountBalancePaisa: {
    marginBottom:hp('2%'),
    paddingHorizontal: wp('5%')
  },
  NewAccountBalance: {
    fontSize: wp('5%'),
    color: '#FFFFFF'
  },
  NewAccountpaisa: {
    color: 'white',
    fontWeight: '600',
    fontSize: wp('10%')
  },
  NewAccountbox: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: wp('5%'),
    borderTopRightRadius: wp('5%'),
    borderWidth: 1,
    borderColor: 'gray',
    width: '100%',
  },
  NewAccountInput: {
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('5%'),
    marginHorizontal: wp('5%'),
    marginVertical: hp('2%'),
    color: 'black',
    borderWidth: 1,
    borderRadius: wp('2%'),
    marginBottom: hp('2%'),
    borderColor: 'lightgrey'
  },
  NewAccounttypeDropdown: {
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    color: 'black',
    borderWidth: 1,
    borderColor: 'lightgrey',
    borderRadius: wp('2%'),
    marginHorizontal: wp('5%'),
    marginBottom: hp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  NewAccounttypeText: {
    color: 'black'
  },
  NewAccountdropdownItem: {
    paddingBottom: hp('3%'),
    paddingHorizontal: wp('10%')
  },
  NewAccountdropdown: {
    backgroundColor: '#fff',
  },
  NewAccountcontinueButton: {
    backgroundColor: '#7F3DFF',
    borderRadius: wp('2%'),
    paddingVertical: hp('2%'),
    marginHorizontal: wp('5%'),
    alignItems: 'center',
    marginBottom: hp('3%')
  },
  NewAccountcontinueButtonText: {
    color: 'white',
    fontSize: wp('5%')
  },

  });