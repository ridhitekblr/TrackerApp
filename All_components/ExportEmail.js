import { Image, globalStylesheet, Text, View, TouchableOpacity, SafeAreaView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import React from 'react';
import { globalStyles } from './globalStyles';

const ExportEmail = ({navigation, route}) => {
  const { userData } = route.params || {};
  return (
    <SafeAreaView style={globalStyles.settingexport2container}>
      <View style={globalStyles.settingexport2cont}>
        <Image 
          source={require('../assets/vector.png')}
          style={globalStyles.settingexport2image}
        />
        <Text style={globalStyles.settingexport2para}>Check your email, we send you the financial report. In certain cases, it might take a little longer, depending on the time period and the volume of activity.</Text>
        <TouchableOpacity style={globalStyles.settingexport2Button} onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
          <Text style={globalStyles.settingexport2ButtonText}>Back to Home</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

export default ExportEmail;

// const globalStyles = globalStylesheet.create({
//   settingexport2container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   settingexport2cont: {
//     flex: 1,
//     justifyContent: 'center',
//   },
//   settingexport2image: {
//     marginVertical:hp('3%'),
//     marginLeft:'auto',
//     marginRight:'auto',
   
//   },
//   settingexport2para: {
//     fontSize: wp('4%'), 
//     textAlign: 'center',
//     marginHorizontal: wp('5%'),
//   },
//   settingexport2Button: {
//     backgroundColor: '#7F3DFF',
//     borderRadius: wp('2%'),
//     paddingVertical: hp('2%'),
//     marginHorizontal: wp('5%'),
//     alignItems: 'center',
//     marginTop: 'auto',
//     marginBottom: hp('3%'), 
//   },
//   settingexport2ButtonText: {
//     color: '#FFFFFF',
//     fontSize: wp('4%'), 
//   }
// })