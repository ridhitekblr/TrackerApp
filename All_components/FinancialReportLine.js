import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, StatusBar, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropDownPicker from 'react-native-dropdown-picker';
import { LineChart } from 'react-native-chart-kit';

const FinancialReportLine = ({ navigation, route, }) => {
  const { userData } = route.params || {};
  const [openMonth, setOpenMonth] = useState(false);
  const [selectedChart, setSelectedChart] = useState('line-chart');
  const [selectedExIn, setSelectedExIn] = useState('TabEx');
  const [valueMonth, setValueMonth] = useState('January');

  const [openCategory, setOpenCategory] = useState(false);
  const [valueCategory, setValueCategory] = useState(null);
  const [itemsCategory, setItemsCategory] = useState([
    { label: 'Shopping', value: 'shopping' },
    { label: 'Subscription', value: 'subscription' },
    { label: 'Food', value: 'food' },
    { label: 'Transportation', value: 'transportation' },
    { label: 'Entertainment', value: 'entertainment' },
    { label: 'Utilities', value: 'utilities' },
    // Add more categories as needed
  ]);

  const incomeData = [
    { id: '1', label: 'Salary', description: 'Monthly Salary', money: '+ $3000', icon: <MaterialCommunityIcons name='cash' style={styles.salary}></MaterialCommunityIcons>, time: '01:00 PM' },
    { id: '2', label: 'Freelancing', description: 'Project Work', money: '+ $1500', icon: <MaterialCommunityIcons name='laptop' style={styles.freelancing}></MaterialCommunityIcons>, time: '11:00 AM' },
    // Add more income data
  ];

  const expenseData = [
    { id: '1', label: 'Shopping', description: 'Buy some grocery', money: '- $120', icon: <MaterialCommunityIcons name='shopping' style={styles.shopping}></MaterialCommunityIcons>, time: '10:00 AM' },
    { id: '2', label: 'Subscription', description: 'Disney+ Annual..', money: '- $80', icon: <MaterialCommunityIcons name='note' style={styles.note}></MaterialCommunityIcons>, time: '03:30 PM' },
    { id: '3', label: 'Food', description: 'Buy a ramen', money: '- $32', icon: <MaterialCommunityIcons name='food' style={styles.food}></MaterialCommunityIcons>, time: '07:30 PM' },
    // Add more expense data
  ];

  const [itemsMonth, setItemsMonth] = useState([
    { label: 'January', value: 'January' },
    { label: 'February', value: 'February' },
    { label: 'March', value: 'March' },
    { label: 'April', value: 'April' },
    { label: 'May', value: 'May' },
    { label: 'June', value: 'June' },
    { label: 'July', value: 'July' },
    { label: 'August', value: 'August' },
    { label: 'September', value: 'September' },
    { label: 'October', value: 'October' },
    { label: 'November', value: 'November' },
    { label: 'December', value: 'December' },
  ]);

  const lineChartData = {
    January: [50, 80, 90, 70, 100],
    February: [60, 70, 80, 90, 100],
    March: [70, 85, 95, 75, 100],
    April: [65, 75, 85, 95, 100],
    May: [55, 65, 75, 85, 100],
    June: [75, 85, 95, 75, 100],
    July: [80, 90, 100, 80, 100],
    August: [70, 80, 90, 70, 100],
    September: [60, 70, 80, 60, 100],
    October: [50, 60, 70, 50, 100],
    November: [40, 50, 60, 40, 100],
    December: [30, 40, 50, 30, 100],
  };
  

  const renderList = (data, isExpense) => (
    <FlatList
      data={data}
      keyExtractor={(item) => item.id}
      renderItem={({ item }) => (
        <View style={{ paddingVertical: 10, paddingHorizontal: 30 }}>
          <View style={styles.listTransaction}>
            <View style={styles.iconwithlabdes}>
              <View style={styles.TransactionIconView}>
                <View style={styles.TransactionIcon}>{item.icon}</View>
              </View>
              <View style={styles.TransactionLabDes}>
                <Text style={styles.TransactionLable}>{item.label}</Text>
                <Text style={styles.TransactionDescription}>{item.description}</Text>
              </View>
            </View>
            <View style={styles.TransactionMoneyTime}>
            <Text style={[styles.TransactionMoney, isExpense ? { color: '#fd3c4a' } : { color: '#00a86b' }]}>{item.money}</Text>
              <Text style={styles.TransactionTime}>{item.time}</Text>
            </View>
          </View>
        </View>
      )}
    />
  );

  return (
    <SafeAreaView style={styles.FinancialReportLineSafe}>
      <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
      <View style={styles.FinancialReportLineMain}>
        <View style={styles.FinancialReportLineTopHead}>
        <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
          <MaterialCommunityIcons name="arrow-left" style={styles.arrowleftFinancialReport} />
          </TouchableOpacity>
          <Text style={styles.FinancialReportText}>Financial Report</Text>
          <TouchableOpacity>
            {/* <Icon name='trash' style={styles.trashDeatailExpense}></Icon> */}
          </TouchableOpacity>
        </View>
        <View style={styles.SubCon2}>
          <View style={styles.DropDownpickmonths}>
            <DropDownPicker
              open={openMonth}
              value={valueMonth}
              items={itemsMonth}
              setOpen={setOpenMonth}
              setValue={setValueMonth}
              setItems={setItemsMonth}
              placeholder="All"
              placeholderStyle={{ color: '#91919f' }}
              selectedItemLabelStyle={{ color: 'blue' }}
              containerStyle={styles.dropdown}
              maxHeight={165}
              style={styles.dropdownStyle}
              textStyle={{ color: '#7f3dff' }}
              ArrowUpIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-up" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
            ArrowDownIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-down" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
            />
          </View>
          <View style={styles.lineAndChartIcon}>
            
            <Icon
              name="line-chart"
              style={[styles.chartIcon1, selectedChart === 'line-chart' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
              onPress={() => setSelectedChart('line-chart')} 
            />
            <TouchableOpacity onPress={() => navigation.navigate('FinancialReportPie')}>
            <Icon
              name="pie-chart"
              style={[styles.chartIcon2, selectedChart === 'pie-chart' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
              onPress={() => setSelectedChart('pie-chart')}
            />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.TotalReportAmountIE}>
          <Text style={styles.TotalReportAmountIETEXT}>$332</Text>
        </View>
        <LineChart
          data={{
            datasets: [
              {
                data: lineChartData[valueMonth],
              },
            ],
          }}
          width={Dimensions.get('window').width}
          height={200}
          chartConfig={{
            backgroundColor: '#fff',
            backgroundGradientFrom: '#7f3dff',
            backgroundGradientTo: '#000',
            decimalPlaces: 2,
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 0,
            },
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 0,
          }}
        />
        <View style={styles.ExpenseIncomeTABS}>
          <Text
            name="TabEx"
            style={[styles.ExpenseIncomeTABSEx, selectedExIn === 'TabEx' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
            onPress={() => setSelectedExIn('TabEx')}
          >
            Expense
          </Text>
          <Text
            name="TabIn"
            style={[styles.ExpenseIncomeTABSIn, selectedExIn === 'TabIn' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
            onPress={() => setSelectedExIn('TabIn')}
          >
            Income
          </Text>
        </View>
        <View style={styles.categoryDropdownWithIcon}>
        <View style={styles.categoryDropdown}>
        <DropDownPicker
            open={openCategory}
            value={valueCategory}
            items={itemsCategory}
            setOpen={setOpenCategory}
            setValue={setValueCategory}
            setItems={setItemsCategory}
            placeholder="Select Category"
            placeholderStyle={{ color: '#91919f' }}
            selectedItemLabelStyle={{ color: 'blue' }}
            containerStyle={styles.dropdown}
            maxHeight={165}
            style={styles.dropdownStyle}
            textStyle={{ color: '#7f3dff' }}
            ArrowUpIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-up" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
            ArrowDownIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-down" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
          />
          
        </View>
        <View style={styles.categoryDropdownWithIconSep}>
        <Icon name="bars" size={20}/>
        </View>
        
        </View>
        {selectedExIn === 'TabEx' ? renderList(expenseData, true) : renderList(incomeData, false)}

      </View>
    </SafeAreaView>
  );
};

export default FinancialReportLine;

const styles = StyleSheet.create({
  FinancialReportLineSafe: {
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  FinancialReportLineMain: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: StatusBar.currentHeight || 0,
  },
  FinancialReportLineTopHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginTop: 20,
  },
  arrowleftFinancialReport: {
    fontSize: hp('3.5%'),
    color: '#000',
  },
  FinancialReportText: {
    fontSize: hp('2.5%'),
    color: '#000',
  },
  SubCon2: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 40,
    marginBottom: 10,
    paddingHorizontal: 20,
  },
  lineAndChartIcon: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignItems: 'center',
    borderRadius: 11,
    borderColor: '#f1f1fa',
    borderWidth: 1,
  },
  chartIcon1: {
    color: '#7f3dff',
    fontSize: 20,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 13,
    borderTopLeftRadius:10,
    borderBottomLeftRadius:10,
  },
  chartIcon2: {
    color: '#7f3dff',
    fontSize: 20,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 13,
    borderTopRightRadius:10,
    borderBottomRightRadius:10,
  },
  TotalReportAmountIE: {
    paddingHorizontal: 30,
    marginBottom: 10,
  },
  TotalReportAmountIETEXT: {
    fontSize: 40,
    fontWeight: '600',
  },
  ExpenseIncomeTABS: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f1f1fa',
    padding: 5,
    marginHorizontal: 20,
    borderRadius: 50,
    marginTop: 10,
  },
  ExpenseIncomeTABSEx: {
    width: '50%',
    textAlign: 'center',
    paddingVertical: 15,
    borderRadius: 50,
  },
  ExpenseIncomeTABSIn: {
    width: '50%',
    textAlign: 'center',
    paddingVertical: 15,
    borderRadius: 50,
  },
  DropDownPickerLineChart: {},
  DropDownpickmonths: {
    width: '30%',
  },
  dropdownStyle: {
    borderColor: '#f1f1fa',
    // borderRadius:50,
  },
  categoryDropdownWithIcon:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
     marginHorizontal: 20,
    marginVertical: 10,
  },
  categoryDropdownWithIconSep:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    borderWidth:1,
    padding:10,
    borderColor:'#f1f1fa',
    borderRadius:10
  },
  categoryDropdown: {
   
    width:'35%',
   
  },
  listTransaction: {
    backgroundColor: '#fcfcfa',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderRadius: 30,
  },
  iconwithlabdes: {
    flexDirection: 'row',
    gap: 10,
  },
  TransactionIcon: {
    color: '#00a86b',
    backgroundColor: '#ffffff',
    borderRadius: 20,
    padding: 5,
    borderColor: '#f1f1fa',
    borderWidth: 1,
  },
  shopping: {
    fontSize: 35,
    color: '#fcac12',
  },
  note: {
    fontSize: 35,
    color: '#7f3dff',
  },
  food: {
    fontSize: 35,
    color: '#fd3c4a',
  },
  salary: {
    fontSize: 35,
    color: '#00a86b',
  },
  freelancing: {
    fontSize: 35,
    color: '#ffa500',
  },
  TransactionLabDes: {
    gap: 5,
  },
  TransactionLable: {
    fontSize: 18,
    color: '#000000',
  },
  TransactionDescription: {
    color: '#91919f',
    fontSize: 14,
  },
  TransactionMoneyTime: {
    gap: 5,
  },
  TransactionMoney: {
    fontSize: 18,
    color: '#fd3c4a',
  },
  TransactionTime: {
    color: '#91919f',
    fontSize: 14,
  },
});

