import React, { useState } from 'react';
import { globalStylesheet, Text, View, TouchableOpacity, ScrollView, TextInput, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import { globalStyles } from './globalStyles';

const NewAccount = ({ navigation }) => {
  const [accountName, setAccountName] = useState('');
  const [accountType, setAccountType] = useState('');
  const [dropdownVisible, setDropdownVisible] = useState(false);

  const toggleDropdown = () => {
    setDropdownVisible(!dropdownVisible);
  };

  const selectAccountType = (type) => {
    setAccountType(type);
    setDropdownVisible(false);
  };

  const handleContinue = () => {
    if (accountName && accountType) {
      navigation.navigate('Wallet');
    } else {
      if (!accountType && !accountName) {
        alert('Please select an account type and enter an account name.');
      } else if (!accountType) {
        alert('Please select an account type.');
      } else {
        alert('Please enter an account name.');
      }
    }
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={globalStyles.NewAccountcontainer}>
          <View style={[globalStyles.NewAccountBalancePaisa, dropdownVisible]}>
            <Text style={globalStyles.NewAccountBalance}>Balance</Text>
            <Text style={globalStyles.NewAccountpaisa}>$00.0</Text>
          </View>
          <View style={[globalStyles.NewAccountbox, dropdownVisible && { bottom: 0 }]}>
            <TextInput
              style={globalStyles.NewAccountInput}
              placeholder="Name"
              value={accountName}
              onChangeText={setAccountName}
            />
            <TouchableOpacity style={globalStyles.NewAccounttypeDropdown} onPress={toggleDropdown}>
              <Text style={globalStyles.NewAccounttypeText}>{accountType ? accountType : 'Account Type'}</Text>
              <Icon name={dropdownVisible ? "caret-up" : "caret-down"} size={20} color="black" />
            </TouchableOpacity>
            {dropdownVisible && (
              <ScrollView style={globalStyles.NewAccountdropdown}>
                <TouchableOpacity style={globalStyles.NewAccountdropdownItem} onPress={() => selectAccountType('Checking')}>
                  <Text>Checking</Text>
                </TouchableOpacity>
                <TouchableOpacity style={globalStyles.NewAccountdropdownItem} onPress={() => selectAccountType('Savings')}>
                  <Text>Savings</Text>
                </TouchableOpacity>
                <TouchableOpacity style={globalStyles.NewAccountdropdownItem} onPress={() => selectAccountType('Investment')}>
                  <Text>Investment</Text>
                </TouchableOpacity>
              </ScrollView>
            )}
            <TouchableOpacity style={globalStyles.NewAccountcontinueButton} onPress={handleContinue}>
              <Text style={globalStyles.NewAccountcontinueButtonText}>Continue</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default NewAccount;

// const globalStyles = globalStylesheet.create({
//   NewAccountcontainer: {
//     flex: 1,
//     backgroundColor: '#7F3DFF',
//     justifyContent: 'flex-end'
//   },
//   NewAccountBalancePaisa: {
//     marginBottom:hp('2%'),
//     paddingHorizontal: wp('5%')
//   },
//   NewAccountBalance: {
//     fontSize: wp('5%'),
//     color: '#FFFFFF'
//   },
//   NewAccountpaisa: {
//     color: 'white',
//     fontWeight: '600',
//     fontSize: wp('10%')
//   },
//   NewAccountbox: {
//     backgroundColor: '#FFFFFF',
//     borderTopLeftRadius: wp('5%'),
//     borderTopRightRadius: wp('5%'),
//     borderWidth: 1,
//     borderColor: 'gray',
//     width: '100%',
//   },
//   NewAccountInput: {
//     paddingVertical: hp('1.5%'),
//     paddingHorizontal: wp('5%'),
//     marginHorizontal: wp('5%'),
//     marginVertical: hp('2%'),
//     color: 'black',
//     borderWidth: 1,
//     borderRadius: wp('2%'),
//     marginBottom: hp('2%'),
//     borderColor: 'lightgrey'
//   },
//   NewAccounttypeDropdown: {
//     paddingVertical: hp('2%'),
//     paddingHorizontal: wp('5%'),
//     backgroundColor: 'rgba(255, 255, 255, 0.2)',
//     color: 'black',
//     borderWidth: 1,
//     borderColor: 'lightgrey',
//     borderRadius: wp('2%'),
//     marginHorizontal: wp('5%'),
//     marginBottom: hp('2%'),
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center'
//   },
//   NewAccounttypeText: {
//     color: 'black'
//   },
//   NewAccountdropdownItem: {
//     paddingBottom: hp('3%'),
//     paddingHorizontal: wp('10%')
//   },
//   NewAccountdropdown: {
//     backgroundColor: '#fff',
//   },
//   NewAccountcontinueButton: {
//     backgroundColor: '#7F3DFF',
//     borderRadius: wp('2%'),
//     paddingVertical: hp('2%'),
//     marginHorizontal: wp('5%'),
//     alignItems: 'center',
//     marginBottom: hp('3%')
//   },
//   NewAccountcontinueButtonText: {
//     color: 'white',
//     fontSize: wp('5%')
//   },
// });