import { CameraView, useCameraPermissions } from 'expo-camera';
import React, { useState } from 'react';
import { Button, globalStylesheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native'; // Import useNavigation hook
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

export default function App() {
  const [facing, setFacing] = useState('back');
  const [permission, requestPermission] = useCameraPermissions();
  const [cameraRef, setCameraRef] = useState(null);
  const navigation = useNavigation(); // Initialize navigation object

  if (!permission) {
    // Camera permissions are still loading.
    return <View />;
  }

  if (!permission.granted) {
    // Camera permissions are not granted yet.
    return (
      <View style={globalStyles.container}>
        <Text style={{ textAlign: 'center' }}>We need your permission to show the camera</Text>
        <Button onPress={requestPermission} title="Grant Permission" />
      </View>
    );
  }

  function toggleCameraFacing() {
    setFacing(current => (current === 'back' ? 'front' : 'back'));
  }

  const takePicture = async () => {
    if (cameraRef) {
      const photo = await cameraRef.takePictureAsync();
      navigation.navigate('AddAttachment', { capturedPhotoUri: photo.uri }); // Navigate to AddAttachment screen and pass the captured photo URI
    } else {
      console.error('Camera reference is null');
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <CameraView style={globalStyles.camera} facing={facing} ref={setCameraRef}>
      <View style={globalStyles.buttonContainer}>
          <TouchableOpacity style={globalStyles.button} onPress={takePicture}>
          <View style={globalStyles.cameraround}>
           
          </View>
          <TouchableOpacity style={globalStyles.FlipCameracon} onPress={toggleCameraFacing}>
            <Text style={globalStyles.Fliptext}>Flip Camera</Text>
          </TouchableOpacity>
            
          </TouchableOpacity>
        </View>
       
      </CameraView>
     
    </View>
  );
}

// const globalStyles = globalStylesheet.create({
//   camera: {
//     flex: 1,
//   },
//   buttonContainer: {
//     flex: 1,
//     flexDirection: 'row',
//     backgroundColor: 'transparent',
//     margin: 20,
//   },
//   button: {
//     flex: 1,
//     alignSelf: 'flex-end',
//     alignItems: 'center',
//   },
//   cameraround:{
//     width: wp('18%'), 
//     height: wp('18%'), 
//     backgroundColor: '#fff',
//     borderWidth: wp('1%'), 
//     borderColor: 'rgba(255, 255, 255, 0.5)', 
//     justifyContent: 'center',
//     alignItems: 'center',
//     borderRadius: wp('9%'),
//   },
//   FlipCameracon:{
//    backgroundColor:'rgba(134,142,146,0.5)',
//    paddingVertical:8,
//    paddingHorizontal:15,
//    marginVertical:10,
//    borderRadius:10
   
//   },
//   Fliptext:{
//     color:'#fff'
//   }
// });
