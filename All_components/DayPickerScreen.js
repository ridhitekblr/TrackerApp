import React, { useState } from 'react';
import { View, Text, SafeAreaView,TouchableOpacity } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { FontAwesome } from '@expo/vector-icons';
const DayPickerScreen = () => {
  const [openFrequency, setOpenFrequency] = useState(false); // State for Frequency toggle
  const [openEndAfter, setOpenEndAfter] = useState(false); // State for End After toggle
  const [selectedDay, setSelectedDay] = useState(null);
  const [selectedMonth, setSelectedMonth] = useState(null);
  const [selectedYear, setSelectedYear] = useState(null);

  const days = Array.from({ length: 31 }, (_, i) => i + 1);
  const months = [
    { label: 'January', value: 1 },
    { label: 'February', value: 2 },
    { label: 'March', value: 3 },
    { label: 'April', value: 4 },
    { label: 'May', value: 5 },
    { label: 'June', value: 6 },
    { label: 'July', value: 7 },
    { label: 'August', value: 8 },
    { label: 'September', value: 9 },
    { label: 'October', value: 10 },
    { label: 'November', value: 11 },
    { label: 'December', value: 12 },
  ];
  const years = Array.from({ length: 10 }, (_, i) => new Date().getFullYear() - i);
  const handleToggleFrequency = () => {
    setOpenFrequency(!openFrequency);
    setOpenEndAfter(false); // Close End After dropdown when Frequency dropdown opens
  };

  const handleToggleEndAfter = () => {
    setOpenEndAfter(!openEndAfter);
    setOpenFrequency(false); // Close Frequency dropdown when End After dropdown opens
  };
  return (
    <SafeAreaView>
       <TouchableOpacity style={styles.Frequency} onPress={handleToggleFrequency}>
        <Text>Frequency</Text>
        <FontAwesome name={openFrequency ? "chevron-up" : "chevron-down"} size={16} color="black" /> 
      </TouchableOpacity>
      {openFrequency && (
    <View style={styles.pickerMain}>
     
      <View style={styles.pickerContainer}>
      
        <Picker 
          style={styles.picker}
          selectedValue={selectedDay}
          onValueChange={(itemValue, itemIndex) => setSelectedDay(itemValue)}
        >
          <Picker.Item label="Day" value={null} />
          {days.map(day => (
            <Picker.Item key={day} label={day.toString()} value={day} />
          ))}
        </Picker>
      </View>

      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={selectedMonth}
          onValueChange={(itemValue, itemIndex) => setSelectedMonth(itemValue)}
        >
          <Picker.Item label="Month" value={null} />
          {months.map(month => (
            <Picker.Item key={month.value} label={month.label} value={month.value} />
          ))}
        </Picker>
      </View>

      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={selectedYear}
          onValueChange={(itemValue, itemIndex) => setSelectedYear(itemValue)}
        >
          <Picker.Item label="Year" value={null} />
          {years.map(year => (
            <Picker.Item key={year} label={year.toString()} value={year} />
          ))}
        </Picker>
      </View>

      {/* {(selectedDay || selectedMonth || selectedYear) && (
        <View style={{ marginTop: 20 }}>
          <Text>You selected: {selectedDay} - {selectedMonth} - {selectedYear}</Text>
        </View>
      )} */}
    </View>
      )}
    <TouchableOpacity style={styles.Frequency} onPress={handleToggleEndAfter}>
        <Text>End After</Text>
        <FontAwesome name={openEndAfter ? "chevron-up" : "chevron-down"} size={16} color="black" /> 
      </TouchableOpacity>
    {openEndAfter && (
    <View style={styles.pickerMain}>
  
      <View style={styles.pickerContainer2}>
      
        <Picker 
          style={styles.picker}
          selectedValue={selectedDay}
          onValueChange={(itemValue, itemIndex) => setSelectedDay(itemValue)}
        >
          <Picker.Item label="Day" value={null} />
          {days.map(day => (
            <Picker.Item key={day} label={day.toString()} value={day} />
          ))}
        </Picker>
      </View>

      <View style={styles.pickerContainer2}>
        <Picker
          style={styles.picker}
          selectedValue={selectedMonth}
          onValueChange={(itemValue, itemIndex) => setSelectedMonth(itemValue)}
        >
          <Picker.Item label="Month" value={null} />
          {months.map(month => (
            <Picker.Item key={month.value} label={month.label} value={month.value} />
          ))}
        </Picker>
      </View>

     

      {/* {(selectedDay || selectedMonth || selectedYear) && (
        <View style={{ marginTop: 20 }}>
          <Text>You selected: {selectedDay} - {selectedMonth} - {selectedYear}</Text>
        </View>
      )} */}
    </View>
)}
    </SafeAreaView>
    
    
  );
};

const styles = {
  pickerMain:{
 flex: 1, justifyContent: 'center', alignItems: 'center' ,
 flexDirection: "row",
 marginHorizontal:20
  },
  pickerContainer: {
    width: '33.33%',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
   
    

  },
  pickerContainer2: {
    width: '50%',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
   
    

  },
  picker: {
    width: '100%',
    
    
  },
  Frequency: {
    paddingVertical: 20,
    backgroundColor: 'red',
    width: '100%',
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    paddingHorizontal: 10, 
    marginVertical:20
    
  },

};

export default DayPickerScreen;
