import React, { useState } from 'react';
import { SafeAreaView, globalStylesheet, Text, View, StatusBar, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DropDownPicker from 'react-native-dropdown-picker';
import { PieChart, } from "react-native-gifted-charts";
import { globalStyles } from './globalStyles';

const FinancialReportPie = ({navigation}) => {
  const [openMonth, setOpenMonth] = useState(false);
  const [selectedChart, setSelectedChart] = useState('line-chart');
  const [selectedExIn, setSelectedExIn] = useState('TabEx');
  const [valueMonth, setValueMonth] = useState('January');

  const [openCategory, setOpenCategory] = useState(false);
  const [valueCategory, setValueCategory] = useState(null);
  const [itemsCategory, setItemsCategory] = useState([
    { label: 'Shopping', value: 'shopping' },
    { label: 'Subscription', value: 'subscription' },
    { label: 'Food', value: 'food' },
    { label: 'Transportation', value: 'transportation' },
    { label: 'Entertainment', value: 'entertainment' },
    { label: 'Utilities', value: 'utilities' },
    // Add more categories as needed
  ]);

  const incomeData = [
    { id: '1', label: 'Salary', description: 'Monthly Salary', money: '+ $3000', icon: <MaterialCommunityIcons name='cash' style={globalStyles.FinancialReportPiesalary}></MaterialCommunityIcons>, time: '01:00 PM' },
    { id: '2', label: 'Freelancing', description: 'Project Work', money: '+ $1500', icon: <MaterialCommunityIcons name='laptop' style={globalStyles.FinancialReportPiefreelancing}></MaterialCommunityIcons>, time: '11:00 AM' },
    // Add more income data
  ];

  const expenseData = [
    { id: '1', label: 'Shopping', description: 'Buy some grocery', money: '- $120', icon: <MaterialCommunityIcons name='shopping' style={globalStyles.FinancialReportPieshopping}></MaterialCommunityIcons>, time: '10:00 AM' },
    { id: '2', label: 'Subscription', description: 'Disney+ Annual..', money: '- $80', icon: <MaterialCommunityIcons name='note' style={globalStyles.FinancialReportPienote}></MaterialCommunityIcons>, time: '03:30 PM' },
    { id: '3', label: 'Food', description: 'Buy a ramen', money: '- $32', icon: <MaterialCommunityIcons name='food' style={globalStyles.FinancialReportPiefood}></MaterialCommunityIcons>, time: '07:30 PM' },
    // Add more expense data
  ];

  const [itemsMonth, setItemsMonth] = useState([
    { label: 'January', value: 'January' },
    { label: 'February', value: 'February' },
    { label: 'March', value: 'March' },
    { label: 'April', value: 'April' },
    { label: 'May', value: 'May' },
    { label: 'June', value: 'June' },
    { label: 'July', value: 'July' },
    { label: 'August', value: 'August' },
    { label: 'September', value: 'September' },
    { label: 'October', value: 'October' },
    { label: 'November', value: 'November' },
    { label: 'December', value: 'December' },
  ]);

 
 

  const renderList = (data, isExpense) => (
    <FlatList
      data={data}
      keyExtractor={(item) => item.id}
      renderItem={({ item }) => (
        <View style={{ paddingVertical: 10, paddingHorizontal: 30 }}>
          <View style={globalStyles.FinancialReportPielistTransaction}>
            <View style={globalStyles.FinancialReportPieiconwithlabdes}>
              <View style={globalStyles.FinancialReportPieTransactionIconView}>
                <View style={globalStyles.FinancialReportPieTransactionIcon}>{item.icon}</View>
              </View>
              <View style={globalStyles.FinancialReportPieTransactionLabDes}>
                <Text style={globalStyles.FinancialReportPieTransactionLable}>{item.label}</Text>
                <Text style={globalStyles.FinancialReportPieTransactionDescription}>{item.description}</Text>
              </View>
            </View>
            <View style={globalStyles.FinancialReportPieTransactionMoneyTime}>
            <Text style={[globalStyles.FinancialReportPieTransactionMoney, isExpense ? { color: '#fd3c4a' } : { color: '#00a86b' }]}>{item.money}</Text>
              <Text style={globalStyles.FinancialReportPieTransactionTime}>{item.time}</Text>
            </View>
          </View>
        </View>
      )}
    />
  );

  return (
    <SafeAreaView style={globalStyles.FinancialReportPieFinancialReportLineSafe}>
      <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
      <View style={globalStyles.FinancialReportPieFinancialReportLineMain}>
        <View style={globalStyles.FinancialReportPieFinancialReportLineTopHead}>
          <MaterialCommunityIcons name="arrow-left" style={globalStyles.arrowleftFinancialReport} />
          <Text style={globalStyles.FinancialReportPieFinancialReportText}>Financial Report</Text>
          <TouchableOpacity>
            {/* <Icon name='trash' style={globalStyles.trashDeatailExpense}></Icon> */}
          </TouchableOpacity>
        </View>
        <View style={globalStyles.FinancialReportPieSubCon2}>
          <View style={globalStyles.FinancialReportPieDropDownpickmonths}>
            <DropDownPicker
              open={openMonth}
              value={valueMonth}
              items={itemsMonth}
              setOpen={setOpenMonth}
              setValue={setValueMonth}
              setItems={setItemsMonth}
              placeholder="All"
              placeholderStyle={{ color: '#91919f' }}
              selectedItemLabelStyle={{ color: 'blue' }}
              containerStyle={globalStyles.FinancialReportPiedropdown}
              maxHeight={165}
              style={globalStyles.FinancialReportPiedropdownStyle}
              textStyle={{ color: '#7f3dff' }}
              ArrowUpIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-up" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
            ArrowDownIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-down" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
            />
          </View>
          <View style={globalStyles.FinancialReportPielineAndChartIcon}>
            
            <Icon
              name="line-chart"
              style={[globalStyles.FinancialReportPiechartIcon1, selectedChart === 'line-chart' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
              onPress={() => setSelectedChart('line-chart')} 
            />
            <TouchableOpacity onPress={() => navigation.navigate('FinancialReportPie')}>
            <Icon
              name="pie-chart"
              style={[globalStyles.FinancialReportPiechartIcon2, selectedChart === 'pie-chart' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
              onPress={() => setSelectedChart('pie-chart')}
            />
            </TouchableOpacity>
          </View>
        </View>
        <View style={globalStyles.FinancialReportPieTotalReportAmountIE}>
          <Text style={globalStyles.FinancialReportPieTotalReportAmountIETEXT}>$332</Text>
        </View>
       
        
        <View style={globalStyles.FinancialReportPieExpenseIncomeTABS}>
          <Text
            name="TabEx"
            style={[globalStyles.FinancialReportPieExpenseIncomeTABSEx, selectedExIn === 'TabEx' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
            onPress={() => setSelectedExIn('TabEx')}
          >
            Expense
          </Text>
          <Text
            name="TabIn"
            style={[globalStyles.FinancialReportPieExpenseIncomeTABSIn, selectedExIn === 'TabIn' ? { backgroundColor: '#7f3dff', color: '#fff' } : null]}
            onPress={() => setSelectedExIn('TabIn')}
          >
            Income
          </Text>
        </View>
        <View style={globalStyles.FinancialReportPiecategoryDropdownWithIcon}>
        <View style={globalStyles.FinancialReportPiecategoryDropdown}>
        <DropDownPicker
            open={openCategory}
            value={valueCategory}
            items={itemsCategory}
            setOpen={setOpenCategory}
            setValue={setValueCategory}
            setItems={setItemsCategory}
            placeholder="Select Category"
            placeholderStyle={{ color: '#91919f' }}
            selectedItemLabelStyle={{ color: 'blue' }}
            containerStyle={globalStyles.FinancialReportPiedropdown}
            maxHeight={165}
            style={globalStyles.FinancialReportPiedropdownStyle}
            textStyle={{ color: '#7f3dff' }}
            ArrowUpIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-up" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
            ArrowDownIconComponent={({ style }) => (
              <MaterialCommunityIcons name="chevron-down" style={{ ...style, color: '#7f3dff', fontSize: 20 }} />
            )}
          />
          
        </View>
        <View style={globalStyles.FinancialReportPiecategoryDropdownWithIconSep}>
        <Icon name="bars" size={20}/>
        </View>
        
        </View>
        {selectedExIn === 'TabEx' ? renderList(expenseData, true) : renderList(incomeData, false)}

      </View>
    </SafeAreaView>
  );
};

export default FinancialReportPie;

// const globalStyles = globalStylesheet.create({
//   FinancialReportPieFinancialReportLineSafe: {
//     flexGrow: 1,
//     backgroundColor: '#fff',
//   },
//   FinancialReportPieFinancialReportLineMain: {
//     display: 'flex',
//     justifyContent: 'center',
//     marginTop: StatusBar.currentHeight || 0,
//   },
//   FinancialReportPieFinancialReportLineTopHead: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     paddingHorizontal: 20,
//     marginTop: 20,
//   },
//   FinancialReportPiearrowleftFinancialReport: {
//     fontSize: hp('3.5%'),
//     color: '#000',
//   },
//   FinancialReportPieFinancialReportText: {
//     fontSize: hp('2.5%'),
//     color: '#000',
//   },
//   FinancialReportPieSubCon2: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     marginTop: 40,
//     marginBottom: 10,
//     paddingHorizontal: 20,
//   },
//   FinancialReportPielineAndChartIcon: {
//     flexDirection: 'row',
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     borderRadius: 11,
//     borderColor: '#f1f1fa',
//     borderWidth: 1,
//   },
//   FinancialReportPiechartIcon1: {
//     color: '#7f3dff',
//     fontSize: 20,
//     backgroundColor: 'white',
//     paddingHorizontal: 10,
//     paddingVertical: 13,
//     borderTopLeftRadius:10,
//     borderBottomLeftRadius:10,
//   },
//   FinancialReportPiechartIcon2: {
//     color: '#7f3dff',
//     fontSize: 20,
//     backgroundColor: 'white',
//     paddingHorizontal: 10,
//     paddingVertical: 13,
//     borderTopRightRadius:10,
//     borderBottomRightRadius:10,
//   },
//   FinancialReportPieTotalReportAmountIE: {
//     paddingHorizontal: 30,
//     marginBottom: 10,
//   },
//   FinancialReportPieTotalReportAmountIETEXT: {
//     fontSize: 40,
//     fontWeight: '600',
//   },
//   FinancialReportPieExpenseIncomeTABS: {
//     flexDirection: 'row',
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#f1f1fa',
//     padding: 5,
//     marginHorizontal: 20,
//     borderRadius: 50,
//     marginTop: 10,
//   },
//   FinancialReportPieExpenseIncomeTABSEx: {
//     width: '50%',
//     textAlign: 'center',
//     paddingVertical: 15,
//     borderRadius: 50,
//   },
//   FinancialReportPieExpenseIncomeTABSIn: {
//     width: '50%',
//     textAlign: 'center',
//     paddingVertical: 15,
//     borderRadius: 50,
//   },
//   FinancialReportPieDropDownPickerLineChart: {},
//   FinancialReportPieDropDownpickmonths: {
//     width: '30%',
//   },
//   FinancialReportPiedropdownStyle: {
//     borderColor: '#f1f1fa',
//     // borderRadius:50,
//   },
//   FinancialReportPiecategoryDropdownWithIcon:{
//     flexDirection:'row',
//     justifyContent:'space-between',
//     alignItems:'center',
//      marginHorizontal: 20,
//     marginVertical: 10,
//   },
//   FinancialReportPiecategoryDropdownWithIconSep:{
//     flexDirection:'row',
//     justifyContent:'space-between',
//     alignItems:'center',
//     borderWidth:1,
//     padding:10,
//     borderColor:'#f1f1fa',
//     borderRadius:10
//   },
//   FinancialReportPiecategoryDropdown: {
   
//     width:'35%',
   
//   },
//   FinancialReportPielistTransaction: {
//     backgroundColor: '#fcfcfa',
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     paddingHorizontal: 20,
//     paddingVertical: 15,
//     borderRadius: 30,
//   },
//   FinancialReportPieiconwithlabdes: {
//     flexDirection: 'row',
//     gap: 10,
//   },
//   FinancialReportPieTransactionIcon: {
//     color: '#00a86b',
//     backgroundColor: '#ffffff',
//     borderRadius: 20,
//     padding: 5,
//     borderColor: '#f1f1fa',
//     borderWidth: 1,
//   },
//   FinancialReportPieshopping: {
//     fontSize: 35,
//     color: '#fcac12',
//   },
//   FinancialReportPienote: {
//     fontSize: 35,
//     color: '#7f3dff',
//   },
//   FinancialReportPiefood: {
//     fontSize: 35,
//     color: '#fd3c4a',
//   },
//   FinancialReportPiesalary: {
//     fontSize: 35,
//     color: '#00a86b',
//   },
//   FinancialReportPiefreelancing: {
//     fontSize: 35,
//     color: '#ffa500',
//   },
//   FinancialReportPieTransactionLabDes: {
//     gap: 5,
//   },
//   FinancialReportPieTransactionLable: {
//     fontSize: 18,
//     color: '#000000',
//   },
//   FinancialReportPieTransactionDescription: {
//     color: '#91919f',
//     fontSize: 14,
//   },
//   FinancialReportPieTransactionMoneyTime: {
//     gap: 5,
//   },
//   FinancialReportPieTransactionMoney: {
//     fontSize: 18,
//     color: '#fd3c4a',
//   },
//   FinancialReportPieTransactionTime: {
//     color: '#91919f',
//     fontSize: 14,
//   },
// });

