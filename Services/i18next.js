import i18next from 'i18next';
import {initReactI18next} from 'react-i18next';
import English from '../Laguage_Formats/English.json';
import Indonesian from '../Laguage_Formats/Indonesian.json';
import Arabic from '../Laguage_Formats/Arabic.json';
import Chinese from '../Laguage_Formats/Chinese .json';
import Dutch from '../Laguage_Formats/Dutch.json';
import French from '../Laguage_Formats/French.json';
import German from '../Laguage_Formats/German.json';
import Italian from '../Laguage_Formats/Italian.json';
import Korean from '../Laguage_Formats/Korean.json';
import Portuguese from '../Laguage_Formats/Portuguese.json';
import Russian from '../Laguage_Formats/Russian.json';
import Spanish from '../Laguage_Formats/Spanish.json';


const LanguageResources = {
  en: { translation: English }, 
  id: { translation: Indonesian },
  ar: { translation: Arabic },
  zh: { translation: Chinese },
  nl: { translation: Dutch },
  fr: { translation: French },
  de: { translation: German },
  it: { translation: Italian },
  ko: { translation: Korean },
  pt: { translation: Portuguese },
  ru: { translation: Russian },
  es: { translation: Spanish }
};

i18next.use(initReactI18next).init({ 
    compatibilityJSON:'v3',
    lng: 'en',
    fallbackLng: 'en', 
    resources: LanguageResources, 
    interpolation: { 
      escapeValue: false 
    } 
  }); 
    
  export default i18next;