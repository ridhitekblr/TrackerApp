import React, { useState } from 'react';
import {
  Stylesheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  KeyboardAvoidingView,
  SafeAreaView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  StatusBar,
  Modal,
  Dimensions,
  
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as ImagePicker from 'expo-image-picker'; 
import * as DocumentPicker from 'expo-document-picker';
import { FontAwesome } from '@expo/vector-icons'; // Import FontAwesome icon
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';


const windowWidth = Dimensions.get('window').width;
// const windowHeight = Dimensions.get('window').height;
const AttachmentModal = ({ isVisible, onClose, onCameraClick,onOpenGallery,onDocumentSelect }) => {
  return (
    <Modal  animationType="slide" transparent={true} visible={isVisible} onRequestClose={onClose}>
      <TouchableWithoutFeedback onPress={onClose}>
        <View style={Styles.modalContainer}>
          <View style={Styles.modalContent}>
            <TouchableOpacity style={Styles.iconContainer} onPress={onCameraClick}>
            <Icon name="camera" size={25} color="#7f3dff"/>
            <Text style={Styles.iconText}>Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.iconContainer} onPress={onOpenGallery}>
            <Icon name="image" size={25} color="#7f3dff"/>
            <Text style={Styles.iconText}>Image</Text>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.iconContainer} onPress={onDocumentSelect}> 
            <Icon name="file" size={25} color="#7f3dff" />
            <Text style={Styles.iconText}>Document</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

// Modal component
const NextModal = ({modalVisible,setModalVisible, setSelectedDay, setSelectedMonth, setSelectedYear, setIsFrequencyEndAfterVisible,}) => {
  // State for Frequency toggle
  const [openFrequency, setOpenFrequency] = useState(false); 
  // State for End After toggle
  const [openEndAfter, setOpenEndAfter] = useState(false); 

  const [openDay, setOpenDay] = useState(false);
  const [valueDay, setValueDay] = useState(null);
  const days = Array.from({ length: 31 }, (_, i) => ({ label: `${i + 1}`, value: `${i + 1}` }));

  const [openMonth, setOpenMonth] = useState(false);
  const [valueMonth, setValueMonth] = useState(null);
  const months = [
    { label: 'January', value: 'January' },
    { label: 'February', value: 'February' },
    { label: 'March', value: 'March' },
    { label: 'April', value: 'April' },
    { label: 'May', value: 'May' },
    { label: 'June', value: 'June' },
    { label: 'July', value: 'July' },
    { label: 'August', value: 'August' },
    { label: 'September', value: 'September' },
    { label: 'October', value: 'October' },
    { label: 'November', value: 'November' },
    { label: 'December', value: 'December' }
  ];
  
  const [openYear, setOpenYear] = useState(false);
  const [valueYear, setValueYear] = useState(null);
  const currentYear = new Date().getFullYear();
  const years = Array.from({ length: 100 }, (_, i) => ({ label: `${currentYear - i}`, value: `${currentYear - i}` }));
// date picker
const [showDatePicker, setShowDatePicker] = useState(false);
const [selectedDate, setSelectedDate] = useState(new Date());

const handleDateChange = (event) => {
  setShowDatePicker(Platform.OS === 'ios'); // Hide the date picker on iOS after selecting a date
  if (selectedDate) {
    setSelectedDate(selectedDate);
    console.log('Selected Date:', selectedDate);
    // Do something with the selected date
  }
}

  const handleToggleFrequency = () => {
    setOpenFrequency(!openFrequency);
    setOpenEndAfter(false); 
  };

  const handleToggleEndAfter = () => {
    setOpenEndAfter(!openEndAfter);
    setOpenFrequency(false); 
  };
  // Handle Press After next
  const handleNextPress = (selectedDay, selectedMonth, selectedYear) => {
    let frequencyValid = true;
    let endAfterValid = true;
  
    // Validate Frequency selection
    if (!valueDay || !valueMonth || !valueYear) {
      Alert.alert('Error', 'Please select a valid frequency.');
      frequencyValid = false;
    }
  
    // Validate End After selection
    if (!selectedDate) {
      Alert.alert('Error', 'Please select a valid end date.');
      endAfterValid = false;
    }
  
    // Proceed only if both Frequency and End After are valid
    if (frequencyValid && endAfterValid) {
      setModalVisible(false);
      setIsFrequencyEndAfterVisible(true); 
    }
    // passing the vales day month year
    setSelectedDay(valueDay);
    setSelectedMonth(valueMonth);
    setSelectedYear(valueYear);
    setSelectedDate(selectedDate);

  };
  
  // <TouchableOpacity style={Styles.nextbuttonAfter} onPress={handleNextPress}>
  //   <Text style={Styles.nextbuttonAfterText}>Next</Text>
  // </TouchableOpacity>
  

  

  return (
    <Modal animationType="slide" transparent={true} visible={modalVisible} onRequestClose={() => setModalVisible(false)}>
      <View style={Styles.modalContainer}>
      
        <View style={Styles.modalContent}>
      
          <View>
          <View style={Styles.BarNextAfter}></View>

            <TouchableOpacity style={Styles.Frequency} onPress={handleToggleFrequency}>
              <Text style={Styles.FrequencyText}>Frequency</Text>
              <FontAwesome name={openFrequency ? "chevron-up" : "chevron-down"} size={16} color="#91919f" /> 
            </TouchableOpacity>
            {openFrequency && (
              <View style={Styles.row}>
              <DropDownPicker
    open={openDay}
    value={valueDay}
    items={days}
    setOpen={(open) => {
      setOpenDay(open);
      if (open) {
        setOpenMonth(false);
        setOpenYear(false);
      }
    }}
    setValue={(value) => {
      setValueDay(value);
      setOpenDay(false);
      handleDateChange(value);
    }}
    placeholder="Day"
    placeholderStyle={{ color: '#91919f' }}
    selectedItemLabelStyle={{ color: 'blue' }}
    defaultIndex={0}
    containerStyle={Styles.dropdown}
    maxHeight={100}
    style={Styles.DropDownPicker}
    textStyle={{ color: '#7f3dff' }}
    
  />

  <DropDownPicker
    open={openMonth}
    value={valueMonth}
    items={months}
    setOpen={(open) => {
      setOpenMonth(open);
      if (open) {
        setOpenDay(false);
        setOpenYear(false);
      }
    }}
    setValue={(value) => {
      setValueMonth(value);
      setOpenMonth(false);
    }}
    placeholder="Month"
    placeholderStyle={{ color: '#91919f' }}
    selectedItemLabelStyle={{ color: 'blue' }}
    defaultIndex={0}
    containerStyle={Styles.dropdown}
    maxHeight={100}
    style={Styles.DropDownPicker}
    textStyle={{ color: '#7f3dff' }}
  />

  <DropDownPicker
    open={openYear}
    value={valueYear}
    items={years}
    setOpen={(open) => {
      setOpenYear(open);
      if (open) {
        setOpenDay(false);
        setOpenMonth(false);
      }
    }}
    setValue={(value) => {
      setValueYear(value);
      setOpenYear(false);
    }}
    placeholder="Year"
    placeholderStyle={{ color: '#91919f' }}
    selectedItemLabelStyle={{ color: 'blue' }}
    defaultIndex={0}
    containerStyle={Styles.dropdown}
    maxHeight={100}
    style={Styles.DropDownPicker}
    textStyle={{ color: '#7f3dff' }}
  />

              </View>
            )}
            <TouchableOpacity style={Styles.Frequency} onPress={handleToggleEndAfter}>
              <Text style={Styles.FrequencyText}>End After</Text>
              <FontAwesome name={openEndAfter ? "chevron-up" : "chevron-down"} size={16} color="#91919f" /> 
            </TouchableOpacity>
            {openEndAfter && (
              <View style={Styles.row}>
                <TouchableOpacity onPress={() => setShowDatePicker(true)} style={Styles.Datepicker}>
      <Text  style={Styles.DatepickerText}>Date</Text>
      <FontAwesome name= "chevron-down" size={16} color="#91919f" />
    </TouchableOpacity>
    {showDatePicker && (
      <DateTimePicker
        value={selectedDate}
        mode="date"
        display="default"
        onChange={handleDateChange}
        
      />
    )}
    {selectedDate && (
      <View style={Styles.DatepickerDisplay}>
      <Text style={Styles.DatepickerDisplayText}>{selectedDate.toDateString()}</Text>
      </View>
    )}
              </View>
            )}
            {/* next After BUtton */}
            <TouchableOpacity style={Styles.nextbuttonAfter} onPress={handleNextPress}>
            <Text style={Styles.nextbuttonAfterText}>Next</Text>
          </TouchableOpacity>
          {/* <View style={Styles.selectedValuesContainer}>
  <Text style={Styles.selectedValue}>{valueDay}</Text>
  <Text style={Styles.selectedValue}>{valueMonth}</Text>
  <Text style={Styles.selectedValue}>{valueYear}</Text>
</View> */}

          </View>
         
        </View>
        
      </View>
    </Modal>
  );
};
const AddAttachmentTransfer = ({ navigation, route, }) => {
  const { capturedPhotoUri } = route.params || { capturedPhotoUri: null };
  const { userData } = route.params || {};
  const [toggleOn, setToggleOn] = useState(false);
  const [attachmentModalVisible, setAttachmentModalVisible] = useState(false);
  const [isImageDisplayed, setIsImageDisplayed] = useState(false);
  const [selectedImage, setSelectedImage] = useState(false); 
  const [isImageSelected, setIsImageSelected] = useState(false);
  const [selectedDocumentImage, setSelectedDocumentImage] = useState(null);
  const [fromValue, setFromValue] = useState('');
  const [toValue, setToValue] = useState('');
  const [fromPlaceholder, setFromPlaceholder] = useState('From');
  const [toPlaceholder, setToPlaceholder] = useState('To');
  // state for description 
  const [description, setDescription] = useState('');
 // next modal
// use state for next modal
const [modalVisible, setModalVisible] = useState(false);
// use state for modalk dropdown next


//hide the frequency and endafter
const [isFrequencyEndAfterVisible, setIsFrequencyEndAfterVisible] = useState(false);

// Alert useState
const [showAlert, setShowAlert] = useState(false);

// custom Alert
const CustomAlert = () => (
  <TouchableWithoutFeedback onPress={handlepressAlertOk}>
  <View style={Styles.customAlertContainer}>
      <View style={Styles.customAlert}>
          <Icon name="check-circle" style={Styles.alertIcon} />
          <Text style={Styles.alertText}>Transaction has been successfully removed</Text>
          <TouchableOpacity onPress={handlepressAlertOk}>
          <Text style={Styles.alertTextOK}>OK</Text>
          </TouchableOpacity>

      </View>
  </View>
  </TouchableWithoutFeedback>
);
// handle press Ok Alert Navigation
handlepressAlertOk = () =>{
  navigation.navigate('Dashboard', { userData: userData });
}

// open gallery handle
const onOpenGallery = async () => {
  const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
  // console.log("Gallery permission status:", status);
  if (status !== 'granted') {
    Alert.alert('Permission Denied', 'Sorry, we need camera roll permissions to make this work!');
    return;
  }
  const result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: true,
    quality: 1,
  });
  // console.log("Image picker result:", result);
  // console.log("Selected image URI:", result.uri);
  if (!result.canceled) {
    setSelectedImage(result.assets[0].uri); 
    console.log("Selected image:", result.assets[0].uri); 
    setIsImageSelected(true);
    setAttachmentModalVisible(false); 
  }
};
// gallery end

// open camera handle
  const handleCameraClick = () => {
    navigation.navigate('CameraScreen'); 
    setAttachmentModalVisible(false); 
  };
  const handleRemoveImage = () => {
    setIsImageDisplayed(false);
    setSelectedImage(null);
    setIsImageSelected(false);
    const updatedParams = { ...route.params, capturedPhotoUri: null }; // Update the capturedPhotoUri in route params
    navigation.setParams(updatedParams); 
};

//handle document selection
  const onDocumentSelect = async () => {
    try {
      const document = await DocumentPicker.getDocumentAsync();
      console.log("Selected document:", document);
  
      if (!document.canceled && document.assets.length > 0) {
        console.log("Selected document URI:", document.assets[0].uri);
        setSelectedDocumentImage(document.assets[0].uri);
        setAttachmentModalVisible(false);
      } else {
        console.log("Document selection was canceled.");
      }
    } catch (error) {
      // error
      console.error("Error selecting document:", error);
    }
  }




const handleContinuePress = () => {
 

  if (!description.trim()) {
    Alert.alert('Error', 'Please enter a description.');
    return;
  }

  

  if (!selectedImage && !selectedDocumentImage && !capturedPhotoUri) {
    Alert.alert('Error', 'Please add an attachment.');
    return;
  }
  if (isFrequencyEndAfterVisible) {
    setModalVisible(false);
    setShowAlert(true);
    return;
    
  }
  // next modal visible
  setModalVisible(true);
  };

// handlecontinuepressEdit
const handleContinuePressEdit = () =>{
  // next modal visible
  setModalVisible(true);
}
//  Toggle Swich for Repeat
  const handleToggleSwitch = () => {
    setToggleOn(!toggleOn);
  };
// Handle Attchment button
  const handleAttachmentPress = () => {
    setAttachmentModalVisible(true);
  };


  // select values frequency an end after
  const [selectedDay, setSelectedDay] = useState('');
  const [selectedMonth, setSelectedMonth] = useState('');
  const [selectedYear, setSelectedYear] = useState('');
  const [selectedDate, setSelectedDate] = useState(new Date());

//   For Tnasfer Changing From And To
const handleExchangePress = () => {
    const tempValue = fromValue;
    const tempPlaceholder = fromPlaceholder;
    setFromValue(toValue);
    setFromPlaceholder(toPlaceholder);
    setToValue(tempValue);
    setToPlaceholder(tempPlaceholder);
  };

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps="handled">
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar backgroundColor="#0077ff" barStyle="light-content"/>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          enabled
          keyboardVerticalOffset={Platform.select({ ios: 0, android: 30 })}>

           <View style={Styles.DetailTransactionExpenseTopHead}>
           <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
           <Icon name='arrow-left' style={Styles.arrowleftDeatailExpense}></Icon>
            </TouchableOpacity>
            <Text style={Styles.DetailTransactionExpenseText}>Transfer</Text>
            <TouchableOpacity >
              {/* <Icon name='trash' style={Styles.trashDeatailExpense}></Icon> */}
            </TouchableOpacity>
          </View>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={{ flexGrow: 1 }}>
              <View style={Styles.Walletcontainermain}>
                <View style={Styles.Walletcontainer}>
                  <View style={[Styles.WalletBalancepaisa, { padding: wp('5%') }]}>
                    <Text style={Styles.WalletBalance}>How much?</Text>
                    <Text style={Styles.Walletpaisa}>$0</Text>
                  </View>
                  <View style={Styles.Walletbox}>
                    {/* Transfer From and To */}
                    <View style={Styles.inputfromto}>
                      <TextInput
                        style={[Styles.inputfrom, Styles.inputMargin]}
                        placeholder={fromPlaceholder}
                        value={fromValue}
                        onChangeText={setFromValue}
                      />
                      <TouchableOpacity style={Styles.iconContainerexchange} onPress={handleExchangePress}>
                        <Icon name="exchange" size={15} color="#7f3dff"/>
                      </TouchableOpacity>
                      <TextInput
                        style={[Styles.inputto, Styles.inputMargin]}
                        placeholder={toPlaceholder}
                        value={toValue}
                        onChangeText={setToValue}
                      />
                     </View>
                    <TextInput
                      style={[Styles.NewAccountInput, { paddingVertical: hp('1.5%'), paddingHorizontal: wp('5%') }]}
                      placeholder="Description"
                      value={description}
                      onChangeText={setDescription}
                    />
                    
  
                    {!capturedPhotoUri && !selectedImage && !isImageDisplayed && !selectedDocumentImage && (
                      <TouchableOpacity style={Styles.box} onPress={handleAttachmentPress}>
                        <View style={Styles.attachmentButton}>
                          <Icon name="paperclip" size={20} color="grey" />
                          <Text style={Styles.addAttachmentText}>Add Attachment</Text>
                        </View>
                      </TouchableOpacity>
                    )}
                    {selectedImage && (
                      <View style={Styles.imageAttach}>
                        <TouchableOpacity onPress={() => setSelectedImage(null)} style={Styles.closeIcon}>
                          <Icon name="close" size={15} color="#ffffff" style={Styles.maincloseicon} />
                        </TouchableOpacity>
                        <Image source={{ uri: selectedImage }} style={Styles.AttachImage} />
                      </View>
                    )}
                    {capturedPhotoUri && (
                      <View key={capturedPhotoUri} style={Styles.imageAttach}>
                        <TouchableOpacity onPress={handleRemoveImage} style={Styles.closeIcon}>
                          <Icon name="close" size={15} color="#ffffff" style={Styles.maincloseicon} />
                        </TouchableOpacity>
                        <Image source={{ uri: capturedPhotoUri }} style={Styles.AttachImage} />
                      </View>
                    )}
                    {selectedDocumentImage && (
                      <View style={Styles.imageAttach}>
                        <TouchableOpacity onPress={() => setSelectedDocumentImage(null)} style={Styles.closeIcon}>
                          <Icon name="close" size={15} color="#ffffff" style={Styles.maincloseicon} />
                        </TouchableOpacity>
                        <Image source={{ uri: selectedDocumentImage }} style={Styles.AttachImage} />
                      </View>
                    )}
  
                    {isFrequencyEndAfterVisible && (
                      <View style={Styles.FrequencyEndafter}>
                        <View style={Styles.freendselect}>
                          <Text style={Styles.freendselectText1}>Frequency</Text>
                          <Text style={Styles.freendselectText2}>{selectedDay} {selectedMonth} {selectedYear}</Text>
                        </View>
                        <View style={Styles.freendselect}>
                          <Text style={Styles.freendselectText1}>End After</Text>
                          <Text style={Styles.freendselectText2}>{selectedDate.toDateString()}</Text>
                        </View>
                        <TouchableOpacity style={Styles.freendselectEdit} onPress={handleContinuePressEdit}>
                          <Text style={Styles.freendselectTextEdit}>Edit</Text>
                        </TouchableOpacity>
                      </View>
                    )}
  
                    <AttachmentModal
                      isVisible={attachmentModalVisible}
                      onClose={() => setAttachmentModalVisible(false)}
                      onCameraClick={handleCameraClick}
                      onOpenGallery={onOpenGallery}
                      onDocumentSelect={onDocumentSelect}
                    />
                    <TouchableOpacity
                      style={Styles.WalletcontinueButton}
                      onPress={handleContinuePress}>
                      <Text style={Styles.WalletcontinueButtonText}>Continue</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <NextModal
                modalVisible={modalVisible}
                setModalVisible={setModalVisible}
                setSelectedDay={setSelectedDay}
                setSelectedMonth={setSelectedMonth}
                setSelectedYear={setSelectedYear}
                setSelectedDate={setSelectedDate}
                setIsFrequencyEndAfterVisible={setIsFrequencyEndAfterVisible}
              />
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
          {/* Custom Alert */}
      {showAlert && <CustomAlert />}
      </SafeAreaView>
    </ScrollView>
  );
  
};

export default AddAttachmentTransfer;
const Styles = Stylesheet.create({
  DetailTransactionExpenseTopHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor:'#0077ff',
    padding:20
  },
  arrowleftDeatailExpense: {
    fontSize: hp('2.5%'),
    color: '#fff',
  },
  DetailTransactionExpenseText: {
    fontSize: hp('2.5%'),
    color: '#fff',
  },
  trashDeatailExpense: {
    fontSize: hp('2.5%'),
    color: '#fff',
  },
  Walletcontainermain:{
    flex: 1,
    backgroundColor: '#0077ff',
  },
  Walletcontainer: {
    position:'absolute',
    bottom:0,
    width:"100%"
  },
  WalletBalancepaisa: {
    padding: wp('5%')
  },
  WalletBalance: {
    fontSize: wp('5%'),
    color: '#FFFFFF'
  },
  Walletpaisa: {
    color: 'white',
    fontWeight: '600',
    fontSize: wp('10%'),
  },
  Walletbox: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: wp('5%'),
    borderTopRightRadius: wp('5%'),
    borderWidth: 1,
    borderColor: 'gray',
    width: '100%',
  },
  inputfromto:{
    flexDirection:'row',
    alignItems:'center',
    marginHorizontal: wp('5%'),
    position:'relative',
    marginTop:hp('2%')
  },
  inputfrom: {
    flex: 1,
    paddingVertical: hp('1.5%'), 
    paddingHorizontal: wp('5%'), 
    marginRight:wp('1%'),
    marginBottom:hp('2%'),
    color: 'black',
    borderWidth: 1,
    borderRadius: wp('2%'), 
    borderColor: 'lightgrey',
    zIndex:-1
  },
  inputto:{
    flex: 1,
    paddingVertical: hp('1.5%'), 
    paddingHorizontal: wp('5%'), 
    marginLeft:wp('1%'),
    marginBottom:hp('2%'),
    color: 'black',
    borderWidth: 1,
    borderRadius: wp('2%'), 
    borderColor: 'lightgrey',
    zIndex:-1
  },
iconContainerexchange: {
    justifyContent: 'center',
    alignItems: 'center',
    height:35,
    width:35,
    borderColor: 'lightgray',
    borderWidth:1,
    backgroundColor: '#fff',
    borderRadius:wp('50%'),
    position:'absolute',
    right:wp('40%'),
    top:hp('1%'),

  },
  NewAccountInput: {
    paddingVertical: hp('2%'), 
    paddingHorizontal: wp('5%'), 
    marginHorizontal: wp('5%'),
    color: 'black',
    borderWidth: 1,
    borderRadius: wp('2%'), 
    borderColor: 'lightgrey'
  },
  WallettypeDropdown: {
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    borderWidth: 1,
    borderColor: 'lightgrey',
    borderRadius: wp('2%'),
    margin: wp('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  WallettypeDropdown1: {
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    borderWidth: 1,
    borderColor: 'lightgrey',
    borderRadius: wp('2%'),
    marginHorizontal: wp('5%'),
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  Walletdropdown1: {
    marginHorizontal: wp('10%'),
    marginBottom: hp('1%')
  },
  dotcategory: {
    width: 10,
    height: 10,
    borderRadius:20,
    marginRight:10,
    backgroundColor: 'green',
  },
  selectedCategoryContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: 'lightgrey',
    borderWidth: 1,
    borderRadius: wp('50%'),
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('.5%'),
  },
  Walletdropdown2: {
    marginHorizontal: wp('10%'),
  },
  WalletdropdownItem: {
    //
  },
  WalletrowContainer: {
    flexDirection: 'column',
    gap: hp('2%'),
    paddingBottom: 0,
    flexWrap: 'wrap',
    alignItems: 'left',
    justifyContent: 'center',
  },
  WalletcontinueButton: {
    backgroundColor: '#7F3DFF',
    borderRadius: wp('2%'),
    paddingVertical: hp('2%'),
    marginHorizontal: wp('5%'),
    alignItems: 'center',
    marginBottom: hp('2%'),
  },
  WalletcontinueButtonText: {
    color: '#FFFFFF',
    fontSize: wp('4%'),
  },
  box: {
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    margin: wp('5%'),
    paddingVertical: hp('1.7%'),
    alignItems: 'center',
    borderRadius: wp('2%'),
    borderColor: 'gray',
    borderWidth: 1,
    borderStyle: 'dashed'
  },
  attachmentButton: {
    flexDirection: 'row',
    gap: wp('2%'),
    textAlign: 'center',
  },
  addAttachmentText: {
    color: 'grey'
  },
  smallText: {
    fontSize: wp('3%'),
    color: '#888',
  },
  itemText: {
    fontSize: wp('4%'),
    color: 'black',
  },
  itemno: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: wp('10%'),
    marginBottom: hp('2%')
  },
  toggleIcon: {
    fontSize: wp('8%'),
  },
  // modal
  attachmentButton: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: wp('2%'),
  },
  addAttachmentText: {
    marginLeft: wp('2%'),
    color: 'grey',
    fontSize: wp('4%'),
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#fff',
    borderTopLeftRadius: wp('5%'),
    borderTopRightRadius: wp('5%'),
    paddingHorizontal: wp('5%'),
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between'
  },
  iconContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor:'#eee5ff',
    width: wp('25%'),
    height: hp('12%'),
    justifyContent:'center',
    marginBottom: hp('6%'),
    marginTop: hp('8%'),
    borderRadius: wp('5%'),
  },
  iconText: {
    marginTop: hp('1%'),
    color:'#7f3dff',
    fontSize: wp('4%'),
    fontWeight:'600'
  },

  // image Attach
  imageAttach:{
    flex: 1, 
    alignItems:'flex-start',
    justifyContent: 'center',
    marginHorizontal:30,
    marginVertical:10,
    borderRadius:10

  },
  AttachImage:{
    width:130,
    height:130,
    borderRadius:20,
    position:"relative",
    zIndex:-1

  },

  maincloseicon:{
    position:"absolute",
    marginLeft:110,
    top:-5,
    backgroundColor:"rgba(173,173,173,0.6)",
    width:"7%",
    padding:5, 
    textAlign:"center",
    borderRadius:40
    },

  // after next modal 
  
    //  next modal  inside 
    row: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      gap: wp('2%'),
    },
    dropdown: {
      width: (wp('100%') - wp('20%')) / 3,
      paddingVertical: hp('1%'),
    },
    dropdown2: {
      width: (wp('100%') - wp('20%')) / 2,
      paddingVertical: hp('1%'),
    },
    BarNextAfter: {
      height: hp('0.5%'),
      width: wp('10%'),
      backgroundColor: '#d3bdff',
      marginVertical: hp('1%'),
      alignSelf: 'center',
      borderRadius: wp('2%'),
    },
    Frequency: {
      paddingVertical: hp('2.2%'),
      backgroundColor: '#fff',
      width: '100%',
      flexDirection: 'row', 
      justifyContent: 'space-between', 
      alignItems: 'center', 
      paddingHorizontal: wp('5%'),
      marginVertical: hp('2%'),
      borderWidth:1,
      borderColor:'#91919f',
      borderRadius: wp('2%'),
    },
    FrequencyEndafter:{
      backgroundColor: '#fff',
      width: '100%',
      flexDirection: 'row', 
      justifyContent: 'space-between', 
      alignItems: 'center', 
      paddingHorizontal: wp('9.5%'),
      marginVertical: hp('2%'),
    },
    freendselect:{
      gap:5
    },
    freendselectText1:{
     color:'#000',
     fontSize:17,
     fontWeight:'400'
    },
    freendselectText2:{
      color:'#91919f',
      fontWeight:'400'
     },
     freendselectEdit:{
      backgroundColor:'#eee5ff',
      paddingVertical:8,
      paddingHorizontal:18,
      borderRadius:25
     },
     freendselectTextEdit:{
      color:'#7f3dff',
      fontSize:12,
      fontWeight:'500'
     },
    Datepicker:{
      width: wp('40%'),
      flexDirection:"row",
      borderWidth:1,
      borderColor:'#91919f',
      alignItems:'center',
      justifyContent:'space-between',
      paddingVertical: hp('1.5%'),
      marginBottom: hp('1.5%'),
      borderRadius: wp('2%'),
      paddingHorizontal: wp('3%'),
    },
    DatepickerDisplay:{
      width: wp('50%'),
      borderWidth:1,
      borderColor:'#91919f',
      alignItems:'center',
      paddingVertical: hp('1.5%'),
      marginBottom: hp('1.5%'),
      borderRadius: wp('2%'),
    },
    DatepickerDisplayText:{
      color:'#7f3dff'
    },
    nextbuttonAfter:{
      backgroundColor:'#7f3dff',
      paddingVertical: hp('2%'),
      marginVertical: hp('1.5%'),
      borderRadius: wp('2%'),
      alignItems:'center',
    },
    nextbuttonAfterText:{
      fontSize: hp('2.2%'),
      fontWeight:'500',
      color:'#fff'
    },
    //   Alert
overlay: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
},
customAlertContainer: {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.5)', 
  justifyContent: 'center',
  alignItems: 'center',
},
customAlert: {
  backgroundColor: '#fff',
  padding: wp('5%'), 
  borderRadius: hp('2%'), 
  alignItems: 'center',
},
alertIcon: {
  fontSize: hp('7%'), 
  color: '#5233ff',
  marginBottom: hp('1%'), 
},
alertText: {
  fontSize: hp('2%'), 
  textAlign: 'center',
},
alertTextOK:{
  backgroundColor: '#5233ff',
    color: '#fff',
    paddingHorizontal: wp('4%'), 
    paddingVertical: hp('1.5%'), 
    marginTop: hp('1%'), 
    borderRadius: wp('2.5%'),
    fontWeight:'600',
}

});
