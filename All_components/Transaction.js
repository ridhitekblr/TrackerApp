import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, Animated, Pressable, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const Transaction = ({navigation,isExpense}) => {
  const [selectedMonth, setSelectedMonth] = useState(null);
  const [isModalVisible, setModalVisible] = useState(false);
  const [modalScale] = useState(new Animated.Value(0)); 

  useEffect(() => {
    if (isModalVisible) {
      // Animate modal scale to 1 when modal becomes visible
      Animated.spring(modalScale, {
        toValue: 1,
        useNativeDriver: true,
      }).start();
    }
  }, [isModalVisible, modalScale]);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const handleMonthSelect = (month) => {
    setSelectedMonth(month);
    toggleModal();
  };

  const months = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];

  // Filter Apply
  const [modalVisible, setModalVisibleFil] = useState(false);
  const [selectedFilters, setSelectedFilters] = useState({
    filterBy: [],
    sortBy: [],
  });
  const [resetClicked, setResetClicked] = useState(false);

  const toggleFilter = (category, option) => {
    setSelectedFilters((prev) => {
      const isSelected = prev[category].includes(option);
      if (isSelected) {
        return {
          ...prev,
          [category]: prev[category].filter((item) => item !== option),
        };
      } else {
        return {
          ...prev,
          [category]: [...prev[category], option],
        };
      }
    });
  };

  const isFilterSelected = (category, option) => selectedFilters[category].includes(option);

  const resetFilters = () => {
    setSelectedFilters({ filterBy: [], sortBy: [] });
    setResetClicked(true);
    setTimeout(() => setResetClicked(false), 5000); 
  };

  // Sample transaction data
  const transactions = [
    { id: '1', label: 'Shopping', description: 'Buy some grocery', money: '- $120', icon: <MaterialCommunityIcons name='shopping' style={styles.shopping}></MaterialCommunityIcons>, time: '10:00 AM',date: '2024-05-19',type: 'DetailTransactionExpense'},
    { id: '2', label: 'Subscription', description: 'Disney+ Annual..', money: '- $80', icon: <MaterialCommunityIcons name='note' style={styles.note}></MaterialCommunityIcons>, time: '03:30 PM',date: '2024-05-19',type: 'DetailTransactionExpense' },
    { id: '3', label: 'Food', description: 'Buy a ramen', money: '- $32', icon: <MaterialCommunityIcons name='food' style={styles.food}></MaterialCommunityIcons>, time: '07:30 PM',date: '2024-05-19',type: 'DetailTransactionExpense' },
    { id: '5', label: 'Salary', description: 'Monthly Salary', money: '+ $3000', icon: <MaterialCommunityIcons name='cash' style={styles.salary}></MaterialCommunityIcons>, time: '01:00 PM',date: '2024-05-18',type: 'DetailTransactionIncome' },
    { id: '6', label: 'Freelancing', description: 'Project Work', money: '+ $1500', icon: <MaterialCommunityIcons name='laptop' style={styles.freelancing}></MaterialCommunityIcons>, time: '11:00 AM',date: '2024-05-18',type: 'DetailTransactionTransfer'},
  ];

  const today = '2024-05-19';
  const yesterday = '2024-05-18';

  const todaysTransactions = transactions.filter(
    (transaction) => transaction.date === today
  );
  const yesterdaysTransactions = transactions.filter(
    (transaction) => transaction.date === yesterday
  );

  const renderItem = ({ item }) => (
    <View style={{ paddingVertical: 10, paddingHorizontal: 30 }}>
    <TouchableOpacity  onPress={() => navigation.navigate(item.type)}>
    <View style={styles.listTransaction}>
    
      <View style={styles.iconwithlabdes}>
        <View style={styles.TransactionIconView}>
          <View style={styles.TransactionIcon}>{item.icon}</View>
        </View>
        <View style={styles.TransactionLabDes}>
          <Text style={styles.TransactionLable}>{item.label}</Text>
          <Text style={styles.TransactionDescription}>{item.description}</Text>
        </View>
      </View>
      <View style={styles.TransactionMoneyTime}>
      <Text style={[styles.TransactionMoney, isExpense ? { color: '#fd3c4a' } : { color: '#00a86b' }]}>{item.money}</Text>
        <Text style={styles.TransactionTime}>{item.time}</Text>
      </View>
    </View>
    </TouchableOpacity>
  </View>
  );

  return (
    <View style={styles.container}>
      {/* Status Bar */}
      <View style={styles.MonthBarHead}>
        
        <TouchableOpacity style={styles.monthSelector} onPress={toggleModal}>
          <Icon name="angle-down" size={26} color="#7f3dff" />
          <Text style={styles.monthText}>
            {selectedMonth ? selectedMonth : 'Month'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setModalVisibleFil(true)}>
          <Icon name="bars" size={26} color="#000" />
        </TouchableOpacity>
       
      </View>
      <Pressable style={styles.SeeFinancialRep} onPress={() => navigation.navigate('FinancialReport')}>
        <Text style={styles.SeeFinancialRepText}>See your financial report</Text>
        <Icon name="angle-right" size={36} color="#7f3dff" />
      </Pressable>
      
      {/* Today's Transactions */}
      <Text style={styles.sectionHeaderTransaction}>Today</Text>
      <FlatList
        data={todaysTransactions}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        style={styles.transactionList}
        
      />
      
      {/* Yesterday's Transactions */}
      <Text style={styles.sectionHeaderTransaction}>Yesterday</Text>
      <FlatList
        data={yesterdaysTransactions}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        style={styles.transactionList}
      />

      {/* Modal for month */}
      <Modal
        animationType="fade"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={toggleModal}
      >
        <View style={styles.modalContainer}>
          <Animated.View style={[styles.modalContent, { transform: [{ scale: modalScale }] }]}>
            <Text style={styles.modalTitle}>Select Month</Text>
            {months.map((month, index) => (
              <TouchableOpacity
                key={index}
                style={styles.monthButton}
                onPress={() => handleMonthSelect(month)}
              >
                <Text style={styles.monthButtonText}>{month}</Text>
              </TouchableOpacity>
            ))}
            {/* <TouchableOpacity style={styles.closeButtonForTransMod} onPress={toggleModal}>
              <Text style={styles.closeButtonText}>Close</Text>
            </TouchableOpacity> */}
          </Animated.View>
        </View>
      </Modal>

      {/* modal Filter */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisibleFil(!modalVisible)}
      >
        <View style={styles.modalOverlay}>
          <View style={styles.modalView}>
            <View style={styles.modalBarLine} />
            <View style={styles.header}>
              <Text style={styles.headerText}>Filter Transaction</Text>
              <Text
                style={[
                  styles.resetText,
                  resetClicked && styles.resetClickedText,
                ]}
                onPress={resetFilters}
              >
                Reset
              </Text>
            </View>
            <View style={styles.filterSection}>
              <Text style={styles.sectionTitle}>Filter By</Text>
              <View style={styles.filterOptions}>
                {['Income', 'Expense', 'Transfer'].map((option) => (
                  <TouchableOpacity
                    key={option}
                    style={[
                      styles.filterOption,
                      isFilterSelected('filterBy', option) && styles.selectedOption,
                    ]}
                    onPress={() => toggleFilter('filterBy', option)}
                  >
                    <Text
                      style={[
                        styles.filterOptionText,
                        isFilterSelected('filterBy', option) && styles.selectedOptionText,
                      ]}
                    >
                      {option}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
            </View>
            <View style={styles.filterSection}>
              <Text style={styles.sectionTitle}>Sort By</Text>
              <View style={styles.filterOptions}>
                {['Highest', 'Lowest', 'Newest', 'Oldest'].map((option) => (
                  <TouchableOpacity
                    key={option}
                    style={[
                      styles.filterOption,
                      isFilterSelected('sortBy', option) && styles.selectedOption,
                    ]}
                    onPress={() => toggleFilter('sortBy', option)}
                  >
                    <Text
                      style={[
                        styles.filterOptionText,
                        isFilterSelected('sortBy', option) && styles.selectedOptionText,
                      ]}
                    >
                      {option}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
            </View>
            <View style={styles.filterSection}>
              <Text style={styles.sectionTitle}>Category</Text>
              <View style={styles.categorySelection}>
                <Text style={styles.chooseCategory}>Choose Category</Text>
                <View style={styles.selectedCategory}>
                  <Text>Selected</Text>
                  <Icon name="angle-right" size={26} color="blue" />
                </View>
              </View>
            </View>
            <TouchableOpacity style={styles.applyButton} onPress={() => {/* Add apply functionality */}}>
              <Text style={styles.applyButtonText}>Apply</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default Transaction;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  MonthBarHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop:20,
    paddingBottom: 10,
    backgroundColor: '#fff',
   
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
  },
  monthSelector: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 50,
    flexDirection:'row',
    borderWidth:1,
    borderColor:'#f1f1fa',
    paddingVertical:8,
    paddingHorizontal:15,
    gap:10
    

  },
  monthText: {
    color: '#000',
    fontSize: 18,

   
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    width: 200,
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
  },
  modalTitle: {
    fontSize: 18,
    marginBottom: 10,
    color:'#7f3dff',
    
  },
  monthButton: {
    paddingVertical: 10,
    width: '100%',
    alignItems: 'center',
  },
  monthButtonText: {
    fontSize: 14,

  },
  closeButtonForTransMod: {
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal:10,
    backgroundColor: '#007bff',
    borderRadius: 5,
    
  },
  closeButtonText: {
    color: '#fff',
    fontSize: 14,
  },

  // modal for filter
  modalOverlay: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalView: {
    width: '100%',
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    paddingBottom: 20,
  },
  modalBarLine: {
    height: 4,
    width: '10%',
    backgroundColor: '#d3bdff',
    marginTop: 10,
    marginBottom: 20,
    alignSelf: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
  },
  resetText: {
    borderWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 50,
    fontSize: 16,
    fontWeight: '400',
    borderColor: '#e3e3e5',
    color: 'black',
  },
  resetClickedText: {
    color: '#7f3dff',
    borderColor: '#7f3dff',
    backgroundColor:'#eee5ff'
  },
  filterSection: {
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 10,
  },
  filterOptions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  filterOption: {
    borderWidth: 1,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 50,
    fontSize: 16,
    fontWeight: '400',
    borderColor: '#e3e3e5',
    marginVertical: 5,
  },
  filterOptionText: {
    color: 'black',
  },
  selectedOption: {
    backgroundColor: '#eee5ff',
    borderColor: '#7f3dff',
  },
  selectedOptionText: {
    color: '#7f3dff',
  },
  categorySelection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  chooseCategory: {
    fontSize: 16,
  },
  selectedCategory: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
  },
  applyButton: {
    alignItems: 'center',
    backgroundColor: '#7f3dff',
    marginHorizontal: 20,
    paddingVertical: 15,
    borderRadius: 10,
    marginTop: 20,
  },
  applyButtonText: {
    fontSize: 20,
    color: '#fff',
    fontWeight: '600',
  },
  // Report BUtton
  SeeFinancialRep:{
    flexDirection:'row',
    alignItems: 'center',
    backgroundColor:'#eee5ff',
    justifyContent:'space-between',
    paddingHorizontal:35,
    paddingVertical:12,
    marginHorizontal:20,
    borderRadius:10,
    marginTop:10,
    marginBottom:20,
  },
  SeeFinancialRepText:{
    fontSize:18,
    color: '#7f3dff',
  },
  // For Flat list
  sectionHeaderTransaction:{
    paddingHorizontal:20,
    fontSize:20,
    fontWeight:'500'

  },
  listTransaction: {
    backgroundColor: '#fcfcfa',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderRadius: 30,
  },
  iconwithlabdes: {
    flexDirection: 'row',
    gap: 10,
  },
  TransactionIcon: {
    color: '#00a86b',
    backgroundColor: '#ffffff',
    borderRadius: 20,
    padding: 5,
    borderColor: '#f1f1fa',
    borderWidth: 1,
  },
  shopping: {
    fontSize: 35,
    color: '#fcac12',
  },
  note: {
    fontSize: 35,
    color: '#7f3dff',
  },
  food: {
    fontSize: 35,
    color: '#fd3c4a',
  },
  salary: {
    fontSize: 35,
    color: '#00a86b',
  },
  freelancing: {
    fontSize: 35,
    color: '#ffa500',
  },
  TransactionLabDes: {
    gap: 5,
  },
  TransactionLable: {
    fontSize: 18,
    color: '#000000',
  },
  TransactionDescription: {
    color: '#91919f',
    fontSize: 14,
  },
  TransactionMoneyTime: {
    gap: 5,
  },
  TransactionMoney: {
    fontSize: 18,
    color: '#fd3c4a',
  },
  TransactionTime: {
    color: '#91919f',
    fontSize: 14,
  },
});
