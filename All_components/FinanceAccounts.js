import React from 'react';
import { SafeAreaView, globalStylesheet, Text, View, StatusBar, Image, ImageBackground, TouchableOpacity, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { globalStyles } from './globalStyles';

// Sample data for demonstration
const walletData = [
    { id: '1', icon: require('../assets/wallet.png'), name: 'Wallet', amount: '$400' },
    { id: '2', icon: require('../assets/chase.png'), name: 'Chase', amount: '$1000' },
    { id: '3', icon: require('../assets/citi.png'), name: 'Citi', amount: '$6000' },
    { id: '4', icon: require('../assets/paypal.png'), name: 'Paypal', amount: '$2000' },
];

const Financeaccounts = ({navigation, route}) => {
    const { userData } = route.params || {};
    // Render item function for FlatList
    const renderWalletItem = ({ item }) => (
        <View style={globalStyles.sectionfinancecontcontainer}>
            <View style={globalStyles.sectionfinancecont}>
                <Image style={globalStyles.imagewallet} source={item.icon} />
                <Text style={globalStyles.sectiontext}>{item.name}</Text>
            </View>
            <Text style={globalStyles.sectionamounttext}>{item.amount}</Text>
        </View>
    );

    return (
        <SafeAreaView style={globalStyles.financeaccountcontainer}>
            <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
            <View style={globalStyles.financeaccountcontent}>
            <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
                <MaterialCommunityIcons name="arrow-left-thin" size={35} color="black" style={globalStyles.FinanceaccountsIcon} />
                </TouchableOpacity>
                <Text style={globalStyles.Financeaccountheading}>Account</Text>
            </View>
            <ImageBackground source={require('../assets/BG.png')}>
                <View style={globalStyles.accountDetails}>
                    <Text style={globalStyles.accountText}>Account Balance</Text>
                    <Text style={globalStyles.balanceText}>$9400</Text>
                </View>
            </ImageBackground>
            <FlatList
                data={walletData}
                renderItem={renderWalletItem}
                keyExtractor={item => item.id}
                ItemSeparatorComponent={() => <View style={globalStyles.horizontalLine} />}
            />
            <TouchableOpacity style={globalStyles.addnewButton} onPress={() => navigation.navigate('SetupAccount')} >
                <Text style={globalStyles.addnewButtonText}>+ Add new wallet</Text>
            </TouchableOpacity>
        </SafeAreaView>
    );
}

export default Financeaccounts;

// const globalStyles = globalStylesheet.create({
//     financeaccountcontainer: {
//         flex: 1,
//         paddingTop: StatusBar.currentHeight,
//     },
//     financeaccountcontent: {
//         paddingHorizontal: wp('5%'),
//         flexDirection: 'row',
//         marginVertical: hp('1%')
//     },
//     Financeaccountheading: {
//         fontSize: wp('6%'),
//         fontWeight: 'bold',
//         marginLeft: wp('25%')
//     },
//     accountDetails: {
//         alignItems: 'center',
//         paddingVertical: hp('8%')
//     },
//     accountText: {
//         fontSize: wp('4%'),
//         color: 'gray',
//         paddingBottom: hp('1%')
//     },
//     balanceText: {
//         fontSize: wp('12%'),
//         fontWeight: 'bold',
//     },
//     sectiontext: {
//         fontSize: wp('5%'),
//         marginLeft: wp('2%')
//     },
//     sectionfinancecont: {
//         flexDirection: 'row',
//         alignItems: 'center'
//     },
//     sectionamounttext: {
//         fontSize: wp('6%'),
//         fontWeight: 'bold',
//     },
//     sectionfinancecontcontainer: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         justifyContent: 'space-between',
//         paddingHorizontal: wp('5%'),
//     },
//     horizontalLine: {
//         borderBottomWidth: 1,
//         borderBottomColor: 'lightgray',
//         marginVertical: hp('2%'),
//     },
//     addnewButton: {
//         backgroundColor: '#7F3DFF',
//         borderRadius: wp('2%'),
//         paddingVertical: hp('2%'),
//         marginHorizontal: wp('5%'),
//         alignItems: 'center',
//         position: 'absolute',
//         bottom: hp('2%'),
//         width: '90%',
//     },
//     addnewButtonText: {
//         color: '#FFFFFF',
//         fontSize: wp('4%'),
//     }
// });