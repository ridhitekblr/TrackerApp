import { globalStylesheet, Text, View, TouchableOpacity, SafeAreaView, StatusBar, Image } from 'react-native';
import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

const DetailBudget = () => {
    const navigation = useNavigation();
    const route = useRoute();

    const { category = '1', sliderValue1 = 0, amount = 0 } = route.params || {};

    // Function to get the name of the selected category
    const getCategoryName = (categoryValue) => {
        switch (categoryValue) {
            case '1':
                return 'Shopping';
            case '2':
                return 'Transportation';
            default:
                return '';
        }
    };

    const categoryName = getCategoryName(category);

    const goBack = () => {
        navigation.navigate('BudgetAfter', { category, sliderValue1, amount });
    };

    const navigateToEditBudget = () => {
        navigation.navigate('Create_budget', { category, sliderValue1, amount });
    };

    // Calculate the percentage of the remaining amount
    const remainingPercentage = (sliderValue1 / amount) * 100;

    const getCategoryInfo = (value) => {
        const data = [
            { label: 'Shopping', value: '1', color: '#fcac12', icon: require('../assets/shopping-bag.png') },
            { label: 'Transportation', value: '2', color: '#0077ff', icon: require('../assets/car.png') },
        ];
        const selectedCategory = data.find(item => item.value === value);
        return selectedCategory ? selectedCategory : null;
    };

    const selectedCategoryInfo = getCategoryInfo(category);
    const spentPercentage = (amount / 1200) * 100;
    const thumbColor = selectedCategoryInfo ? selectedCategoryInfo.color : '#7F3DFF';

    return (
        <SafeAreaView style={globalStyles. containerdetailbudget}>
            <StatusBar backgroundColor="transparent" barStyle="dark-content" />
            <View style={globalStyles.headerdetailbudget}>
                <TouchableOpacity onPress={goBack} accessibilityLabel="Go Back" accessibilityHint="Navigates to the previous screen">
                    <MaterialCommunityIcons name="arrow-left-thin" solid style={globalStyles.iconsdetailbudget} />
                </TouchableOpacity>
                <Text style={globalStyles.headerTextdetailbudget}>Detail Budget</Text>
                <TouchableOpacity accessibilityLabel="Delete Budget" accessibilityHint="Deletes the current budget">
                    <MaterialCommunityIcons name="trash-can-outline" solid style={globalStyles.biniconsdetailbudget} />
                </TouchableOpacity>
            </View>
            <View style={globalStyles.bodydetailbudget}>
                <View style={globalStyles.detailRemainderContainer}>
                    <View style={globalStyles.categoryContainer}>
                        <Image source={selectedCategoryInfo ? selectedCategoryInfo.icon : null} style={globalStyles.categoryIcon} />
                        <Text style={globalStyles.categoryLabel}>{categoryName}</Text>
                    </View>
                    <View style={globalStyles.detailRemaining}>
                        <Text style={globalStyles.detailRemainingHeading}>Remaining</Text>
                        <Text style={globalStyles.detailRemainingBalance}>${sliderValue1}</Text>
                    </View>
                    <View style={globalStyles.thumbBar}>
                        <View style={[globalStyles.thumbProgress, { backgroundColor: thumbColor, width: `${spentPercentage}%` }]}></View>
                    </View>
                </View>
                <View style={globalStyles.buttonContainerdetailsbudget}>
                    <TouchableOpacity onPress={navigateToEditBudget} style={globalStyles.buttondetailsbudget} accessibilityLabel="Edit Budget" accessibilityHint="Navigates to the budget editing screen">
                        <Text style={globalStyles.buttonTextdetailbudget}>Edit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default DetailBudget;

// const globalStyles = globalStylesheet.create({
//     containerdetailbudget: {
//         flex: 1,
//         backgroundColor: '#fff',
//     },
//     headerdetailbudget: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         paddingHorizontal: wp('5%'),
//         marginTop: hp('4%'),
//         alignItems: 'center',
//         paddingTop: StatusBar.currentHeight,
//     },
//     headerTextdetailbudget: {
//         color: '#000',
//         fontSize: wp('6%'),
//         fontWeight: 'bold',
//     },
//     iconsdetailbudget: {
//         fontSize: 35,
//         color: '#000',
//     },
//     biniconsdetailbudget: {
//         fontSize: 25,
//         color: '#000',
//     },
//     bodydetailbudget: {
//         flex: 1,
//         paddingHorizontal: wp('5%'),
//     },
//     buttonContainerdetailsbudget: {
//         flex: 1,
//         justifyContent: 'flex-end',
//     },
//     buttondetailsbudget: {
//         backgroundColor: '#7f3dff',
//         borderRadius: wp('2%'),
//         paddingVertical: hp('2%'),
//         alignItems: 'center',
//         marginBottom: hp('2%'),
//     },
//     buttonTextdetailbudget: {
//         fontWeight: 'bold',
//         color: '#fff',
//         fontSize: wp('5%'),
//     },
//     detailRemainderContainer: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     detailRemaining: {
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     detailRemainingHeading: {
//         fontSize: wp('7%'),
//         fontWeight: 'bold',
//     },
//     detailRemainingBalance: {
//         fontSize: wp('12%'),
//         fontWeight: 'bold',
//     },
//     thumbBar: {
//         marginTop: hp('3%'),
//         height: 10,
//         backgroundColor: 'lightgray',
//         borderRadius: 5,
//         marginVertical: hp('2%'),
//         width: '90%',
//     },
//     thumbProgress: {
//         height: '100%',
//         borderRadius: 5,
//     },
//     categoryContainer: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         borderWidth: 1,
//         borderColor: 'lightgray',
//         paddingHorizontal: wp('2%'),
//         paddingVertical: hp('1%'),
//         borderRadius: 20,
//         marginBottom: hp('3%'),
//     },
//     categoryIcon: {
//         width: wp('5%'),
//         height: wp('5%'),
//         marginRight: wp('2%'),
//     },
//     categoryLabel: {
//         fontSize: wp('4.5%'),
//     },
//     remainingAmount: {
//         fontSize: wp('4.5%'),
//         marginTop: hp('2%'),
//     },
// });
