import { SafeAreaView, globalStylesheet, Text, View, Image, StatusBar, TouchableOpacity,Modal,TouchableWithoutFeedback} from 'react-native'
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';


const DetailTransactionExpense = ({ navigation, route, }) => {
      const { userData } = route.params || {};
      // use state for modal
      const [showModal, setShowModal] = useState(false);
      // Alert useState
      const [showAlert, setShowAlert] = useState(false);
       // toggle for modal
    const toggleModal = () => {
        setShowModal(!showModal);
      };
    // handle Alert
    const handleYesButton = () => {
        toggleModal(); // Close the modal
        setShowAlert(true); // Show the custom alert
    };
    // Close the custom alert when press the outside of the screen
    const handleOverlayPress = () => {
        setShowAlert(false); 
    };
    const CustomAlert = () => (
        <TouchableWithoutFeedback onPress={handleOverlayPress}>
        <View style={globalStyles.customAlertContainer}>
            <View style={globalStyles.customAlert}>
                <Icon name="check-circle" style={globalStyles.alertIcon} />
                <Text style={globalStyles.alertText}>Transaction has been successfully removed</Text>
            </View>
        </View>
        </TouchableWithoutFeedback>
    );
  return (
    <SafeAreaView style={globalStyles.DetailTransactionExpenseSafe}>
      <StatusBar translucent backgroundColor="#fd3c4a" />
      <View style={globalStyles.DetailTransactionExpenseMain}>
        <View style={globalStyles.DetailTransactionExpenseTopBOTH}>
          <View style={globalStyles.DetailTransactionExpenseTopHead}>
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
            <Icon name='arrow-left' style={globalStyles.arrowleftDeatailExpense}></Icon>
          </TouchableOpacity>
            <Text style={globalStyles.DetailTransactionExpenseText}>Detail Transaction</Text>
            <TouchableOpacity onPress={toggleModal}>
              <Icon name='trash' style={globalStyles.trashDeatailExpense}></Icon>
            </TouchableOpacity>
          </View>
          <View style={globalStyles.DetailTransactionExpenseSub2}>
            <Text style={globalStyles.DetailTransactionExpenseSub2Rupess}>$120</Text>
            <Text style={globalStyles.DetailTransactionExpenseSub2Shoping}>Buy some grocery</Text>
            <View style={globalStyles.DetailTransactionExpenseSu2DayDate}>
              <Text style={globalStyles.DetailTransactionExpenseSub2Day}>Saturday 4 June 2021</Text>
              <Text style={globalStyles.DetailTransactionExpenseSubDate}>16.20</Text>
            </View>
          </View>
        </View>

        <View style={globalStyles.DetailTransactionExpenseSub3}>
          <View style={globalStyles.DetailTransactionExpenseSub3Sub}>
            <Text style={globalStyles.DetailTransactionExpenseSub3Text1}>Type</Text>
            <Text style={globalStyles.DetailTransactionExpenseSub3Text2}>Expense</Text>
          </View>
          <View style={globalStyles.DetailTransactionExpenseSub3Sub}>
            <Text style={globalStyles.DetailTransactionExpenseSub3Text1}>Category</Text>
            <Text style={globalStyles.DetailTransactionExpenseSub3Text2}>Shopping</Text>
          </View>
          <View style={globalStyles.DetailTransactionExpenseSub3Sub}>
            <Text style={globalStyles.DetailTransactionExpenseSub3Text1}>Wallet</Text>
            <Text style={globalStyles.DetailTransactionExpenseSub3Text2}>Wallet</Text>
          </View>
        </View>

        <View style={globalStyles.DetailTransactionExpenseSub4}>
          <Text style={globalStyles.DetailTransactionExpenseSub4Des}>Description</Text>
          <Text style={globalStyles.DetailTransactionExpenseSub4Con}>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</Text>
          <Text style={globalStyles.DetailTransactionExpenseSub4AttachHead}>Attachment</Text>
          <Image
            source={{ uri: 'https://s3-alpha-sig.figma.com/img/49cc/86e6/849cbd762a23c32962df5f832e7fdf9e?Expires=1716163200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=oanIqv2mujPJGvJT9frRJmX4kSv67HlnW7BP3KVL26yYlO3f6OdEPaCWQ504KEEnVkjxw~Hrdo2yIk2yv2xa965t2tQjM2FKPaRdfvytAFHhxWcs7A4ooi8Qt-tIK8~0F9oPFCZ~enJBtxAJs1pWKvoOHkqhY1Gzf11zIF7QF1KDjKSNgn2DGrSHyXdu2Q3JJIOqgr0M~mIP6FGaaUMkDDsYvZxs~0HD5lhkmW4-Yv2U6MIAqIkvrywLUCODJ3iLWLkV0mCTPMD5-FZTwySRwkjiRmC-YeSPySuXadb-HVta2oeVgNh5lmN7gSfrZDRWnZgqnSsSodW9XS~K73V6GQ__' }}
            style={globalStyles.DetailTransactionExpenseSub4Image}
          />
        </View>
      </View>

      {/* Bottom Button */}
      <TouchableOpacity style={globalStyles.DetailTransactionExpenseSub5} >
        <Text style={globalStyles.DetailTransactionExpenseSub5Text}>Edit</Text>
      </TouchableOpacity>
      {/* Modal */}
      <Modal visible={showModal} animationType="slide" transparent>
        <View style={globalStyles.modalContainer}>
          <View style={globalStyles.modalContent}>
            <View style={globalStyles.modalDivider}></View>
            <Text style={globalStyles.modalTitle}>Remove this transaction?</Text>
            <Text style={globalStyles.modalText}>Are you sure you want to remove this transaction?</Text>
            <View style={globalStyles.modalButtonContainer}>
              <TouchableOpacity style={globalStyles.modalButtonNo} onPress={toggleModal}>
                <Text style={globalStyles.modalButtonTextNo}>No</Text>
              </TouchableOpacity>
              <TouchableOpacity style={globalStyles.modalButtonYes} onPress={handleYesButton}>
                <Text style={globalStyles.modalButtonTextYes} >Yes</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* Custom Alert */}
      {showAlert && <CustomAlert />}
    </SafeAreaView>
  )
}

export default DetailTransactionExpense

// const globalStyles = globalStylesheet.create({
//   DetailTransactionExpenseSafe: {
//     flexGrow:1
//   },
//   DetailTransactionExpenseMain: {
//     display:'flex',
//     justifyContent:'center',
//     marginTop: StatusBar.currentHeight || 0,
//   },
//   DetailTransactionExpenseTopBOTH: {
//     backgroundColor: '#fd3c4a',
//     paddingHorizontal: wp('5%'),
//     paddingVertical: hp('5%'),
//     borderBottomLeftRadius: hp('3%'),
//     borderBottomRightRadius: hp('3%'),
//     position: 'relative',
//   },
//   DetailTransactionExpenseTopHead: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   arrowleftDeatailExpense: {
//     fontSize: hp('2.5%'),
//     color: '#fff',
//   },
//   DetailTransactionExpenseText: {
//     fontSize: hp('2.5%'),
//     color: '#fff',
//   },
//   trashDeatailExpense: {
//     fontSize: hp('2.5%'),
//     color: '#fff',
//   },
//   DetailTransactionExpenseSub2: {
//     justifyContent: 'center',
//     alignItems: 'center',
//     paddingTop: hp('4%'),
//     paddingBottom: hp('4%'),
//     gap: hp('1%'),
//   },
//   DetailTransactionExpenseSu2DayDate: {
//     flexDirection: 'row',
//     gap: hp('1%'),
//   },
//   DetailTransactionExpenseSub2Rupess: {
//     fontSize: hp('5%'),
//     fontWeight: '700',
//     color: '#fff',
//   },
//   DetailTransactionExpenseSub2Shoping: {
//     color: '#fff',
//     fontSize: hp('2%'),
//   },
//   DetailTransactionExpenseSub2Day: {
//     color: '#fff',
//     fontSize: hp('2%'),
//   },
//   DetailTransactionExpenseSubDate: {
//     color: '#fff',
//     fontSize: hp('2%'),
//   },
//   DetailTransactionExpenseSub3: {
//     flexDirection: 'row',
//     borderWidth: 1,
//     marginHorizontal: wp('5%'),
//     justifyContent: 'space-around',
//     bottom: hp('4%'),
//     backgroundColor: '#fff',
//     paddingVertical: hp('2a%'),
//     borderRadius: hp('1%'),
//     borderColor: '#f1f1fa',
//   },
//   DetailTransactionExpenseSub3Sub: {
//     alignItems: 'center',
//     gap: hp('1%'),
//   },
//   DetailTransactionExpenseSub3Text1: {
//     color: '#91919f',
//     fontSize: hp('1.7%'),
//     fontWeight: '500',
//   },
//   DetailTransactionExpenseSub3Text2: {
//     fontSize: hp('2%'),
//     fontWeight: '500',
//   },
//   DetailTransactionExpenseSub4: {
//     borderTopWidth: hp('0.2%'),
//     borderStyle: 'dashed',
//     marginHorizontal: wp('5%'),
//     bottom: hp('2%'),
//     paddingVertical: hp('2%'),
//     gap: hp('1%'),
//   },
//   DetailTransactionExpenseSub4Des: {
//     fontSize: hp('2%'),
//     color: '#91919f',
//   },
//   DetailTransactionExpenseSub4Con: {
//     fontSize: hp('1.8%'),
//   },
//   DetailTransactionExpenseSub4AttachHead: {
//     fontSize: hp('2%'),
//     color: '#91919f',
//   },
//   DetailTransactionExpenseSub4Image: {
//     width: '100%',
//     height: hp('20%'),
//     borderRadius: hp('1%'),
//   },
//   DetailTransactionExpenseSub5: {
//     position: 'absolute',
//     bottom: hp('2%'),
//     left: wp('5%'),
//     right: wp('5%'),
//     backgroundColor: '#7f3dff',
//     alignItems: 'center',
//     paddingVertical: hp('2%'),
//     borderRadius: hp('1%'),
//   },
//   DetailTransactionExpenseSub5Text: {
//     fontSize: hp('2%'),
//     fontWeight: '600',
//     color: '#fff',
//   },
//   //   for modal
//   modalContainer: {
//     flex: 1,
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//     backgroundColor: 'rgba(0, 0, 0, 0.5)',
//   },
//   modalContent: {
//     backgroundColor: '#ffffff',
//     width: '100%',
//     paddingVertical: hp('2%'),
//     paddingHorizontal: wp('5%'),
//     borderTopLeftRadius: hp('2%'),
//     borderTopRightRadius: hp('2%'),
//     gap:15,
//   },
//   modalDivider: {
//     width: '10%',
//     height: 5,
//     backgroundColor: '#d3bdff',
//     alignSelf: 'center',
//     marginBottom: hp('1%'),
//   },
//   modalTitle: {
//     fontSize: hp('2.2%'),
//     fontWeight: '600',
//     textAlign: 'center',
//     marginBottom: hp('1%'),
//   },
//   modalText: {
//     fontSize: hp('2%'),
//     textAlign: 'center',
//     marginBottom: hp('2%'),
//     color:'#91919f'
//   },
//   modalButtonContainer: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },
//   modalButtonNo: {
//     flex: 1,
//     paddingVertical: hp('2%'),
//     alignItems: 'center',
//     backgroundColor: '#eee5ff',
//     borderRadius: hp('2%'),
//     marginHorizontal: wp('1%'),
//   },
//   modalButtonYes: {
//     flex: 1,
//     paddingVertical: hp('2%'),
//     alignItems: 'center',
//     backgroundColor: '#7f3dff',
//     borderRadius: hp('2%'),
//     marginHorizontal: wp('1%'),
//   },
//   modalButtonTextNo: {
//     fontSize: hp('2%'),
//     fontWeight: '600',
//     color: '#7f3dff',
//   },
//   modalButtonTextYes: {
//     fontSize: hp('2%'),
//     fontWeight: '600',
//     color: '#ffffff',
//   },
// //   Alert
// overlay: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: 'rgba(0, 0, 0, 0.5)', 
// },
// customAlertContainer: {
//     position: 'absolute',
//     top: 0,
//     bottom: 0,
//     left: 0,
//     right: 0,
//     backgroundColor: 'rgba(0, 0, 0, 0.5)', 
//     justifyContent: 'center',
//     alignItems: 'center',
// },
// customAlert: {
//     backgroundColor: '#fff',
//     padding: wp('5%'), 
//     borderRadius: hp('2%'), 
//     alignItems: 'center',
// },
// alertIcon: {
//     fontSize: hp('7%'), 
//     color: '#5233ff',
//     marginBottom: hp('1%'), 
// },
// alertText: {
//     fontSize: hp('2%'), 
//     textAlign: 'center',
// },
// })
