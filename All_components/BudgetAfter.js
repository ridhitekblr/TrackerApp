import React, { useState, useEffect } from 'react';
import { Stylesheet, Text, View, TouchableOpacity, SafeAreaView, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation, useRoute } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { globalStyles } from './globalStyles';

const BudgetAfter = ({ route }) => {
    const navigation = useNavigation();
    const { category = '1', sliderValue1 = 0, amount = 0 } = route.params || {};

    const getCategoryInfo = (value) => {
        const data = [
            { label: 'Shopping', value: '1', color: '#fcac12' },
            { label: 'Transportation', value: '2', color: '#0077ff' },
        ];
        const selectedCategory = data.find(item => item.value === value);
        return selectedCategory ? selectedCategory : null;
    };

    const getCategoryLabel = (value) => {
        const data = [
            { label: 'Shopping', value: '1' },
            { label: 'Transportation', value: '2' },
        ];
        const selectedCategory = data.find(item => item.value === value);
        return selectedCategory ? selectedCategory.label : '';
    };

    const selectedCategoryInfo = getCategoryInfo(category);
    const [currentMonth, setCurrentMonth] = useState('May');
    const [sliderValue, setSliderValue] = useState(sliderValue1);

    useEffect(() => {
        if (route.params && route.params.sliderValue1 !== undefined) {
            setSliderValue(route.params.sliderValue1);
        }
    }, [route.params]);

    const goToPreviousMonth = () => {
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        const currentIndex = months.indexOf(currentMonth);
        const previousIndex = (currentIndex - 1 + months.length) % months.length;
        setCurrentMonth(months[previousIndex]);
    };
    
    const goToNextMonth = () => {
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        const currentIndex = months.indexOf(currentMonth);
        const nextIndex = (currentIndex + 1) % months.length;
        setCurrentMonth(months[nextIndex]);
    };

    const navigateToDetailBudget = () => {
        navigation.navigate('Detail_budget', { category, sliderValue1: sliderValue, amount });
    };

    const spentPercentage = (amount / 1200) * 100;
    const thumbColor = selectedCategoryInfo ? selectedCategoryInfo.color : '#7F3DFF';

    return (
        <SafeAreaView style={globalStyles.Aftercontainer}>
            <StatusBar backgroundColor="#7F3DFF" barStyle="light-content" />
            <View style={globalStyles.Afterheader}>
                <TouchableOpacity onPress={goToPreviousMonth}>
                    <Icon name='chevron-left' solid style={globalStyles.Aftericons}></Icon>
                </TouchableOpacity>
                <Text style={globalStyles.Aftermonth}>{currentMonth}</Text>
                <TouchableOpacity onPress={goToNextMonth}>
                    <Icon name='chevron-right' solid style={globalStyles.Aftericons}></Icon>
                </TouchableOpacity>
            </View>
            <View style={globalStyles.Afterbody}>
                <View style={globalStyles.Aftercategorycont}>
                    <View style={globalStyles.categoryBorder}>
                        {selectedCategoryInfo && (
                            <FontAwesomeIcon name="circle" size={10} color={selectedCategoryInfo.color} style={{ marginRight: 5 }} />
                        )}
                        <Text>{getCategoryLabel(category)}</Text>
                    </View>
                    <Text style={globalStyles.remainingamount}>Remaining ${Math.max(Math.round(sliderValue), 0)}</Text>
                    <View style={globalStyles.thumbBar}>
                        <View style={[globalStyles.thumbProgress, { backgroundColor: thumbColor, width: `${spentPercentage}%` }]}></View>
                    </View>
                    <Text style={globalStyles.remainingamountdoller}>${Math.round(sliderValue)} of ${amount}</Text>
                </View>
                <View style={globalStyles.Budgetbutton}>
                    <TouchableOpacity onPress={navigateToDetailBudget}>
                        <Text style={globalStyles.BudgetbuttonText}>Create a budget</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default BudgetAfter;

// const globalStyles = globalStylesheet.create({
//     Aftercontainer: {
//         flex: 1,
//         justifyContent: 'space-between',
//         backgroundColor: '#7F3DFF',
//         paddingTop: StatusBar.currentHeight,
//     },
//     Afterheader: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         marginTop: hp('4%'),
//         paddingHorizontal: wp('7%'),
//         alignItems: 'baseline',
//     },
//     Aftermonth: {
//         fontSize: hp('3%'),
//         color: '#ffffff',
//     },
//     Aftericons: {
//         fontSize: wp('6%'),
//         color: '#ffffff',
//     },
//     Afterbody: {
//         justifyContent: 'flex-end',
//         backgroundColor: '#fff',
//         paddingHorizontal: wp('5%'),
//         marginTop: hp('6%'),
//         borderTopLeftRadius: wp('10%'),
//         borderTopRightRadius: wp('10%'),
//     },
//     Budgetbutton: {
//         backgroundColor: '#7f3dff',
//         borderRadius: wp('2%'),
//         paddingVertical: hp('2%'),
//         alignItems: 'center',
//         marginBottom: hp('2%'),
//     },
//     BudgetbuttonText: {
//         fontWeight: 'bold',
//         color: '#fff',
//         fontSize: wp('5%'),
//     },
//     Aftercategorycont: {
//         marginVertical: hp('4%'),
//     },
//     categoryBorder: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         justifyContent: 'space-evenly',
//         borderWidth: 1,
//         width: wp('35%'),
//         borderColor: 'lightgray',
//         textAlign: 'center',
//         paddingHorizontal: wp('2%'),
//         paddingVertical: hp('.5%'),
//         borderRadius: 20,
//         marginBottom: hp('1%'),
//     },
//     remainingamount: {
//         fontSize: wp('7%'),
//         fontWeight: 'bold',
//     },
//     remainingamountdoller: {
//         fontSize: wp('4%'),
//     },
//     thumbBar: {
//         height: 10,
//         backgroundColor: 'lightgray',
//         borderRadius: 5,
//         marginVertical: hp('2%'),
//     },
//     thumbProgress: {
//         height: '100%',
//         borderRadius: 5,
//     },
// });
