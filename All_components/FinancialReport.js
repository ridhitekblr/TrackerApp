import React from 'react';
import { globalStylesheet, Text, View, TouchableOpacity, Image,StatusBar, SafeAreaView } from 'react-native';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/FontAwesome';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';



const FinancialReport = ({ navigation }) => {
    return (
        <SafeAreaView style={globalStyles.financialreportmaincontainer}>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent={true} />
        <Swiper
            style={globalStyles.wrapper}
            showsButtons={false}
            dot={<View style={globalStyles.dotStyle} />}
            activeDot={<View style={globalStyles.activeDotStyle} />}
            paginationStyle={globalStyles.paginationStyle}
        >
            
            <View style={globalStyles.slide1}>
                <View style={globalStyles.page1_maintxt1}>
                    <Text style={globalStyles.txt1}>This Month</Text>
                </View>
                <View style={globalStyles.page1_middlecontainer}>
                    <Text style={globalStyles.page1_txt2}>   You Spend 💸</Text>
                    <Text style={globalStyles.page1_txt3}>$332</Text>
                </View>
                <View style={globalStyles.page1_footer}>
                    <View style={globalStyles.page1_ftr}>
                        <Text style={globalStyles.page1_footertxt1}>and your biggest spending is from</Text>
                        <View style={globalStyles.page1_shopping}>
                            <TouchableOpacity style={globalStyles.page1_shopping1}>
                                <Icon name="shopping-basket" style={globalStyles.page1_topicon} />
                                <Text style={globalStyles.page1_txt4}>Shopping</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={globalStyles.page1_footertxt2}>$120</Text>
                    </View>
                </View>
            </View>
            <View style={globalStyles.slide2}>
                <View style={globalStyles.page1_maintxt1}>
                    <Text style={globalStyles.txt1}>This Month</Text>
                </View>
                <View style={globalStyles.page1_middlecontainer}>
                    <Text style={globalStyles.page1_txt2}>You Earned 💰</Text>
                    <Text style={globalStyles.page1_txt3}>$6000</Text>
                </View>
                <View style={globalStyles.page1_footer}>
                    <View style={globalStyles.page1_ftr}>
                        <Text style={globalStyles.page1_footertxt1}>your biggest Income is from</Text>
                        <View style={globalStyles.page1_shopping}>
                            <TouchableOpacity style={globalStyles.page1_shopping1}>
                                <Icon name="money" style={globalStyles.page1_topicon} />
                                <Text style={globalStyles.page1_txt4}>Salary</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={globalStyles.page1_footertxt2}>$5000</Text>
                    </View>
                </View>
            </View>
            <View style={globalStyles.slide3}>
                <View style={globalStyles.page3_maintxt1}>
                    <Text style={globalStyles.txt1}>This Month</Text>
                </View>
                <View style={globalStyles.page3_container1}>
                    <Text style={globalStyles.page3_container1txt}>2 of 12 Budget is</Text>
                    <Text style={globalStyles.page3_container1txt1} > exceeds the limit</Text>
                    <View style={globalStyles.page3buttons}>
                        <View style={globalStyles.page3_shopping}>
                            <TouchableOpacity style={globalStyles.page3_shopping1}>
                                <Icon name="shopping-basket" style={globalStyles.page3_topicon1} />
                                <Text style={globalStyles.page1_txt4}>Shopping</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={globalStyles.page3_shopping2}>
                            <TouchableOpacity style={globalStyles.page3_shopping1}>
                                {/* <Icon name="spoon" style={globalStyles.page3_topicon2} /> */}
                                <Image source={require('../assets/icon.png')} style={globalStyles.page3_topicon2} />
                                <Text style={globalStyles.page1_txt4}>Food</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View></View>
            </View>
            <View style={globalStyles.slide4}>
                <View style={globalStyles.page4_maintxt}>
                    <Text style={globalStyles.page4_txt1}>“Financial freedom is freedom from fear.”</Text>
                    <Text style={globalStyles.page4_txt2}>-Robert Kiyosaki</Text>
                </View>
                <View style={globalStyles.buttonmain}>
                    <TouchableOpacity style={globalStyles.page4_button} onPress={() => navigation.navigate('FinancialReportLine')}>
                        <Text style={globalStyles.page4_buttontxt}  >See the full detail</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Swiper>
        </SafeAreaView>
    );
};
export default FinancialReport;

// const globalStyles = globalStylesheet.create({
//     financialreportmaincontainer:{
//         flex:1,
//         paddingTop: StatusBar.currentHeight,
//     },
//     paginationStyle: {
//         bottom: hp('94%'),
//         gap: 10
//     },
//     dotStyle: {
//         width: wp(20),
//         height: wp(1),
//         borderRadius: 5,
//         backgroundColor: 'rgba(255,255,255,.3)',
//     },
//     activeDotStyle: {
//         width: wp(20),
//         height: wp(1),
//         borderRadius: 5,
//         backgroundColor: '#fff',
//     },
//     slide1: {
//         flex: 1,
//         justifyContent: 'space-evenly',
//         alignItems: 'center',
//         backgroundColor: 'rgb(253,60,74)',
//     },
//     txt1: {
//         color: 'white',
//         fontSize: wp('5%'),
//         alignItems: 'center',
//         fontWeight: "600",
//     },
//     page1_txt2: {
//         color: 'white',
//         fontSize: wp('8%'),
//         alignItems: 'center',
//         fontWeight: "700"
//     },
//     page1_txt3: {
//         color: 'white',
//         fontSize: wp('10%'),
//         fontWeight: "700",
//         textAlign: 'center',
//     },
//     page1_footer: {
//         width: wp('80%'),
//         alignItems: 'center',
//         backgroundColor: 'white',
//         borderRadius: 30,
//         padding: wp('5%')
//     },
//     page1_ftr: {
//         gap: 20,
//     },
//     page1_footertxt1: {
//         fontSize: wp('7%'),
//         textAlign: 'center',
//         fontWeight: "700",
//     },
//     page1_shopping: {
//         width: wp('40%'),
//         marginLeft: 'auto',
//         marginRight: 'auto',
//         paddingVertical: hp('2%'),
//         borderWidth: 1,
//         borderColor: 'lightgray',
//         justifyContent: 'center',
//         borderRadius: 20,
//     },
//     page1_shopping1: {
//         flexDirection: 'row',
//         gap: 15,
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
//     page1_topicon: {
//         borderRadius: 8,
//         backgroundColor: '#FCEED4',
//         color: 'gold',
//         fontSize: wp(4),
//         padding: 4
//     },
//     page1_footertxt2: {
//         fontSize: wp('8%'),
//         fontWeight: '700',
//         textAlign: 'center',
//     },
//     slide2: {
//         flex: 1,
//         justifyContent: 'space-evenly',
//         alignItems: 'center',
//         backgroundColor: 'rgb(0,168,107)',
//     },
//     slide3: {
//         flex: 1,
//         justifyContent: 'space-evenly',
//         alignItems: 'center',
//         backgroundColor: '#rgb(127,61,255)',
//     },
//     page3_shopping: {
//         width: wp('40%'),
//         padding: hp('2%'),
//         borderColor: 'gray',
//         alignItems: 'center',
//         justifyContent: 'center',
//         borderRadius: 20,
//         backgroundColor: '#FCFCFC',
//         flexDirection: 'row'
//     },
//     page3_shopping2: {
//         width: wp('40%'),
//         padding: hp('2%'),
//         borderWidth: 0.5,
//         borderColor: 'gray',
//         alignItems: 'center',
//         justifyContent: 'center',
//         borderRadius: 20,
//         backgroundColor: '#FCFCFC',
//         flexDirection: 'row'
//     },
//     page3_shopping1: {
//         flexDirection: 'row',
//         gap: 15,
//     },
//     page3_maintxt1: {
//         marginTop: hp('-8%')
//     },
//     page3_container1txt: {
//         textAlign: 'center',
//         color: 'white',
//         fontSize: wp('7%'),
//         fontWeight: 'bold',
//         letterSpacing: 0.6
//     },
//     page3_container1txt1: {
//         textAlign: 'center',
//         color: 'white',
//         fontSize: wp('7%'),
//         fontWeight: 'bold',
//         letterSpacing: 1
//     },
//     page3buttons: {
//         display: 'flex',
//         flexDirection: 'row',
//         marginTop: hp('3%'),
//         gap: 10,
//     },
//     page3_topicon1: {
//         borderRadius: 8,
//         backgroundColor: '#FCEED4',
//         color: 'gold',
//         width: wp(6.5),
//         height: hp(3),
//         fontSize: wp(4),
//         alignItems: 'center',
//         justifyContent: 'center',
//         padding: 3.5

//     },
//     page3_topicon2: {
//         borderRadius: 8,
//         backgroundColor: '#FDD5D7',
//         color: 'gold',
//         width: wp(6.5),
//         height: hp(3),
//         fontSize: wp(2),
//         alignItems: 'center',
//         justifyContent: 'center',
//         padding: 3.5
//     },
//     slide4: {
//         flex: 1,
//         backgroundColor: '#rgb(127,61,255)',
//     },
//     page4_maintxt: {
//         flex: 1,
//         justifyContent: 'center',
//         paddingHorizontal: wp('10%')
//     },
//     page4_txt1: {
//         fontSize: wp('8%'),
//         fontWeight: 'bold',
//         color: 'white',
//     },
//     page4_txt2: {
//         fontSize: wp('5%'),
//         fontWeight: 'bold',
//         color: 'white',
//         paddingTop: hp('1%')
//     },
//     buttonmain: {
//         flex: 1,
//         justifyContent: 'flex-end',
//     },
//     page4_button: {
//         backgroundColor: '#fff',
//         borderRadius: wp('2%'),
//         paddingVertical: hp('2%'),
//         marginHorizontal: wp('5%'),
//         alignItems: 'center',
//         marginBottom: hp('3%')

//     },
//     page4_buttontxt: {
//         fontWeight: 'bold',
//         color: '#rgb(127,61,255)',
//         fontSize: wp('5%')
//     }
// });