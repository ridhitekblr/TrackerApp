import React, { useState } from 'react';
import { View, Text, TouchableOpacity, ScrollView, FlatList, Image, globalStylesheet,StatusBar} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { globalStyles } from './globalStyles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


const AppTABHome= ({navigation}) => {
  const [selectedPeriod, setSelectedPeriod] = useState('today');
  const [data, setData] = useState([
    { id: '1', label: 'Shopping', description:'Buy some grocery', money:'- $120', icon: <MaterialCommunityIcons name='shopping' style={globalStyles.shopping}></MaterialCommunityIcons>,time:'10:00 AM'},
    { id: '2', label: 'Subscription', description:'Disney+ Annual..', money:'- $80', icon: <MaterialCommunityIcons name='note' style={globalStyles.note}></MaterialCommunityIcons>,time:'03:30 PM' },
    { id: '3', label: 'Food', description:'Buy a ramen', money:'- $32', icon: <MaterialCommunityIcons name='food' style={globalStyles.food}></MaterialCommunityIcons>,time:'07:30 PM'},

  ]);

  const renderItem = ({ item }) => (
    <View style={{ paddingVertical: 10, paddingHorizontal: 30 }}>
    <View style={globalStyles.listTransaction}>
      <View style={globalStyles.iconwithlabdes}>
      <View style={globalStyles.TransactionIconView}>
      <View style={globalStyles.TransactionIcon}>{item.icon}</View>
      </View>
      <View style={globalStyles.TransactionLabDes}>
      <Text style={globalStyles.TransactionLable}>{item.label}</Text>
      <Text style={globalStyles.TransactionDescription}>{item.description}</Text>
      </View>
      </View>
      <View style={globalStyles.TransactionMoneyTime}>
      <Text style={globalStyles.TransactionMoney}>{item.money}</Text>
      <Text style={globalStyles.TransactionTime}>{item.time}</Text> 
      </View>
      
      </View>
    </View>
  );

  const handlePeriodChange = (period) => {
    let updatedData = [];
    switch (period) {
      case 'today':
        updatedData = [
            { id: '1', label: 'Shopping', description:'Buy some grocery', money:'- $120', icon: <MaterialCommunityIcons name='shopping' style={globalStyles.shopping}></MaterialCommunityIcons>,time:'10:00 AM'},
            { id: '2', label: 'Subscription', description:'Disney+ Annual..', money:'- $80', icon: <MaterialCommunityIcons name='note' style={globalStyles.note}></MaterialCommunityIcons>,time:'03:30 PM' },
            { id: '3', label: 'Food', description:'Buy a ramen', money:'- $32', icon: <MaterialCommunityIcons name='food' style={globalStyles.food}></MaterialCommunityIcons>,time:'07:30 PM'},
        ];
        break;
      case 'week':
        updatedData = [
            
            { id: '1', label: 'Shopping', description:'Buy some grocery', money:'- $120', icon: <MaterialCommunityIcons name='shopping' style={globalStyles.shopping}></MaterialCommunityIcons>,time:'10:00 AM'},
            { id: '2', label: 'Food', description:'Buy a ramen', money:'- $32', icon: <MaterialCommunityIcons name='food' style={globalStyles.food}></MaterialCommunityIcons>,time:'07:30 PM'},
            { id: '3', label: 'Subscription', description:'Disney+ Annual..', money:'- $80', icon: <MaterialCommunityIcons name='note' style={globalStyles.note}></MaterialCommunityIcons>,time:'03:30 PM' },
            
        ];
        break;
      case 'month':
        updatedData = [
            
            { id: '1', label: 'Food', description:'Buy a ramen', money:'- $32', icon: <MaterialCommunityIcons name='food' style={globalStyles.food}></MaterialCommunityIcons>,time:'07:30 PM'},
            { id: '2', label: 'Shopping', description:'Buy some grocery', money:'- $120', icon: <MaterialCommunityIcons name='shopping' style={globalStyles.shopping}></MaterialCommunityIcons>,time:'10:00 AM'},
            { id: '3', label: 'Subscription', description:'Disney+ Annual..', money:'- $80', icon: <MaterialCommunityIcons name='note' style={globalStyles.note}></MaterialCommunityIcons>,time:'03:30 PM' },
            
        ];
        break;
      case 'year':
        updatedData = [
            { id: '1', label: 'Shopping', description:'Buy some grocery', money:'- $120', icon: <MaterialCommunityIcons name='shopping' style={globalStyles.shopping}></MaterialCommunityIcons>,time:'10:00 AM'},
            { id: '2', label: 'Subscription', description:'Disney+ Annual..', money:'- $80', icon: <MaterialCommunityIcons name='note' style={globalStyles.note}></MaterialCommunityIcons>,time:'03:30 PM' },
            { id: '3', label: 'Food', description:'Buy a ramen', money:'- $32', icon: <MaterialCommunityIcons name='food' style={globalStyles.food}></MaterialCommunityIcons>,time:'07:30 PM'},
        ];
        break;
      default:
        updatedData = [];
    }
    setData(updatedData);
    setSelectedPeriod(period);
  };

  return (
    <View style={globalStyles.containertabhome}>
    <View style={globalStyles.ThreeSectionsJoin}>
      {/* Top Section */}
      <View style={globalStyles.topSection}>
        {/* Profile Image */}
        <Image source={{ uri: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8fDA%3D' }} style={globalStyles.profileImage} />
        {/* Text */}
        <Text style={globalStyles.textDrop}><MaterialCommunityIcons name='arrow-down' style={{fontSize:20,color:'#7f3dff'}}></MaterialCommunityIcons> October</Text>
        {/* Notification Icon */}
        <TouchableOpacity onPress={() => navigation.navigate('TrackerNotification')}>
          {/* Your notification icon here */}
          <MaterialCommunityIcons name='bell' style={globalStyles.userBell}></MaterialCommunityIcons>
        </TouchableOpacity>
      </View>

      {/* Second Section */}
      <View style={globalStyles.secondSection}>
        <Text style={globalStyles.balanceText}>Account Balance</Text>
        <Text style={globalStyles.balanceAmount}>$9400</Text>
      </View>
      
       {/* Second Sub Section */}
       <View style={globalStyles.incomeexpenses}>
       <View style={globalStyles.incomeCard}>
       <MaterialCommunityIcons name='arrow-down' style={globalStyles.incomeIcon}></MaterialCommunityIcons> 
       <View style={globalStyles.incomeCardDetails}>
        <Text style={globalStyles.incomeCardIncome}>Income</Text>
        <Text style={globalStyles.incomeCardMoney}>$5000</Text>
       </View>
       </View>
       <View style={globalStyles.expensesCard}>
       <MaterialCommunityIcons name='arrow-up' style={globalStyles.expenseIcon} ></MaterialCommunityIcons> 
       <View style={globalStyles.expensesCardDetails}>
       <Text style={globalStyles.expensesCardExpense}>Expenses</Text>
       <Text style={globalStyles.expensesCardMoney}>$1200</Text>
       </View>
       </View>
     </View>
     </View>

      {/* Third Section */}
      <ScrollView>
      <View style={globalStyles.thirdSection}>
        <Text style={globalStyles.spendFrequencyText}>Spend Frequency</Text>
        {/* Your graph component here */}
        <View style={{ width: '100%', height: 200, backgroundColor: 'lightgray', marginTop: 10 }} >
        <Image source={require('../assets/UserDashBoardChart.jpg')} style={{ width:"100%", height: 200 }}/>
        </View>
      </View>
      

      {/* Fourth Section (FlatList) */}
      <View style={globalStyles.fourthSection}>
        <TouchableOpacity onPress={() => handlePeriodChange('today')}>
          <Text style={[globalStyles.periodButton, { color: selectedPeriod === 'today' ? '#fcac12' : '#a7a7b2',backgroundColor: selectedPeriod === 'today' ? '#fceed4' : '#ffffff' }]}>Today</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handlePeriodChange('week')}>
          <Text style={[globalStyles.periodButton, { color: selectedPeriod === 'week' ? '#fcac12' : '#a7a7b2',backgroundColor: selectedPeriod === 'week' ? '#fceed4' : '#ffffff' }]}>Week</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handlePeriodChange('month')}>
          <Text style={[globalStyles.periodButton, { color: selectedPeriod === 'month' ? '#fcac12' : '#a7a7b2',backgroundColor: selectedPeriod === 'month' ? '#fceed4' : '#ffffff' }]}>Month</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handlePeriodChange('year')}>
          <Text style={[globalStyles.periodButton, { color: selectedPeriod === 'year' ? '#fcac12' : '#a7a7b2',backgroundColor: selectedPeriod === 'year' ? '#fceed4' : '#ffffff'  }]}>Year</Text>
        </TouchableOpacity>
      </View>

    {/* Fifth Section (See All Transaction) */}
    <View style={globalStyles.RecentTransaction}>
    <Text style={globalStyles.RecentTransactionText}>Recent Transaction</Text>
    <Text style={globalStyles.RecentTransactionSeeAll}>See All</Text>
    </View>

      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        scrollEnabled={false}
      />
      </ScrollView>
    </View>
  );
};

export default AppTABHome;

// const globalStyles = globalStylesheet.create({
//   containertabhome: {
//     flex: 1,
//     backgroundColor:'#ffffff'
//   },
//   ThreeSectionsJoin:{
//     backgroundColor:'#fdf7eb',
//     borderBottomRightRadius: wp(10),
//     borderBottomLeftRadius: wp(10),
//   },
//   topSection: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//     paddingHorizontal: wp(5),
//     paddingTop: hp(3),
//   },
//   profileImage: {
//     width: wp(12),
//     height: wp(12),
//     borderRadius: wp(6),
//     borderWidth: wp(0.5),
//     borderColor:'#ad00ff'
//   },
//   textDrop: {
//     fontSize: wp(5),
//     marginRight: wp(7.5),
//     backgroundColor: '#fdf7ea',
//     borderWidth: wp(0.25),
//     paddingVertical: hp(1.25),
//     paddingHorizontal: wp(5),
//     borderColor:'#f1f1fa',
//     borderRadius: wp(5),
//   },
//   userBell:{
//     fontSize: wp(6.25),
//     color:'#7f3dff'
//   },
//   secondSection: {
//     paddingHorizontal: wp(5),
//     paddingTop: hp(1.25),
//     alignItems:'center',
//     gap: hp(0.875),
//   },
//   incomeexpenses:{
//     display:'flex',
//     flexDirection:'row',
//     justifyContent:'center',
//     gap: wp(2.5),
//     width: '100%',
//     paddingHorizontal: wp(7.5),
//     paddingVertical: hp(2.5),
//   },
//   incomeIcon:{
//     fontSize: wp(10),
//     color:'#00a86b',
//     backgroundColor:'#ffffff',
//     borderRadius: wp(5),
//     padding: wp(1.25),
//   },
//   expenseIcon:{
//     fontSize: wp(10),
//     color:'#fd3c4a',
//     backgroundColor:'#ffffff',
//     borderRadius: wp(5),
//     padding: wp(1.25),
//   },
//   incomeCard:{
//     width: '50%',
//     backgroundColor:'#00a86b',
//     alignItems:'center',
//     justifyContent:'center',
//     flexDirection:'row',
//     gap: wp(2.5),
//     borderRadius: wp(7.5),
//   },
//   expensesCard:{
//     width: '50%',
//     backgroundColor:'#fd3c4a',
//     alignItems:'center',
//     justifyContent:'center',
//     flexDirection:'row',
//     gap: wp(2.5),
//     padding: wp(3.25),
//     borderRadius: wp(7.5),
//   },
//   incomeCardIncome:{
//     fontSize: wp(3.75),
//     fontWeight:'500',
//     color:'#ffffff'
//   },
//   incomeCardMoney:{
//     fontSize: wp(6.25),
//     fontWeight:'500',
//     color:'#ffffff'
//   },
//   expensesCardExpense:{
//     fontSize: wp(3.75),
//     fontWeight:'500',
//     color:'#ffffff'
//   },
//   expensesCardMoney:{
//     fontSize: wp(6.25),
//     fontWeight:'500',
//     color:'#ffffff'
//   },
//   balanceText: {
//     fontSize: wp(4.5),
//     color:'#91919f',
//     fontWeight:'500'
//   },
//   balanceAmount: {
//     fontSize: wp(10),
//     fontWeight:'700'
//   },
//   thirdSection: {
//     paddingHorizontal: wp(0),
//     paddingTop: hp(2.5),
//   },
//   spendFrequencyText: {
//     fontSize: wp(5),
//     fontWeight:'500',
//     paddingHorizontal: wp(5),
//   },
//   fourthSection: {
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//     marginTop: hp(2.5),
//   },
//   periodButton: {
//     fontSize: wp(4),
//     paddingHorizontal: wp(3.75),
//     paddingVertical: hp(0.625),
//     borderRadius: wp(5),
//   },
//   RecentTransaction:{
//     flexDirection:'row',
//     justifyContent:'space-between',
//     paddingHorizontal: wp(7.5),
//     paddingVertical: hp(2.5),
//   },
//   RecentTransactionText:{
//     fontSize: wp(5),
//     fontWeight:'500',
//   },
//   RecentTransactionSeeAll:{
//     fontSize: wp(4),
//     backgroundColor: '#eee5ff',
//     paddingHorizontal: wp(3.75),
//     paddingVertical: hp(0.625),
//     borderRadius: wp(5),
//     color:'#7f3dff'
//   },
//   listTransaction:{
//     backgroundColor:'#fcfcfa',
//     flexDirection:'row',
//     justifyContent:'space-between',
//     paddingHorizontal: wp(5),
//     paddingVertical: hp(1.825),
//     borderRadius: wp(7.5),
//   },
//   iconwithlabdes:{
//     flexDirection:'row',
//     gap: wp(2.5),
//   },
//   TransactionIcon:{
//     color:'#00a86b',
//     backgroundColor:'#ffffff',
//     borderRadius: wp(5),
//     padding: wp(1.25),
//     borderColor:'#f1f1fa',
//     borderWidth: wp(0.25),
//   },
//   shopping:{
//     fontSize: wp(8),
//     color:'#fcac12',
//   },
//   note:{
//     fontSize: wp(8),
//     color:'#7f3dff',
//   },
//   food:{
//     fontSize: wp(8),
//     color:'#fd3c4a',
//   },
//   TransactionLabDes:{
//     gap: hp(1),
//   },
//   TransactionLable:{
//     fontSize: wp(4),
//     color:'#000000'
//   },
//   TransactionDescription:{
//     color:'#91919f',
//     fontSize: wp(3),
//   },
//   TransactionMoneyTime:{
//     gap: hp(1),
//   },
//   TransactionMoney:{
//     fontSize: wp(4),
//     color:'#fd3c4a'
//   },
//   TransactionTime:{
//     color:'#91919f',
//     fontSize: wp(3),
//   },



//   });