import React, { useState } from 'react';
import { View, Text, Modal, Button, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const TransactionFilterModal = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedFilters, setSelectedFilters] = useState({
    filterBy: [],
    sortBy: [],
  });
  const [resetClicked, setResetClicked] = useState(false);

  const toggleFilter = (category, option) => {
    setSelectedFilters((prev) => {
      const isSelected = prev[category].includes(option);
      if (isSelected) {
        return {
          ...prev,
          [category]: prev[category].filter((item) => item !== option),
        };
      } else {
        return {
          ...prev,
          [category]: [...prev[category], option],
        };
      }
    });
  };

  const isFilterSelected = (category, option) => selectedFilters[category].includes(option);

  const resetFilters = () => {
    setSelectedFilters({ filterBy: [], sortBy: [] });
    setResetClicked(true);
    setTimeout(() => setResetClicked(false), 5000); 
  };

  return (
    <View style={styles.container}>
      <Button title="Show Modal" onPress={() => setModalVisible(true)} />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(!modalVisible)}
      >
        <View style={styles.modalOverlay}>
          <View style={styles.modalView}>
            <View style={styles.modalBarLine} />
            <View style={styles.header}>
              <Text style={styles.headerText}>Filter Transaction</Text>
              <Text
                style={[
                  styles.resetText,
                  resetClicked && styles.resetClickedText,
                ]}
                onPress={resetFilters}
              >
                Reset
              </Text>
            </View>
            <View style={styles.filterSection}>
              <Text style={styles.sectionTitle}>Filter By</Text>
              <View style={styles.filterOptions}>
                {['Income', 'Expense', 'Transfer'].map((option) => (
                  <TouchableOpacity
                    key={option}
                    style={[
                      styles.filterOption,
                      isFilterSelected('filterBy', option) && styles.selectedOption,
                    ]}
                    onPress={() => toggleFilter('filterBy', option)}
                  >
                    <Text
                      style={[
                        styles.filterOptionText,
                        isFilterSelected('filterBy', option) && styles.selectedOptionText,
                      ]}
                    >
                      {option}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
            </View>
            <View style={styles.filterSection}>
              <Text style={styles.sectionTitle}>Sort By</Text>
              <View style={styles.filterOptions}>
                {['Highest', 'Lowest', 'Newest', 'Oldest'].map((option) => (
                  <TouchableOpacity
                    key={option}
                    style={[
                      styles.filterOption,
                      isFilterSelected('sortBy', option) && styles.selectedOption,
                    ]}
                    onPress={() => toggleFilter('sortBy', option)}
                  >
                    <Text
                      style={[
                        styles.filterOptionText,
                        isFilterSelected('sortBy', option) && styles.selectedOptionText,
                      ]}
                    >
                      {option}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
            </View>
            <View style={styles.filterSection}>
              <Text style={styles.sectionTitle}>Category</Text>
              <View style={styles.categorySelection}>
                <Text style={styles.chooseCategory}>Choose Category</Text>
                <View style={styles.selectedCategory}>
                  <Text>Selected</Text>
                  <Icon name="angle-right" size={26} color="blue" />
                </View>
              </View>
            </View>
            <TouchableOpacity style={styles.applyButton} onPress={() => {/* Add apply functionality */}}>
              <Text style={styles.applyButtonText}>Apply</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalView: {
    width: '100%',
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    paddingBottom: 20,
  },
  modalBarLine: {
    height: 4,
    width: '10%',
    backgroundColor: '#d3bdff',
    marginTop: 10,
    marginBottom: 20,
    alignSelf: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
  },
  resetText: {
    borderWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 50,
    fontSize: 16,
    fontWeight: '400',
    borderColor: '#e3e3e5',
    color: 'black',
  },
  resetClickedText: {
    color: '#7f3dff',
    borderColor: '#7f3dff',
    backgroundColor:'#eee5ff'
  },
  filterSection: {
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: '500',
    marginBottom: 10,
  },
  filterOptions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  filterOption: {
    borderWidth: 1,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 50,
    fontSize: 16,
    fontWeight: '400',
    borderColor: '#e3e3e5',
    marginVertical: 5,
  },
  filterOptionText: {
    color: 'black',
  },
  selectedOption: {
    backgroundColor: '#eee5ff',
    borderColor: '#7f3dff',
  },
  selectedOptionText: {
    color: '#7f3dff',
  },
  categorySelection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  chooseCategory: {
    fontSize: 16,
  },
  selectedCategory: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
  },
  applyButton: {
    alignItems: 'center',
    backgroundColor: '#7f3dff',
    marginHorizontal: 20,
    paddingVertical: 15,
    borderRadius: 10,
    marginTop: 20,
  },
  applyButtonText: {
    fontSize: 20,
    color: '#fff',
    fontWeight: '600',
  },
});

export default TransactionFilterModal;
