import React from 'react';
import { globalStylesheet, Text, View, SafeAreaView, StatusBar, Image, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { globalStyles } from './globalStyles';

const transactionsToday = [
    {
        id: '1',
        category: 'Shopping',
        description: 'Buy some grocery',
        amount: '- $120',
        time: '10:00 AM',
        image: require('../assets/paypal.png')
    },
    {
        id: '2',
        category: 'Subscription',
        description: 'Disney+ Annual..',
        amount: '- $80',
        time: '03:30 PM',
        image: require('../assets/paypal.png')
    },
    {
        id: '3',
        category: 'Food',
        description: 'Buy a ramen',
        amount: '- $32',
        time: '07:30 PM',
        image: require('../assets/paypal.png')
    },
];
const transactionsyesterday = [
    {
        id: '1',
        category: 'Shopping',
        description: 'Charging Tesla',
        amount: '- $18',
        time: '02:30 PM',
        image: require('../assets/paypal.png')
    }
];

const Detailaccount = () => {
    const renderTransaction = ({ item }) => (
        <View style={globalStyles.detaildaysconttransfer}>
            <View style={globalStyles.detaildaysconttransfersubcont}>
                <Image source={item.image} />
                <View style={globalStyles.detaildaysconttransfersubmain}>
                    <Text style={globalStyles.detaildaysconttransferhead}>{item.category}</Text>
                    <Text style={globalStyles.detaildaysconttransfersub}>{item.description}</Text>
                </View>
            </View>
            <View>
                <Text style={globalStyles.detaildaysconttransferamount}>{item.amount}</Text>
                <Text style={globalStyles.detaildaysconttransfersub}>{item.time}</Text>
            </View>
        </View>
    );

    return (
        <SafeAreaView style={globalStyles.detailaccountcontainer}>
            <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
            <View style={globalStyles.detailaccountcontent}>
                <MaterialCommunityIcons name="arrow-left-thin" size={35} color="black" style={globalStyles.detailaccountIcon} />
                <Text style={globalStyles.detailaccountheading}>Detail Account</Text>
                <MaterialCommunityIcons name="pencil" size={25} color="black" style={globalStyles.editIcon} />
            </View>
            <View style={globalStyles.detailaccountcont}>
                <Image source={require('../assets/paypal.png')} />
                <Text style={globalStyles.detailaccountText}>Account Balance</Text>
                <Text style={globalStyles.detailbalanceText}>$9400</Text>
            </View>
            <View style={globalStyles.detaildayscontainer}>
                <Text style={globalStyles.detaildayscontheading}>Today</Text>
                <FlatList
                    data={transactionsToday}
                    renderItem={renderTransaction}
                    keyExtractor={item => item.id}
                />
            </View>
            <View style={globalStyles.detaildayscontainer}>
                <Text style={globalStyles.detaildayscontheading}>Yesterday</Text>
                <FlatList
                    data={transactionsyesterday}
                    renderItem={renderTransaction}
                    keyExtractor={item => item.id}
                />
            </View>
        </SafeAreaView>
    );
};

export default Detailaccount;

// const globalStyles = globalStylesheet.create({
//     detailaccountcontainer: {
//         flex: 1,
//         paddingTop: StatusBar.currentHeight,
//     },
//     detailaccountcontent: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         alignItems: 'center',
//         paddingHorizontal: wp('5%'),
//         paddingVertical: hp('2%'),
//     },
//     detailaccountheading: {
//         fontSize: wp('6%'),
//         fontWeight: 'bold',
//     },
//     detailaccountcont: {
//         alignItems: 'center',
//         paddingVertical: hp('3%')
//     },
//     detailaccountText: {
//         fontSize: wp('4%'),
//         color: 'gray',
//         paddingBottom: hp('1%')
//     },
//     detailbalanceText: {
//         fontSize: wp('12%'),
//         fontWeight: 'bold',
//     },
//     detaildayscontainer: {
//         paddingHorizontal: wp('5%'),
//     },
//     detaildayscontheading: {
//         fontWeight: '700',
//         fontSize: wp('5%')
//     },
//     detaildaysconttransfer: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         alignItems: 'center',
//         marginVertical: hp('3%')
//     },
//     detaildaysconttransfersubcont: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         alignItems: 'center',
//     },
//     detaildaysconttransfersub: {
//         fontSize: wp('4%'),
//         fontWeight: "500",
//         color: 'gray',
//     },
//     detaildaysconttransferhead: {
//         fontSize: wp('5%'),
//         paddingBottom: hp('1%')
//     },
//     detaildaysconttransferamount: {
//         fontWeight: '700',
//         fontSize: wp('5%'),
//         color: 'red',
//         paddingBottom: hp('1%')
//     },
//     detaildaysconttransfersubmain: {
//         marginLeft: wp('4%')
//     }
// });