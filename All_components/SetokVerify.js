import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, BackHandler, Alert,TouchableOpacity } from 'react-native';
import { globalStyles } from './globalStyles';




const SetokVerify = ({navigation}) => {
   
  useEffect(() => {
    const backAction = () => {
      Alert.alert(
        'Hold on!',
        'Are you sure you want to go back?',
        [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          { text: 'YES', onPress: () => navigation.navigate('SignupCarousel') },
        ],
        { cancelable: false }
      );
      return true;
    };

    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

    return () => backHandler.remove();
  }, [navigation]);

  return (
    <View style={globalStyles.Setcontainer}>
      <View>
        <Image source={require('../assets/success.png')} style={globalStyles.setimage} />
      </View>
      <Text style={globalStyles.set1}>Verification Successful!</Text>
      <TouchableOpacity onPress={() => navigation.navigate('Login')} style={globalStyles.gotoLogin}>
      <Text style={globalStyles.gotoLoginText}>Go To Login</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({});

export default SetokVerify;
