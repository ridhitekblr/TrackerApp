import React from 'react';
import { View, Text, globalStylesheet, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

const AboutPage = ({navigation}) => {
  return (
    <SafeAreaView style={globalStyles.containerAbout}>
      <ScrollView contentContainerStyle={globalStyles.scrollContainer}>
        <Text style={globalStyles.headerAbout}>About</Text>
        <View style={globalStyles.sectionAbout}>
          <Text style={globalStyles.sectionTitleAbout}>Purpose</Text>
          <Text style={globalStyles.sectionTextAbout}>
            Our tracker app is designed to help you keep track of various activities, tasks, or goals in your life.
            Whether it's tracking your daily exercise routine, monitoring your spending habits, or managing your to-do list,
          </Text>
          <Text style={globalStyles.sectionTextAbout}>
            Our app provides you with the tools you need to stay organized and focused.
          </Text>
        </View>
        <View style={globalStyles.sectionAbout}>
          <Text style={globalStyles.sectionTitleAbout}>Features</Text>
          <Text style={globalStyles.sectionTextAbout}>
            - Track various activities, tasks, or goals
            {"\n"}- Set reminders and notifications
            {"\n"}- View detailed statistics and reports
            {"\n"}- Customize categories and labels
            {"\n"}- Sync data across multiple devices
          </Text>
        </View>
        <View style={globalStyles.sectionAbout}>
          <Text style={globalStyles.sectionTitleAbout}>Contact Us</Text>
          <Text style={globalStyles.sectionTextAbout}>
            If you have any questions, suggestions, or feedback, feel free to contact us at tracker@example.com.
            We're always here to help!
          </Text>
          <Text style={globalStyles.sectionTextAbout}>
            We're always here to help!
          </Text>
        </View>
        <TouchableOpacity style={globalStyles.aboutButton} onPress={() => navigation.navigate('Settings')}>
          <Text style={globalStyles.aboutButtonText}>Back</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

// const globalStyles = globalStylesheet.create({
//   containerAbout: {
//     flex: 1,
//     backgroundColor: '#FFFFFF',
//   },
//   scrollContainer: {
//     flexGrow: 1,
//     justifyContent: 'flex-start',
//   },
//   headerAbout: {
//     fontSize: wp('6%'),
//     fontWeight: 'bold',
//     textAlign: 'center',
//     marginTop: hp('4%'),
//     marginBottom: hp('4%'),
//   },
//   sectionAbout: {
//     marginBottom: hp('2.5%'),
//     marginHorizontal: wp('5%'),
//   },
//   sectionTitleAbout: {
//     fontSize: wp('5%'),
//     fontWeight: 'bold',
//     marginBottom: hp('2%'),
//   },
//   sectionTextAbout: {
//     fontSize: wp('3%'),
//     lineHeight: hp('3.5%'),
//     marginBottom: hp('1%'),
//   },
//   aboutButton: {
//     backgroundColor: '#7F3DFF',
//     borderRadius: wp('2%'),
//     paddingVertical: hp('2%'),
//     marginHorizontal: wp('5%'),
//     alignItems: 'center',
//     marginBottom: hp('3%'),
//   },
//   aboutButtonText: {
//     color: '#FFFFFF',
//     fontSize: wp('4%'),
//   },
// });

export default AboutPage;