import React, { useState } from 'react';
import { StyleSheet, Text, View, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 

const TrackerNotification = ({ navigation, route, }) => {
    const { userData } = route.params || {};
    const [dropdownVisible, setDropdownVisible] = useState(false);

    const handleEllipsisPress = () => {
        setDropdownVisible(!dropdownVisible);
    };

    const handleMarkAllRead = () => {
        // Implement logic for marking all notifications as read
        setDropdownVisible(false);
    };

    const handleRemoveAll = () => {
        // Implement logic for removing all notifications
        setDropdownVisible(false);
    };

    return (
        <SafeAreaView style={styles.notificationtrcontainer}>
            <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent={true} />
            <View style={styles.notificationtrcont}>
                <View style={styles.notificationtrContainertitle}>
                <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
                <MaterialCommunityIcons name="arrow-left-thin" size={35} color="black" style={styles.notificationtrIcon} />
                </TouchableOpacity>
                    <Text style={styles.notificationtrTitle}>Notification</Text>
                    <TouchableOpacity onPress={handleEllipsisPress}>
                    <MaterialCommunityIcons name="dots-horizontal" size={30} color="black" style={styles.notificationtrIcon} />
                    </TouchableOpacity>
                </View>
                {dropdownVisible && (
                    <View style={styles.dropdown}>
                        <TouchableOpacity style={styles.dropdownItem} onPress={handleMarkAllRead}>
                            <Text style={styles.dropdowntexttr}>Mark all read</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.dropdownItem} onPress={handleRemoveAll}>
                            <Text style={styles.dropdowntexttr}>Remove all</Text>
                        </TouchableOpacity>
                    </View>
                )}
                <View style={styles.notificationtrmaintextcont}>
                    <View style={styles.notificationtrtext}>
                        <Text style={styles.notificationtrtextheading}>Shopping budget has exceeds the..</Text>
                        <Text style={styles.notificationtrtextpara}>Your Shopping budget has exceeds the lim....</Text>
                    </View>
                    <View style={styles.notificationtrtime}>
                        <Text style={styles.notificationtrtimetext}>19:30</Text>
                    </View>
                </View>
                <View style={styles.notificationtrmaintextcont}>
                    <View style={styles.notificationtrtext}>
                        <Text style={styles.notificationtrtextheading}>Utilities budget has exceeds the..</Text>
                        <Text style={styles.notificationtrtextpara}>Your Utilities budget has exceeds the limit....</Text>
                    </View>
                    <View style={styles.notificationtrtime}>
                        <Text style={styles.notificationtrtimetext}>19:30</Text>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default TrackerNotification;

const styles = StyleSheet.create({
    notificationtrcontainer: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
    },
    notificationtrContainertitle: {
        flexDirection: 'row',
        paddingHorizontal: wp('5%'),
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: hp('3%'),
    },
    notificationtrTitle: {
        fontSize: wp('6%'),
        fontWeight: 'bold',
    },
    notificationtrmaintextcont: {
        paddingHorizontal: wp('5%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: hp('1.5%'),
    },
    notificationtrtextheading: {
        fontSize: wp('4.4%'),
        fontWeight: '700',
        paddingBottom: hp('1%')
    },
    notificationtrtextpara: {
        fontSize: wp('3.8%'),
        color: 'gray'
    },
    notificationtrtimetext: {
        color: 'gray',
        fontWeight: 'bold'
    },
    dropdown: {
        position: 'absolute',
        top: hp('7.5%'),
        right: wp('5%'), 
        backgroundColor: 'white',
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal:15,
        elevation: 5,
        zIndex:1
    },
    dropdownItem: {
        paddingVertical: 10,
    },
    dropdowntexttr:{
        fontSize:wp('4%')
    }
});