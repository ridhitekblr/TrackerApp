import { globalStylesheet, Text, View, TouchableOpacity, StatusBar, Switch, SafeAreaView, Alert, PanResponder } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import React, { useState, useRef } from 'react';
import { useNavigation, useFocusEffect, useRoute } from '@react-navigation/native';
import { Dropdown } from 'react-native-element-dropdown';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'; 
import { globalStyles } from './globalStyles';

const Create_budget = () => {
    const navigation = useNavigation();
    const route = useRoute();

    const data = [
        { label: 'Shopping', value: '1' },
        { label: 'Transportation', value: '2' },
    ];

    const [value, setValue] = useState(null);
    const [isEnabled, setIsEnabled] = useState(false);
    const [sliderValue, setSliderValue] = useState(0);
    const [percentage, setPercentage] = useState(0);
    const [trackWidth, setTrackWidth] = useState(null);
    const [thumbWidth, setThumbWidth] = useState(null);
    const trackRef = useRef(null);
    const thumbRef = useRef(null);

    useFocusEffect(
        React.useCallback(() => {
            if (route.params?.from === 'BudgetEmpty') {
                resetStates();
            }
        }, [route])
    );

    const resetStates = () => {
        setValue(null);
        setIsEnabled(false);
        setSliderValue(0);
        setPercentage(0);
    };

    const toggleSwitch = () => {
        if (!value) {
            Alert.alert('Warning!', 'Please select a category.');
        } else {
            setIsEnabled(!isEnabled);
        }
    };

    const navigateToBudget = () => {
        if (!value) {
            Alert.alert('Warning!', 'Please select a category.');
        } else if (!isEnabled) {
            Alert.alert('Warning!', 'Please activate the switch.');
        } else if (sliderValue === 0) {
            Alert.alert('Warning!', 'Please set a non-zero value on the slider.');
        } else {
            navigation.navigate('BudgetAfter', { category: value, isEnabled: isEnabled, amount: sliderValue });
        }
    };

    const navigateToBudgetEmpty = () => {
        navigation.navigate('BudgetEmpty');
    };

    const thumbPanResponder = PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponder: () => true,
        onPanResponderMove: (_, gestureState) => {
            const newX = Math.min(trackWidth - thumbWidth, Math.max(0, gestureState.moveX - thumbWidth / 2));
            const newPercentage = (newX / (trackWidth - thumbWidth)) * 100;
            const newValue = Math.floor((newPercentage / 100) * 1200);
            setSliderValue(newValue);
            setPercentage(Math.min(100, Math.max(0, newPercentage)));
        },
    });

    const handlePress = (event) => {
        if (!trackWidth) return;
        const locationX = event.nativeEvent.locationX !== undefined ? event.nativeEvent.locationX : event.nativeEvent.pageX - trackRef.current.pageX;
        const newPercentage = (locationX / trackWidth) * 100;
        const newValue = Math.floor((newPercentage / 100) * 1200);
        setSliderValue(newValue);
        setPercentage(Math.min(100, Math.max(0, newPercentage)));
    };

    return (
        <SafeAreaView style={globalStyles.Create_budgetcontainer}>
            <StatusBar backgroundColor="transparent" barStyle="light-content" translucent={true} />
            {/* <View style={globalStyles.notificationtrContainertitle}>
                <TouchableOpacity onPress={navigateToBudgetEmpty}>
                    <MaterialCommunityIcons name="arrow-left-thin" size={35} color="#fff" style={globalStyles.notificationtrIcon} />
                </TouchableOpacity>
                <Text style={globalStyles.notificationtrTitle}>Notification</Text>
            </View> */}
            <View>
                <View style={globalStyles.Create_budgetamount}>
                    <Text style={globalStyles.Create_budgetspend}>How much do you want to spend?</Text>
                    <Text style={globalStyles.Create_budgetamount1}>${isEnabled ? sliderValue : 0}</Text>
                </View>
                <View style={globalStyles.Create_budgetbottom}>
                    <Dropdown
                        style={globalStyles.Create_budgetdropdown}
                        placeholderStyle={globalStyles.Create_budgetplaceholderStyle}
                        selectedTextStyle={globalStyles.Create_budgetselectedTextStyle}
                        inputSearchStyle={globalStyles.Create_budgetinputSearchStyle}
                        iconStyle={globalStyles.iconStyle}
                        data={data}
                        maxHeight={120}
                        labelField="label"
                        valueField="value"
                        placeholder="Category"
                        value={value}
                        onChange={item => {
                            setValue(item.value);
                        }}
                    />
                    <View style={globalStyles.Create_budgettoogle}>
                        <View>
                            <Text style={globalStyles.Create_budgetalert}>Receive Alert</Text>
                            <Text style={globalStyles.Create_budgetalert1}>Receive alert when it reaches{'\n'}some point</Text>
                        </View>
                        <TouchableOpacity style={globalStyles.Create_budgetswitch}>
                            <Switch
                                trackColor={{ false: '', true: '#7F3DFF' }}
                                thumbColor={isEnabled ? '' : '#F5F5F5'}
                                onValueChange={toggleSwitch}
                                value={isEnabled}
                            />
                        </TouchableOpacity>
                    </View>
                    {isEnabled && (
                        <View style={globalStyles.containercus}>
                            <TouchableOpacity
                                ref={trackRef}
                                style={globalStyles.sliderTrack}
                                onLayout={(event) => setTrackWidth(event.nativeEvent.layout.width)}
                                onPress={handlePress}
                            >
                                <View style={[globalStyles.sliderBar, { width: `${percentage}%` }]} />
                            </TouchableOpacity>
                            <View
                                ref={thumbRef}
                                style={[globalStyles.thumb, { left: `${percentage}%` }]}
                                onLayout={(event) => setThumbWidth(event.nativeEvent.layout.width)}
                                {...thumbPanResponder.panHandlers}>
                                <Text style={globalStyles.percentageText}>{Math.round(percentage)}%</Text>
                            </View>
                        </View>
                    )}
                    <View style={globalStyles.Create_budgetbutton}>
                        <TouchableOpacity onPress={navigateToBudget}>
                            <Text style={globalStyles.Create_budgetbuttonText}>Continue</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default Create_budget;

// const globalStyles = globalStylesheet.create({
//     notificationtrContainertitle: {
//         flexDirection: 'row',
//         paddingHorizontal: wp('4%'),
//         alignItems: 'center',
//         paddingTop: hp('3%'),
//     },
//     notificationtrTitle: {
//         fontSize: wp('6'),
//         fontWeight: 'bold',
//         color: '#fff',
//         marginLeft: wp('20%')
//     },
//     containercus: {
//         alignItems: 'center',
//         flex: 1,
//         position: 'relative',
//         paddingBottom: hp('6%'),
//         paddingHorizontal: wp('5%')
//     },
//     sliderTrack: {
//         backgroundColor: '#ddd',
//         height: hp('1.5%'),
//         borderRadius: 10,
//         width: wp('90%'),
//     },
//     sliderBar: {
//         height: '100%',
//         backgroundColor: '#7f3dff',
//         borderRadius: 10,
//     },
//     thumb: {
//         position: 'absolute',
//         justifyContent: 'center',
//         alignItems: 'center',
//         width: wp('12%'),
//         height: hp('4%'),
//         backgroundColor: '#7f3dff',
//         borderRadius: 20,
//         marginTop: hp('-1.5%'),
//         borderColor: '#ffffff',
//         borderWidth: 1,
//     },
//     percentageText: {
//         fontSize: wp('3%'),
//         color: '#fff',
//         fontWeight: '700'
//     },
//     Create_budgetcontainer: {
//         flex: 1,
//         backgroundColor: '#7F3DFF',
//         justifyContent: 'flex-end',
//         paddingTop: StatusBar.currentHeight,
//     },
//     Create_budgetspend: {
//         fontSize: wp('4%'),
//         color: '#fcfcfc'
//     },
//     Create_budgetamount: {
//         paddingHorizontal: wp('5%'),
//         paddingBottom: hp('3%')
//     },
//     Create_budgetamount1: {
//         fontSize: wp('12%'),
//         color: '#fff',
//     },
//     Create_budgetbottom: {
//         backgroundColor: '#ffffff',
//         padding: wp('5%'),
//         borderTopLeftRadius: wp('5%'),
//         borderTopRightRadius: wp('5%'),
//     },
//     Create_budgetdropdown: {
//         paddingVertical: hp('1%'),
//         paddingHorizontal: wp('5%'),
//         marginBottom: hp('2%'),
//         borderRadius: wp('2%'),
//         borderColor: 'lightgray',
//         borderWidth: 1
//     },
//     Create_budgettoogle: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         padding: wp('2%'),
//         marginBottom: hp('3%'),
//     },  
//     Create_budgetalert: {
//         fontSize: wp('4%'),
//         color: '#000',
//     },
//     Create_budgetalert1: {
//         fontSize: wp('3%'),
//         color: '#808080',
//     },
//     Create_budgetbutton: {
//         backgroundColor: '#7f3dff',
//         borderRadius: wp('2%'),
//         paddingVertical: hp('2%'),
//         alignItems: 'center',
       
//     },
//     Create_budgetbuttonText: {
//         fontWeight: 'bold',
//         color: '#fff',
//         fontSize: wp('5%')
//     },
//     Create_budgetplaceholderStyle: {
//         fontSize: wp('4%'),
//         color: '#808080',
//     },  
//     Create_budgetselectedTextStyle: {
//         fontSize: wp('4%'),
//     },
// });

