

import { SafeAreaView, globalStylesheet, Text, View, Image, StatusBar, TouchableOpacity, Modal, TouchableWithoutFeedback } from 'react-native'
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

const DetailTransactionTransfer = ({ navigation, route, }) => {
     const { userData } = route.params || {};
    // use state for modal
    const [showModal, setShowModal] = useState(false);
    // Alert useState
    const [showAlert, setShowAlert] = useState(false);
    
    // toggle for modal
    const toggleModal = () => {
        setShowModal(!showModal);
      };
    // handle Alert
    const handleYesButton = () => {
        toggleModal(); // Close the modal
        setShowAlert(true); // Show the custom alert
    };
    // Close the custom alert when press the outside of the screen
    const handleOverlayPress = () => {
        setShowAlert(false); 
    };
    const CustomAlert = () => (
        <TouchableWithoutFeedback onPress={handleOverlayPress}>
        <View style={globalStyles.DetailTransactionTransfercustomAlertContainer}>
            <View style={globalStyles.DetailTransactionTransfercustomAlert}>
                <Icon name="check-circle" style={globalStyles.DetailTransactionTransferalertIcon} />
                <Text style={globalStyles.alertText}>Transaction has been successfully removed</Text>
            </View>
        </View>
        </TouchableWithoutFeedback>
    );
  return (
    <SafeAreaView style={globalStyles.DetailTransactionTransferSafe}>
      <StatusBar translucent backgroundColor="#0077ff" />
      <View style={globalStyles.DetailTransactionTransferMain}>
        <View style={globalStyles.DetailTransactionTransferTopBOTH}>
          <View style={globalStyles.DetailTransactionTransferTopHead}>
            <TouchableOpacity onPress={() => navigation.navigate('Dashboard', { userData: userData })}>
            <Icon name='arrow-left' style={globalStyles.arrowleftDeatailTransfer}></Icon>
            </TouchableOpacity>
            <Text style={globalStyles.DetailTransactionTransferText}>Detail Transaction</Text>
            <TouchableOpacity onPress={toggleModal}>
              <Icon name='trash' style={globalStyles.trashDeatailTransfer}></Icon>
            </TouchableOpacity>
          </View>
          <View style={globalStyles.DetailTransactionTransferSub2}>
            <Text style={globalStyles.DetailTransactionTransferSub2Rupess}>$2000</Text>
            {/* <Text style={globalStyles.DetailTransactionTransferSub2Shoping}>Salary for July</Text> */}
            <View style={globalStyles.DetailTransactionTransferSu2DayDate}>
              <Text style={globalStyles.DetailTransactionTransferSub2Day}>Saturday 4 June 2021</Text>
              <Text style={globalStyles.DetailTransactionTransferSubDate}>16.20</Text>
            </View>
          </View>
        </View>

        <View style={globalStyles.DetailTransactionTransferSub3}>
          <View style={globalStyles.DetailTransactionTransferSub3Sub}>
            <Text style={globalStyles.DetailTransactionTransferSub3Text1}>Type</Text>
            <Text style={globalStyles.DetailTransactionTransferSub3Text2}>Transfer</Text>
          </View>
          <View style={globalStyles.DetailTransactionTransferSub3Sub}>
            <Text style={globalStyles.DetailTransactionTransferSub3Text1}>From</Text>
            <Text style={globalStyles.DetailTransactionTransferSub3Text2}>Paypal</Text>
          </View>
          <View style={globalStyles.DetailTransactionTransferSub3Sub}>
            <Text style={globalStyles.DetailTransactionTransferSub3Text1}>To</Text>
            <Text style={globalStyles.DetailTransactionTransferSub3Text2}>Chase</Text>
          </View>
        </View>

        <View style={globalStyles.DetailTransactionTransferSub4}>
          <Text style={globalStyles.DetailTransactionTransferSub4Des}>Description</Text>
          <Text style={globalStyles.DetailTransactionTransferSub4Con}>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</Text>
          <Text style={globalStyles.DetailTransactionTransferSub4AttachHead}>Attachment</Text>
          <Image
            source={{ uri: 'https://s3-alpha-sig.figma.com/img/49cc/86e6/849cbd762a23c32962df5f832e7fdf9e?Expires=1716163200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=oanIqv2mujPJGvJT9frRJmX4kSv67HlnW7BP3KVL26yYlO3f6OdEPaCWQ504KEEnVkjxw~Hrdo2yIk2yv2xa965t2tQjM2FKPaRdfvytAFHhxWcs7A4ooi8Qt-tIK8~0F9oPFCZ~enJBtxAJs1pWKvoOHkqhY1Gzf11zIF7QF1KDjKSNgn2DGrSHyXdu2Q3JJIOqgr0M~mIP6FGaaUMkDDsYvZxs~0HD5lhkmW4-Yv2U6MIAqIkvrywLUCODJ3iLWLkV0mCTPMD5-FZTwySRwkjiRmC-YeSPySuXadb-HVta2oeVgNh5lmN7gSfrZDRWnZgqnSsSodW9XS~K73V6GQ__' }}
            style={globalStyles.DetailTransactionTransferSub4Image}
          />
        </View>
      </View>

      {/* Bottom Button */}
      <TouchableOpacity style={globalStyles.DetailTransactionTransferSub5} >
        <Text style={globalStyles.DetailTransactionTransferSub5Text}>Edit</Text>
      </TouchableOpacity>

      {/* Modal */}
      <Modal visible={showModal} animationType="slide" transparent>
        <View style={globalStyles.DetailTransactionTransfermodalContainer}>
          <View style={globalStyles.DetailTransactionTransfermodalContent}>
            <View style={globalStyles.DetailTransactionTransfermodalDivider}></View>
            <Text style={globalStyles.DetailTransactionTransfermodalTitle}>Remove this transaction?</Text>
            <Text style={globalStyles.DetailTransactionTransfermodalText}>Are you sure you want to remove this transaction?</Text>
            <View style={globalStyles.DetailTransactionTransfermodalButtonContainer}>
              <TouchableOpacity style={globalStyles.DetailTransactionTransfermodalButtonNo} onPress={toggleModal}>
                <Text style={globalStyles.DetailTransactionTransfermodalButtonTextNo}>No</Text>
              </TouchableOpacity>
              <TouchableOpacity style={globalStyles.DetailTransactionTransfermodalButtonYes} onPress={handleYesButton}>
                <Text style={globalStyles.DetailTransactionTransfermodalButtonTextYes} >Yes</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* Custom Alert */}
      {showAlert && <CustomAlert />}
    </SafeAreaView>
  )
}

export default DetailTransactionTransfer;

// const globalStyles = globalStylesheet.create({
//     DetailTransactionTransferSafe: {
//         flexGrow:1
//       },
//       DetailTransactionTransferMain: {
//         display:'flex',
//         justifyContent:'center',
//         marginTop: StatusBar.currentHeight || 0,
//       },
//       DetailTransactionTransferTopBOTH: {
//         backgroundColor: '#0077ff',
//         paddingHorizontal: wp('5%'),
//         paddingVertical: hp('5%'),
//         borderBottomLeftRadius: hp('3%'),
//         borderBottomRightRadius: hp('3%'),
//         position: 'relative',
//       },
//       DetailTransactionTransferTopHead: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//       },
//       arrowleftDeatailTransfer: {
//         fontSize: hp('2.5%'),
//         color: '#fff',
//       },
//       DetailTransactionTransferText: {
//         fontSize: hp('2.5%'),
//         color: '#fff',
//       },
//       trashDeatailTransfer: {
//         fontSize: hp('2.5%'),
//         color: '#fff',
//       },
//       DetailTransactionTransferSub2: {
//         justifyContent: 'center',
//         alignItems: 'center',
//         paddingTop: hp('4%'),
//         paddingBottom: hp('4%'),
//         gap: hp('1%'),
//       },
//       DetailTransactionTransferSu2DayDate: {
//         flexDirection: 'row',
//         gap: hp('1%'),
//       },
//       DetailTransactionTransferSub2Rupess: {
//         fontSize: hp('5%'),
//         fontWeight: '700',
//         color: '#fff',
//       },
//       DetailTransactionTransferSub2Shoping: {
//         color: '#fff',
//         fontSize: hp('2%'),
//       },
//       DetailTransactionTransferSub2Day: {
//         color: '#fff',
//         fontSize: hp('2%'),
//       },
//       DetailTransactionTransferSubDate: {
//         color: '#fff',
//         fontSize: hp('2%'),
//       },
//       DetailTransactionTransferSub3: {
//         flexDirection: 'row',
//         borderWidth: 1,
//         marginHorizontal: wp('5%'),
//         justifyContent: 'space-around',
//         bottom: hp('4%'),
//         backgroundColor: '#fff',
//         paddingVertical: hp('2a%'),
//         borderRadius: hp('1%'),
//         borderColor: '#f1f1fa',
//       },
//       DetailTransactionTransferSub3Sub: {
//         alignItems: 'center',
//         gap: hp('1%'),
//       },
//       DetailTransactionTransferSub3Text1: {
//         color: '#91919f',
//         fontSize: hp('1.7%'),
//         fontWeight: '500',
//       },
//       DetailTransactionTransferSub3Text2: {
//         fontSize: hp('2%'),
//         fontWeight: '500',
//       },
//       DetailTransactionTransferSub4: {
//         borderTopWidth: hp('0.2%'),
//         borderStyle: 'dashed',
//         marginHorizontal: wp('5%'),
//         bottom: hp('2%'),
//         paddingVertical: hp('2%'),
//         gap: hp('1%'),
//       },
//       DetailTransactionTransferSub4Des: {
//         fontSize: hp('2%'),
//         color: '#91919f',
//       },
//       DetailTransactionTransferSub4Con: {
//         fontSize: hp('1.8%'),
//       },
//       DetailTransactionTransferSub4AttachHead: {
//         fontSize: hp('2%'),
//         color: '#91919f',
//       },
//       DetailTransactionTransferSub4Image: {
//         width: '100%',
//         height: hp('20%'),
//         borderRadius: hp('1%'),
//       },
//       DetailTransactionTransferSub5: {
//         position: 'absolute',
//         bottom: hp('2%'),
//         left: wp('5%'),
//         right: wp('5%'),
//         backgroundColor: '#7f3dff',
//         alignItems: 'center',
//         paddingVertical: hp('2%'),
//         borderRadius: hp('1%'),
//       },
//       DetailTransactionTransferSub5Text: {
//         fontSize: hp('2%'),
//         fontWeight: '600',
//         color: '#fff',
//       },
//     //   for modal
//     DetailTransactionTransfermodalContainer: {
//         flex: 1,
//         justifyContent: 'flex-end',
//         alignItems: 'center',
//         backgroundColor: 'rgba(0, 0, 0, 0.5)',
//       },
//       DetailTransactionTransfermodalContent: {
//         backgroundColor: '#ffffff',
//         width: '100%',
//         paddingVertical: hp('2%'),
//         paddingHorizontal: wp('5%'),
//         borderTopLeftRadius: hp('2%'),
//         borderTopRightRadius: hp('2%'),
//         gap:15,
//       },
//       DetailTransactionTransfermodalDivider: {
//         width: '10%',
//         height: 5,
//         backgroundColor: '#d3bdff',
//         alignSelf: 'center',
//         marginBottom: hp('1%'),
//       },
//       DetailTransactionTransfermodalTitle: {
//         fontSize: hp('2.2%'),
//         fontWeight: '600',
//         textAlign: 'center',
//         marginBottom: hp('1%'),
//       },
//       DetailTransactionTransfermodalText: {
//         fontSize: hp('2%'),
//         textAlign: 'center',
//         marginBottom: hp('2%'),
//         color:'#91919f'
//       },
//       DetailTransactionTransfermodalButtonContainer: {
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//       },
//       DetailTransactionTransfermodalButtonNo: {
//         flex: 1,
//         paddingVertical: hp('2%'),
//         alignItems: 'center',
//         backgroundColor: '#eee5ff',
//         borderRadius: hp('2%'),
//         marginHorizontal: wp('1%'),
//       },
//       DetailTransactionTransfermodalButtonYes: {
//         flex: 1,
//         paddingVertical: hp('2%'),
//         alignItems: 'center',
//         backgroundColor: '#7f3dff',
//         borderRadius: hp('2%'),
//         marginHorizontal: wp('1%'),
//       },
//       DetailTransactionTransfermodalButtonTextNo: {
//         fontSize: hp('2%'),
//         fontWeight: '600',
//         color: '#7f3dff',
//       },
//       DetailTransactionTransfermodalButtonTextYes: {
//         fontSize: hp('2%'),
//         fontWeight: '600',
//         color: '#ffffff',
//       },
//     //   Alert
//     DetailTransactionTransferoverlay: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: 'rgba(0, 0, 0, 0.5)', 
//     },
//     DetailTransactionTransfercustomAlertContainer: {
//         position: 'absolute',
//         top: 0,
//         bottom: 0,
//         left: 0,
//         right: 0,
//         backgroundColor: 'rgba(0, 0, 0, 0.5)', 
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     DetailTransactionTransfercustomAlert: {
//         backgroundColor: '#fff',
//         padding: wp('5%'), 
//         borderRadius: hp('2%'), 
//         alignItems: 'center',
//     },
//     DetailTransactionTransferalertIcon: {
//         fontSize: hp('7%'), 
//         color: '#5233ff',
//         marginBottom: hp('1%'), 
//     },
//     DetailTransactionTransferalertText: {
//         fontSize: hp('2%'), 
//         textAlign: 'center',
//     },
// })