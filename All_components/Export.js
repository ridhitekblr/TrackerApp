import React, { useState } from 'react';
import { SafeAreaView, globalStylesheet, Text, View, TouchableOpacity, ScrollView, StatusBar, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { globalStyles } from './globalStyles';

const Export = ({navigation}) => {
  const [selectedExportData, setSelectedExportData] = useState(null);
  const [selectedRange, setSelectedRange] = useState(null);
  const [selectedExportFormat, setSelectedExportFormat] = useState(null);
  const [activeDropdown, setActiveDropdown] = useState(null);

  const toggleDropdown = (dropdown) => {
    setActiveDropdown(activeDropdown === dropdown ? null : dropdown);
  };

  const selectExportData = (data) => {
    setSelectedExportData(data);
    setActiveDropdown(null);
  };

  const selectRange = (range) => {
    setSelectedRange(range);
    setActiveDropdown(null);
  };

  const selectExportFormat = (format) => {
    setSelectedExportFormat(format);
    setActiveDropdown(null);
  };

  const exportDataOptions = ['All', 'Data 2', 'Data 3']; 
  const rangeOptions = ['last 7 days', 'last 30 days', 'last 6 month']; 
  const exportFormatOptions = ['CSV', 'PDF', 'DOCX','XLSV','PNG/JPEG']; 

  const showAlertExportData = () => {
    Alert.alert('Error', 'Please select export data.');
  };

  const showAlertRange = () => {
    Alert.alert('Error', 'Please select data range.');
  };

  const showAlertExportFormat = () => {
    Alert.alert('Error', 'Please select export format.');
  };

  return (
    <SafeAreaView style={globalStyles.settingsexportContainer}>
      <StatusBar translucent backgroundColor="transparent" />
      <View style={globalStyles.settingsexportContent}>
        <Text style={globalStyles.settingsexportTitle}>What data do you want to export?</Text>
        <View>
          <TouchableOpacity style={globalStyles.settingsexportDropdown} onPress={() => toggleDropdown('exportData')}>
            <Text>{selectedExportData || "Select Data"}</Text>
            <Icon name={activeDropdown === 'exportData' ? 'caret-up' : 'caret-down'}/>
          </TouchableOpacity>

          {activeDropdown === 'exportData' && (
            <ScrollView style={globalStyles.settingsexportDropdownContent}>
              <View style={globalStyles.settingsexportDropdownOptions}>
                {exportDataOptions.map((data, index) => (
                  <TouchableOpacity
                    key={index}
                    style={globalStyles.settingsexportDropdownItem}
                    onPress={() => selectExportData(data)}>
                    <Text>{data}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            </ScrollView>
          )}
        </View>
        <Text style={globalStyles.settingsexportTitle}>When data range?</Text>
        <View>
          <TouchableOpacity style={globalStyles.settingsexportDropdown} onPress={() => toggleDropdown('range')}>
            <Text>{selectedRange || "Range"}</Text>
            <Icon name={activeDropdown === 'range' ? 'caret-up' : 'caret-down'} />
          </TouchableOpacity>

          {activeDropdown === 'range' && (
            <ScrollView style={globalStyles.settingsexportDropdownContent}>
              <View style={globalStyles.settingsexportDropdownOptions}>
                {rangeOptions.map((range, index) => (
                  <TouchableOpacity
                    key={index}
                    style={globalStyles.settingsexportDropdownItem}
                    onPress={() => selectRange(range)}>
                    <Text>{range}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            </ScrollView>
          )}
        </View>
        <Text style={globalStyles.settingsexportTitle}>What format do you want to export?</Text>
        <View>
          <TouchableOpacity style={globalStyles.settingsexportDropdown} onPress={() => toggleDropdown('exportFormat')}>
            <Text>{selectedExportFormat || "Format"}</Text>
            <Icon name={activeDropdown === 'exportFormat' ? 'caret-up' : 'caret-down'}  />
          </TouchableOpacity>

          {activeDropdown === 'exportFormat' && (
            <ScrollView style={globalStyles.settingsexportDropdownContent}>
              <View style={globalStyles.settingsexportDropdownOptions}>
                {exportFormatOptions.map((format, index) => (
                  <TouchableOpacity
                    key={index}
                    style={globalStyles.settingsexportDropdownItem}
                    onPress={() => selectExportFormat(format)}>
                    <Text>{format}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            </ScrollView>
          )}
        </View>
        <TouchableOpacity
          style={globalStyles.settingsexportButton}
          onPress={() => {
            if (!selectedExportData) {
              showAlertExportData();
            } else if (!selectedRange) {
              showAlertRange();
            } else if (!selectedExportFormat) {
              showAlertExportFormat();
            } else {
              navigation.navigate('ExportEmail');
            }
          }}>
          <Icon name="download" size={wp('4%')} color="#FFFFFF" style={globalStyles.settingsexportIcon} />
          <Text style={globalStyles.settingsexportButtonText}>Export</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Export;

// const globalStyles = globalStylesheet.create({
//   settingsexportContainer: {
//     flex: 1,
//     backgroundColor: '#FFFFFF',
//     paddingTop: StatusBar.currentHeight || 0, 
//   },
//   settingsexportContent: {
//     flex: 1,
//     paddingHorizontal: wp('5%'),
//     paddingTop: hp('2%'),
//   },
//   settingsexportTitle: {
//     fontSize: wp('4.5%'),
//     fontWeight: '600',
//     marginBottom: hp('2%'),
//   },
//   settingsexportDropdown: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//     paddingVertical: hp('2%'),
//     paddingHorizontal: wp('5%'),
//     borderWidth: 1,
//     borderColor: 'lightgrey',
//     borderRadius: wp('2%'),
//     marginBottom: hp('2%'),
//   },
//   settingsexportDropdownOptions: {
//     paddingVertical: hp('1%'),
//     paddingHorizontal: wp('3%'),
//   },
//   settingsexportDropdownItem: {
//     paddingVertical: hp('1%'),
//     paddingHorizontal: wp('3%'),
//   },
//   settingsexportButton: {
//     backgroundColor: '#7F3DFF',
//     borderRadius: wp('2%'),
//     paddingVertical: hp('2%'),
//     alignItems: 'center',
//     marginTop: 'auto', 
//     marginBottom: hp('2%'),
//     flexDirection: 'row',
//     justifyContent: 'center',
//   },
//   settingsexportButtonText: {
//     color: '#FFFFFF',
//     fontSize: wp('5%'),
//   },
//   settingsexportIcon: {
//     marginRight: wp('4%'),
//   },
// });